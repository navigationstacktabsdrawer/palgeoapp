import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  StatusBar,
  Platform,
  PermissionsAndroid,
  AppState,
  BackHandler,
  LogBox,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {Root} from 'native-base';
import MainNavigator from './src/Navigation/MainNavigator';
import {navigationRef} from './src/components/RootNavigation';
import SplashScreen from 'react-native-splash-screen';
import VersionNumber from 'react-native-version-number';
import Const from './src/components/common/Constants';
import AskForUpdate from './src/components/common/AskForUpdate';
import WifiManager from 'react-native-wifi-reborn';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Geolocation from 'react-native-geolocation-service';
import _BackgroundTimer from 'react-native-background-timer';
import OneSignal from 'react-native-onesignal';
import * as RootNavigation from './src/components/RootNavigation';
import Boundary, {Events} from 'react-native-boundary';
import GPSState from 'react-native-gps-state';
import axios from 'axios';
var moment = require('moment');

import AskForPermissions from './src/components/common/AskForPermissions';

import NetInfo from '@react-native-community/netinfo';
import {addTask} from './src/utils/helperFunctions';
import VersionCheck from 'react-native-version-check';

const slides = [
  {
    key: '1',
    title: 'Welcome to PalGeo',
    text: 'Geofencing attendance system',
    image: require('./src/assets/intro1.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: '2',
    title: 'Secured Checkin',
    text: 'Checkin securely through face evaluation from location specified',
    image: require('./src/assets/intro2.png'),
    backgroundColor: '#59b2ab',
  },
  {
    key: '3',
    title: 'Staff Tasks & Reports',
    text: 'Get Staff Reports and Day to day tasks',
    image: require('./src/assets/intro3.png'),
    backgroundColor: '#febe29',
  },
];
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
      appIntro: '',
      newVersionRequired: false,
      inactive: true,
      position: {},
      bearer_token: '',
      show: false,
      locationPermissions: false,
      gpsInfo: [],
      netInfo: {},
      isOn: true,
    };
    this.ConfigureOneSignal();
  }

  async componentDidMount() {
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const StaffNo = await AsyncStorage.getItem('user_id');
    const geoOld = await AsyncStorage.getItem('geo_id');
    console.disableYellowBox = true;
    //LogBox.ignoreAllLogs(true)
    fetch(Const + 'api/Staff/IsCheckedInNew', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        staffCode: StaffNo,
        instituteId: Number(institute_id),
      }),
    })
      .then((response) => response.json())
      .then(async (json) => {
        console.log('jOSN=-==', json);
        if (json.isCheckedIn && json.locationDetails.type === 'Circle') {
          await AsyncStorage.setItem('checked_out', 'no');
          if (geoOld) {
            return;
          }
          await AsyncStorage.setItem(
            'radius',
            json.locationDetails.radius.toString(),
          );
          await AsyncStorage.setItem(
            'geo_id',
            json.locationDetails.id.toString(),
          );
          await AsyncStorage.setItem(
            'coordinates',
            JSON.stringify(json.locationDetails.coordinates),
          );
        }
      })
      .catch((error) => console.log('error_IS_CHECKED_IN =>', error));
    SplashScreen.hide();
    const checked_out = await AsyncStorage.getItem('checked_out');
    AsyncStorage.getItem('locationPermissions').then((locationPermissions) => {
      console.log('locationPermissions ==>', locationPermissions);
      this.setState({locationPermissions: locationPermissions}, () => {
        if (
          this.state.locationPermissions === 'yes' ||
          this.state.locationPermissions
        ) {
          console.log('permissions granted');
        }
      });
    });
    this.unsubscribe = NetInfo.addEventListener(this.checkInternet);
    AppState.addEventListener('change', this.checkUserActivity);

    GPSState.addListener(this.checkGpsStatus);

    //this.checkUpdateVersion();
    AsyncStorage.getItem('appIntro').then((appIntro) => {
      console.log('appIntro', appIntro);
      this.setState({appIntro: appIntro});
    });
  }

  checkInternet = async (state) => {
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const checked_out = await AsyncStorage.getItem('checked_out');
    console.log('StaffNo', StaffNo);
    console.log('Is connected?', state.isConnected);
    this.setState({isOn: state.isConnected});
    if (checked_out === 'yes' || !checked_out) {
      return;
    }
    if (!state.isConnected) {
      console.log('I ran without internet connection dude!!');
      let temp = {
        AccessTime: new Date().toUTCString(),
        IsOn: state.isConnected,
        StaffCode: StaffNo,
      };
      await AsyncStorage.setItem('netInfo', JSON.stringify(temp)).then(() =>
        this.setState({netInfo: temp}),
      );
      return;
    }
    if (state.isConnected && StaffNo && institute_id) {
      const gpsInfo = JSON.parse(await AsyncStorage.getItem('gpsInfo'));
      if (gpsInfo?.length > 0) {
        try {
          return axios
            .post(Const + 'api/Staff/MobileGpsOnOrOffOffline', {
              StaffCode: StaffNo,
              InstituteId: Number(institute_id),
              Data: gpsInfo,
            })
            .then((response) => {
              this.setState({gpsInfo: []});
              AsyncStorage.setItem('gpsInfo', JSON.stringify([]));
              console.log('response ==>', response.data);
              return;
              //this.unsubscribe();
            })
            .catch((err) => console.log('error_GPS', err));
        } catch (error) {
          console.log('error_GPS_INFO ==>', error);
        }
      }
      const netInfo = JSON.parse(await AsyncStorage.getItem('netInfo'));
      let t = netInfo;
      console.log('nnetInfo ==>', t);

      if (netInfo && netInfo != {}) {
        let temp1 = {
          AccessTime: new Date().toUTCString(),
          IsOn: true,
          StaffCode: StaffNo,
        };
        console.log('I ran dude!');
        try {
          return axios
            .post(Const + 'api/Staff/MobileInternetOnOrOff', {
              StaffCode: StaffNo,
              InstituteId: Number(institute_id),
              Data: [netInfo, temp1],
            })
            .then(async (response) => {
              // console.log('netinfo', netInfo);
              this.setState({netInfo: {}});
              await AsyncStorage.setItem('netInfo', JSON.stringify({}));

              console.log(response.data);
            })
            .catch((error) => console.log('error', error));
        } catch (error) {
          console.log('error_NETINFO ==>', error);
        }
      }
    }
  };
  checkGpsStatus = async (status) => {
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const checked_out = await AsyncStorage.getItem('checked_out');
    console.log('GPS_state', status, checked_out);
    if (checked_out === 'yes' || !checked_out) {
      return;
    }
    if (!this.state.isOn) {
      this.setState(
        {
          gpsInfo: [
            ...this.state.gpsInfo,
            {
              AccessTime: new Date().toUTCString(),
              IsGpsOn: status == 1 || status == 2 ? false : true,
              StaffCode: StaffNo,
            },
          ],
        },
        () => {
          const uniques = Object.values(
            this.state.gpsInfo.reduce((a, c) => {
              a[c.AccessTime] = c;
              return a;
            }, {}),
          );
          AsyncStorage.setItem('gpsInfo', JSON.stringify(uniques));
          return console.log('GPS)INFO', uniques);
        },
      );
      return;
    }

    //console.log(institute_id);
    //call api
    console.log('change in gps state');
    try {
      return axios
        .post(Const + 'api/Staff/MobileGpsOnOrOff', {
          StaffCode: StaffNo,
          InstituteId: Number(institute_id),
          IsGpsOn: status == 1 || status == 2 ? false : true,
        })
        .then((response) => console.log('JSON_GPS', response.data));
    } catch (error) {
      console.log('error_NET_ON_GPSERROR ==>', error);
    }
  };
  checkUserActivity = async (state) => {
    //console.log('app', state);
    const prevState = await AsyncStorage.getItem('app_status');
    const time = await AsyncStorage.getItem('app_change_time');
    //console.log('step 1', prevState);
    if (state !== prevState) {
      let now = moment().format('YYYY-MM-DD HH:mm:ss');

      let then = moment(new Date(time)).format('YYYY-MM-DD HH:mm:ss');

      var ms = moment(now, 'YYYY-MM-DD HH:mm:ss').diff(
        moment(then, 'YYYY-MM-DD HH:mm:ss'),
      );

      var minutes = moment.duration(ms).asMinutes();
      //console.log('duration', minutes);
      // if(parseInt(minutes) >=10 && state === 'active'){
      //   await AsyncStorage.setItem('bearer_token', '')
      //   RootNavigation.navigate('MainNavigator')
      // }
      if (parseInt(minutes) >= 3 && state === 'active') {
        await this.updateStatus(false, time);
      }
    }
    await AsyncStorage.setItem('app_status', state);
    await AsyncStorage.setItem('app_change_time', new Date().toString());

    //console.log('app_status_previous', prevState, time);
  };

  successLocation = (position) => {
    this.setState({position});
    //console.log('accuracy_watch', position[0]);
  };

  errorLocation = (error) => console.log(error);

  componentWillUnmount() {
    OneSignal.removeEventListener('ids', this.onIds);
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    AppState.removeEventListener('change', this.checkUserActivity);

    if (this.unsubscribe) {
      this.unsubscribe();
      this.unsubscribe = null;
    }
    GPSState.removeListener(() => {
      console.log('GPS_state');
    });
  }
  ConfigureOneSignal = () => {
    OneSignal.setLogLevel(7, 0);
    OneSignal.inFocusDisplaying(2);
    OneSignal.init('cb52438d-c790-46e4-83de-effe08725aff', {
      kOSSettingsKeyAutoPrompt: true,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.configure({});
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
  };

  updateStatus = async (app_state_now, time) => {
    //console.log('app_status_now', app_state_now, new Date(time).toUTCString());
    const checked_out = await AsyncStorage.getItem('checked_out');
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const isFaceRequired = await AsyncStorage.getItem('isFaceRequired');
    //const app_status = await AsyncStorage.getItem('app_status');
    if (isFaceRequired === 'true') {
      if (checked_out === 'yes' || !checked_out) {
        return;
      }
      try {
        fetch(Const + 'api/Staff/UpdateStaffMobileAppActiveInactiveStatus', {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + bearer_token,
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            StaffCode: StaffNo,
            InstituteId: Number(institute_id),
            IsActive: app_state_now,
            UpdateTime: new Date(time).toUTCString(),
          }),
        }).then(() => AsyncStorage.setItem('app_status', 'active'));
      } catch (error) {
        console.log('error', error);
      }

      return;
    }
    if (isFaceRequired === 'false') {
      try {
        fetch(Const + 'api/Staff/UpdateStaffMobileAppActiveInactiveStatus', {
          method: 'POST',
          headers: {
            Authorization: 'Bearer ' + bearer_token,
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            StaffCode: StaffNo,
            InstituteId: Number(institute_id),
            IsActive: app_state_now,
            UpdateTime: new Date(time).toUTCString(),
          }),
        }).then(() => AsyncStorage.setItem('app_status', 'active'));
      } catch (error) {
        console.log('error', error);
      }
      return;
    }
  };

  onIds(device) {
    try {
      AsyncStorage.setItem('device_token', device.userId);
      console.log('[device_token]', device.userId);
    } catch (e) {
      console.log(e);
    }
  }
  async onReceived(notification) {
    //console.warn('Notification received: ', notification);
    try {
      const response = await fetch(Const + 'api/MobileApp/UpdateNotification', {
        method: 'POST',
        headers: {
          Accept: 'text/plain',
          'Content-Type': 'application/json-patch+json',
        },
        body: JSON.stringify({
          NotificationId: notification.payload.NotificationID,
          IsReceived: true,
          IsRead: false,
        }),
      });
      //RootNavigation.navigate('CircularList', {data: 'push'});
    } catch (error) {
      console.log('error', error);
    }
  }
  async onOpened(openResult) {
    //console.log('opened', openResult);
    try {
      const response = await fetch(Const + 'api/MobileApp/UpdateNotification', {
        method: 'POST',
        headers: {
          Accept: 'text/plain',
          'Content-Type': 'application/json-patch+json',
        },
        body: JSON.stringify({
          NotificationId: openResult.notification.payload.NotificationID,
          IsReceived: true,
          IsRead: true,
        }),
      });
      RootNavigation.navigate('CircularList', {data: 'push'});
    } catch (error) {
      console.log('error', error);
    }
  }

  _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <View>
          <Image
            source={item.image}
            resizeMode="contain"
            style={styles.image}
          />
        </View>
        <View style={styles.headingContainer}>
          <Text style={styles.heading}>{item.title}</Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{item.text}</Text>
        </View>
      </View>
    );
  };
  _onDone = () => {
    try {
      AsyncStorage.setItem('appIntro', 'done');
      this.setState({showRealApp: true, appIntro: 'done'});
    } catch (e) {
      console.log(e);
    }
  };

  checkUpdateVersion = () => {
    VersionCheck.needUpdate().then(async (res) => {
      console.log('update needed ==>', res);
      if (res.isNeeded) {
        this.setState({newVersionRequired: true});
      } else {
        this.setState({newVersionRequired: false});
      }
    });
    // this.setState({showAlert: true});
    // fetch(
    //   Const +
    //     'api/MobileApp/IsMobileUpdateRequired?version=' +
    //     parseInt(VersionNumber.buildVersion),
    //   {
    //     method: 'GET',
    //     headers: {
    //       Accept: 'text/plain',
    //       'Content-Type': 'application/json-patch+json',
    //     },
    //   },
    // )
    //   .then((response) => response.json())
    //   .then((json) => {
    //     //console.log('version_json_check', json);
    //     if (json) {
    //       this.setState({newVersionRequired: true});
    //     } else {
    //       this.setState({newVersionRequired: false});
    //     }
    //   })
    //   .catch((error) => {
    //     this.setState({showAlert: false});
    //     console.error(error);
    //   });
  };
  renderDoneButton = () => {
    return (
      <View style={styles.nextContainer}>
        <Text style={styles.next}>Done</Text>
      </View>
    );
  };
  renderNextButton = () => {
    return (
      <View style={styles.nextContainer}>
        <Text style={styles.next}>Next</Text>
      </View>
    );
  };
  renderPrevButton = () => {
    return (
      <View style={styles.nextContainer1}>
        <Text style={styles.next}>Back</Text>
      </View>
    );
  };

  askForPermissions = async () => {
    if (Platform.OS == 'android') {
      const granted = await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
        ],

        {
          title: 'Attention',
          message: 'Palgeo needs access to your location in background',
        },
      );
      console.log('Android  Location permission granted?' + granted);
      if (granted) {
        const enabled = await WifiManager.isEnabled();
        if (!enabled) WifiManager.setEnabled(true);
        await AsyncStorage.setItem('locationPermissions', 'yes');
        this.setState({locationPermissions: true}, () => {
          console.log('prmissions is true');
          addTask();
        });
      } else {
        alert('Permission to access location denied');
      }
    } else if (Platform.OS == 'ios') {
      Geolocation.requestAuthorization('always');
    }
  };

  //backgroundTimer

  // backgroundCall = () => {

  // };

  render() {
    if (this.state.newVersionRequired) {
      return <AskForUpdate />;
    }

    if (!this.state.locationPermissions) {
      return (
        <AskForPermissions
          onPress={this.askForPermissions}
          onPressCancel={() => {
            this.setState({locationPermissions: false});
            BackHandler.exitApp();
          }}
        />
      );
    }

    // if (this.state.show) {
    //   return (
    //     <MovingOutModal
    //       visible={this.state.show}
    //       onClose={() => this.setState({show: false})}
    //     />
    //   );
    // }

    if (this.state.appIntro == 'done') {
      return (
        <NavigationContainer ref={navigationRef}>
          <Root>
            <MainNavigator />
          </Root>
        </NavigationContainer>
      );
    } else if (this.state.appIntro == '') {
      return (
        <View style={{flex: 1, backgroundColor: '#ffffff'}}>
          <StatusBar backgroundColor="#ffffff" hidden />
          <Image
            source={require('./src/assets/ic_bg.png')}
            style={styles.introBg}
          />
          <View style={{flex: 1, marginBottom: hp('15')}}>
            <AppIntroSlider
              renderItem={this._renderItem}
              data={slides}
              onDone={this._onDone}
              activeDotStyle={{backgroundColor: '#f05760'}}
              dotStyle={{backgroundColor: '#f9bcbf'}}
              showDoneButton={true}
              showNextButton={true}
              showPrevButton={true}
              renderDoneButton={this.renderDoneButton}
              renderNextButton={this.renderNextButton}
              renderPrevButton={this.renderPrevButton}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={{flex: 1, backgroundColor: '#ffffff'}}>
          <StatusBar backgroundColor="#ffffff" hidden />
          <Image
            source={require('./src/assets/ic_bg.png')}
            style={styles.introBg}
          />
          <View style={{flex: 1, marginBottom: hp('15')}}>
            <AppIntroSlider
              renderItem={this._renderItem}
              data={slides}
              onDone={this._onDone}
              activeDotStyle={{backgroundColor: '#f05760'}}
              dotStyle={{backgroundColor: '#f9bcbf'}}
              showDoneButton={true}
              showNextButton={true}
              showPrevButton={true}
              renderDoneButton={this.renderDoneButton}
              renderNextButton={this.renderNextButton}
              renderPrevButton={this.renderPrevButton}
            />
          </View>
        </View>
      );
    }
  }
}
const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 250,
  },
  slide: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '30%',
  },
  heading: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 22,
    color: '#000000',
    textAlign: 'center',
    textAlign: 'center',
  },
  description: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#555a6d',
    textAlign: 'center',
  },
  descriptionContainer: {
    marginLeft: '10%',
    marginRight: '10%',
  },
  headingContainer: {
    marginTop: '7%',
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  next: {
    fontFamily: 'Poppins-Regular',
    fontSize: 17,
  },
  previous: {
    fontFamily: 'Poppins-Regular',
    fontSize: 17,
  },
  next: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#f05760',
  },
  nextContainer: {
    paddingTop: '25%',
  },
  nextContainer1: {
    paddingTop: '26%',
  },
  introBg: {
    width: wp('100'),
    height: hp('100'),
    backgroundColor: '#f05760',
    position: 'absolute',
  },
});
