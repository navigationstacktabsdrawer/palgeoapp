import AsyncStorage from '@react-native-community/async-storage';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import Const from '../components/common/Constants';
import RNLocation from 'react-native-location';
import axios from 'axios';
import Geocoder from 'react-native-geocoding';
import moment from 'moment';
import CameraMP from 'react-native-camera-megapixel';
import {GOOGLE_MAPS_APIKEY} from './configs/Constants';

var geolib = require('geolib');

export const getAssignedLocations = async () => {
  try {
    const institute_id = await AsyncStorage.getItem('institute_id');
    const user_id = await AsyncStorage.getItem('user_id');
    console.log('institute_id and user_id', institute_id, user_id);
    if (user_id && institute_id) {
      const response = await fetch(
        Const + 'api/Staff/information/' + institute_id + '/' + user_id,
        {
          method: 'GET',
          headers: {
            Accept: 'text/plain',
            'content-type': 'application/json-patch+json',
          },
        },
      );
      const json = await response.json();
      if (json.status) {
        const locations = json.assignedLocations.filter(
          (assLoc) => assLoc.type === 'Circle',
        );
        console.log('locations', locations);
        return await AsyncStorage.setItem(
          'locations',
          JSON.stringify(locations),
        );
      }
    }
  } catch (error) {
    console.log(error, 'error to add boundaries');
    return error;
  }
};

export const addBoundaries = async (user_id, institute_id) => {
  if (user_id) {
    try {
      const response = await fetch(
        Const + 'api/Staff/information/' + institute_id + '/' + user_id,
        {
          method: 'GET',
          headers: {
            Accept: 'text/plain',
            'content-type': 'application/json-patch+json',
          },
        },
      );
      const json = await response.json();
      if (json.status) {
        console.log('LOCATIONS ==>', json.assignedLocations);
        const locations = json.assignedLocations.filter(
          (location) =>
            location.accessLocation !== 'Travel Check In' &&
            location.type === 'Circle',
        );
        await AsyncStorage.setItem('locations', JSON.stringify(locations));
        //console.log('locations', locations);
        // for (let i = 0; i < locations.length; i++) {
        //   await Boundary.add({
        //     lat: parseFloat(locations[i].coordinates[0].latitude),
        //     lng: parseFloat(locations[i].coordinates[0].longitude),
        //     radius: parseInt(locations[i].radius),
        //     id: locations[i].id.toString(),
        //   })
        //     .then(() => console.log('success!!!'))
        //     .catch((error) => console.log('Failed to add'));
        // }
      }
    } catch (error) {
      console.log(error, 'error to add boundaries');
      return;
    }
  }
};

export const addTask = () => {
  RNLocation.configure({
    distanceFilter: 0, // Meters
    desiredAccuracy: {
      ios: 'best',
      android: 'balancedPowerAccuracy',
    },
    // Android only
    androidProvider: 'auto',
    interval: 60000, // Milliseconds
    fastestInterval: 60000, // Milliseconds
    maxWaitTime: 5000, // Milliseconds
    // iOS Only
    activityType: 'other',
    allowsBackgroundLocationUpdates: false,
    headingFilter: 1, // Degrees
    headingOrientation: 'portrait',
    pausesLocationUpdatesAutomatically: false,
    showsBackgroundLocationIndicator: false,
  });
  let locationSubscription = null;
  let locationTimeout = null;
  // if (run) {
  if (ReactNativeForegroundService.is_task_running('taskid')) return;
  //}
  ReactNativeForegroundService.add_task(
    () => {
      RNLocation.requestPermission({
        ios: 'whenInUse',
        android: {
          detail: 'fine',
        },
      }).then((granted) => {
        //console.log('Location Permissions: ', granted);
        // if has permissions try to obtain location with RN location
        if (granted) {
          locationSubscription && locationSubscription();
          locationSubscription = RNLocation.subscribeToLocationUpdates(
            async (position) => {
              locationSubscription();
              locationTimeout && clearTimeout(locationTimeout);

              const StaffNo = await AsyncStorage.getItem('user_id');
              const institute_id = await AsyncStorage.getItem('institute_id');
              const locations = JSON.parse(
                await AsyncStorage.getItem('locations'),
              );
              //console.log('locations', locations);

              // New method for entry and exit triggers:
              // check 1:
              let checkedInLocation = await AsyncStorage.getItem('geo_id');
              const geoLocation = await AsyncStorage.getItem('geo_location');
              const isFaceRequired = await AsyncStorage.getItem(
                'isFaceRequired',
              );
              const bearer_token = await AsyncStorage.getItem('bearer_token');
              const checked_out = await AsyncStorage.getItem('checked_out');
              const current_travel_checkin = await AsyncStorage.getItem(
                'current_travel_checkin',
              );
              const running_status = await AsyncStorage.getItem(
                'running_status',
              );
              const coordinates = JSON.parse(
                await AsyncStorage.getItem('coordinates'),
              );
              const radius = await AsyncStorage.getItem('radius');
              if (geoLocation === 'Travel Check In') {
                checkedInLocation = '';
              }

              console.log('GEO_ID_CHECKED_IN ==>', checkedInLocation);
              // alert(
              //   'CURRENT_Location ==>' +
              //     geoLocation +
              //     '(' +
              //     checkedInLocation +
              //     ')' +
              //     'with accuracy ' +
              //     position[0].accuracy.toString(),
              // );
              if (Number(position[0].accuracy) >= 40) {
                return;
              }
              if (locations && locations.length > 0) {
                let currentTime = moment.utc().format('HH:mm');
                let checkoutTime = moment(
                  locations[0].checkOutTime,
                  'HH:mm',
                ).format('HH:mm');
                const compareResult = compareTwoTime(currentTime, checkoutTime);
                console.log('compare_back ==>', compareResult);
                if (compareResult) {
                  return ReactNativeForegroundService.stop();
                }
                //return
              }

              const coordinatess = {
                latitude: parseFloat(position[0].latitude),
                longitude: parseFloat(position[0].longitude),
              };
              // if(checked_out !== 'yes'){
              await AsyncStorage.setItem(
                'userCurrentLocation',
                JSON.stringify(coordinatess),
              );
              console.log('coordinates ==', coordinatess);
              const json = await staffCurrentLocation(
                bearer_token,
                institute_id,
                StaffNo,
                coordinatess,
              );
              console.log('json_location_current', json);
              // }
              if (
                current_travel_checkin == 'running' &&
                running_status != 'pause'
              ) {
                const travelLoc = await travelCheckCurrentLocation(
                  bearer_token,
                  institute_id,
                  StaffNo,
                  coordinatess,
                );
                console.log('travelCheckin location updated');
              }
              if (checkedInLocation == '' || !checkedInLocation) {
                if (locations?.length > 0) {
                  locations.forEach(async (location) => {
                    const point = geolib.isPointWithinRadius(
                      {
                        latitude: parseFloat(position[0].latitude),
                        longitude: parseFloat(position[0].longitude),
                      },
                      {
                        latitude: parseFloat(location.coordinates[0].latitude),
                        longitude: parseFloat(
                          location.coordinates[0].longitude,
                        ),
                      },
                      Number(location.radius),
                    );
                    console.log('object', point);
                    if (!point) {
                      return;
                      // alert(
                      //   'You are not in location ' +
                      //     location.accessLocation,
                      // );
                    }
                    await AsyncStorage.setItem(
                      'geo_id',
                      location.id.toString(),
                    );
                    await AsyncStorage.setItem(
                      'geo_location',
                      location.accessLocation,
                    );
                    await AsyncStorage.setItem(
                      'coordinates',
                      JSON.stringify(location.coordinates),
                    );
                    await AsyncStorage.setItem(
                      'radius',
                      location.radius.toString(),
                    );

                    if (isFaceRequired === 'false') {
                      let uploadData = new FormData();

                      //uploadData.append('file', {uri: '', name: 'photo.png', type: 'image/png'});
                      //uploadData.append('file', uri);
                      uploadData.append('StaffNo', StaffNo);
                      uploadData.append(
                        'Latitude',
                        Number(position[0].latitude),
                      );
                      uploadData.append(
                        'Longitude',
                        Number(position[0].longitude),
                      );
                      uploadData.append('InstituteId', institute_id);
                      uploadData.append(
                        'Accuracy',
                        Number(position[0].accuracy),
                      );
                      uploadData.append('IsTravelCheckIn', false);
                      uploadData.append(
                        'IsFaceAuthenticationRequired',
                        'false',
                      );
                      try {
                        const response = await axios({
                          method: 'post',
                          url: Const + 'api/Staff/CheckInWithFormFile',
                          data: uploadData,
                          headers: {'Content-Type': 'multipart/form-data'},
                        });
                        const json = response.data;
                        console.log('CHECKIN', json);
                        if (json.response.status) {
                          await AsyncStorage.setItem('checked_out', 'no');
                          return alert(json.response.message);
                        }
                      } catch (e) {
                        console.log('error', e);
                        return;
                      }
                      return;
                    }
                    // alert('You entered the geofence ' + location.accessLocation);
                    //alert('My request to enter API run...');
                    if (checked_out === 'no' && isFaceRequired === 'true') {
                      try {
                        const response = await fetch(
                          Const + 'api/Staff/StaffInOrOutAfterCheckIn',
                          {
                            method: 'POST',
                            headers: {
                              Accept: 'application/json, text/plain',
                              'Content-Type': 'application/json-patch+json',
                            },
                            body: JSON.stringify({
                              StaffCode: StaffNo,
                              InstituteId: Number(institute_id),
                              IsOutside: false,
                              GeoLocationId: location.id,
                            }),
                          },
                        );
                        const json = await response.json();
                        if (json?.status) {
                          return;
                          // alert(
                          //   'You entered the geofence ' +
                          //     location.accessLocation,
                          // );
                        }
                      } catch (error) {
                        console.log(error);
                        return;
                      }
                    }
                  });
                }
                return;
              }
              //alert('I ran for checking exit');
              const point = geolib.isPointWithinRadius(
                {
                  latitude: parseFloat(position[0].latitude),
                  longitude: parseFloat(position[0].longitude),
                },
                {
                  latitude: parseFloat(coordinates[0].latitude),
                  longitude: parseFloat(coordinates[0].longitude),
                },
                Number(radius),
              );
              console.log('Initial check ==>', point);
              if (point) {
                return;
                // alert(
                //   'You are inside your checked in location ' +
                //     geoLocation,
                // );
              }
              await AsyncStorage.setItem('geo_id', '');
              await AsyncStorage.setItem('coordinates', JSON.stringify([]));
              await AsyncStorage.setItem('radius', '');
              await AsyncStorage.setItem('geo_location', '');
              //alert('My request to exit API run...');
              if (isFaceRequired === 'false') {
                let uploadData1 = new FormData();

                uploadData1.append('StaffNo', StaffNo);
                uploadData1.append('Latitude', Number(position[0].latitude));
                uploadData1.append('Longitude', Number(position[0].longitude));
                uploadData1.append('InstituteId', institute_id);
                uploadData1.append('Accuracy', Number(position[0].accuracy));
                uploadData1.append('IsTravelCheckIn', false);
                uploadData1.append('IsFaceAuthenticationRequired', 'false');

                try {
                  const response = await axios({
                    method: 'post',
                    url: Const + 'api/Staff/CheckOutWithFormFile',
                    data: uploadData1,
                    headers: {'Content-Type': 'multipart/form-data'},
                  });
                  const json = response.data;
                  //alert(JSON.stringify(json));
                  if (json.status) {
                    await AsyncStorage.setItem('checked_out', 'yes');
                    return alert(json.message);
                  }
                  return alert(json.message);
                } catch (e) {
                  console.log('error', e);
                  return alert('Error', e.toString());
                }
              }
              if (checked_out === 'no' && isFaceRequired === 'true') {
                try {
                  const response = await fetch(
                    Const + 'api/Staff/StaffInOrOutAfterCheckIn',
                    {
                      method: 'POST',
                      headers: {
                        Accept: 'application/json, text/plain',
                        'Content-Type': 'application/json-patch+json',
                      },
                      body: JSON.stringify({
                        StaffCode: StaffNo,
                        InstituteId: Number(institute_id),
                        IsOutside: true,
                        GeoLocationId: Number(checkedInLocation),
                      }),
                    },
                  );
                  const json = await response.json();
                  if (json?.status) {
                    return console.log('You exited the location ', geoLocation);
                  }
                  return;
                } catch (error) {
                  console.log(error);
                  return;
                }
              }
            },
          );
        } else {
          locationSubscription && locationSubscription();
          locationTimeout && clearTimeout(locationTimeout);
          console.log('no permissions to obtain location');
        }
      });
    },
    {
      delay: 1000,
      onLoop: true,
      taskId: 'taskid',
      onError: (e) => console.log('Error logging:', e),
    },
  );
};

export const getStatus = async (bearer_token, StaffNo, institute_id) => {
  //console.log(bearer_token, institute_id, StaffNo)
  return fetch(Const + 'api/Staff/IsCheckedInNew', {
    method: 'POST',
    withCredentials: true,
    credentials: 'include',
    headers: {
      Authorization: bearer_token,
      Accept: 'text/plain',
      'Content-Type': 'application/json-patch+json',
    },
    body: JSON.stringify({
      staffCode: StaffNo,
      instituteId: Number(institute_id),
    }),
  })
    .then((response) => response.json())
    .then(async (json) => {
      //console.log('jOSN=-==', json);
      if (json && json.isCheckedIn) {
        return true;
      }
      return false;
    });
};

export const staffCurrentLocation = async (
  bearer_token,
  institute_id,
  StaffNo,
  coordinates,
) => {
  return fetch(Const + 'api/GeoFencing/addOrUpdate/stafflocation', {
    method: 'POST',
    withCredentials: true,
    credentials: 'include',
    headers: {
      Authorization: 'Bearer ' + bearer_token,
      Accept: 'application/json, text/plain',
      'Content-Type': 'application/json-patch+json',
    },
    body: JSON.stringify({
      InstituteId: institute_id,
      StaffCode: StaffNo,
      Coordinates: coordinates,
    }),
  })
    .then((response) => response.json())
    .then((json) => {
      return json;
    })
    .catch((error) => {});
};

export const travelCheckCurrentLocation = async (
  bearer_token,
  institute_id,
  StaffNo,
  coordinates,
) => {
  const currentTravelLocation = JSON.parse(
    await AsyncStorage.getItem('currentTravelLocation'),
  );
  const radius = 100;
  let idealValue = false;
  if (currentTravelLocation) {
    AsyncStorage.setItem('currentTravelLocation', JSON.stringify(coordinates));
    const point = geolib.isPointWithinRadius(
      {
        latitude: parseFloat(currentTravelLocation.latitude),
        longitude: parseFloat(currentTravelLocation.longitude),
      },
      {
        latitude: parseFloat(coordinates.latitude),
        longitude: parseFloat(coordinates.longitude),
      },
      radius,
    );
    if (point) {
      idealValue = true;
    }
  }
  let Address = '';
  Geocoder.init(GOOGLE_MAPS_APIKEY);
  Geocoder.from(coordinates.latitude, coordinates.longitude)
    .then(async (json) => {
      Address = json.results[0].formatted_address;
      console.log('Address', Address, 'idealVal =', idealValue);
      try {
        const response = await fetch(Const + 'api/GeoFencing/travelcheckin', {
          method: 'POST',
          withCredentials: true,
          credentials: 'include',
          headers: {
            Authorization: 'Bearer ' + bearer_token,
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            InstituteId: institute_id,
            StaffCode: StaffNo,
            Coordinates: coordinates,
            Address,
            IsCheckIn: false,
            IsCheckOut: false,
            IsIdeal: idealValue,
          }),
        });
        const json_1 = await response.json();
        return json_1;
      } catch (error) {
        return error;
      }
    })
    .catch((e) => console.log(e));
  return;
};

export const compareTwoTime = (t1, t2) => {
  let time1 = moment(t1, 'h:mma');
  let time2 = moment(t2, 'h:mma');
  if (time1.isSameOrAfter(time2)) {
    return true;
  }
  return false;
};

export const loginAPI = async () => {
  try {
    const mobileNumber = await AsyncStorage.getItem('mobile');
    const mpin = await AsyncStorage.getItem('mpin');
    const response = await axios.post(
      Const + 'api/Security/Mobilelogin',
      {
        mobileNumber,
        mpin,
      },
      {
        headers: {
          'Content-Type': 'application/json-patch+json',
        },
      },
    );
    const json = response.data;
    if (json.isAuthenticated) {
      await AsyncStorage.setItem('menus', JSON.stringify(json.menus));
      await AsyncStorage.setItem('app_status', this.state.app_status);
      await AsyncStorage.setItem('user_id', json.staffNumber);
      await AsyncStorage.setItem('bearer_token', json.bearerToken);
      await AsyncStorage.setItem('institute_id', json.instituteId.toString());
      await AsyncStorage.setItem('org_id', json.organisationId.toString());
      await AsyncStorage.setItem(
        'driving_type',
        json.travelPreference.toString(),
      );
      await AsyncStorage.setItem(
        'start_time',
        json.staffTravelStartTime.toString(),
      );
      await AsyncStorage.setItem(
        'profile_pic',
        json.profilePic ? json.profilePic : '',
      );
      await AsyncStorage.setItem(
        'base64Data',
        json.profilePicBase64 ? json.profilePicBase64 : '',
      );
      if (json.isTravelCheckInAvailable) {
        await AsyncStorage.setItem('travel_check_in', 'yes');
      } else {
        await AsyncStorage.setItem('travel_check_in', 'no');
      }
      if (json.isQrCheckIn) {
        await AsyncStorage.setItem('qrCheckin', 'true');
      } else {
        await AsyncStorage.setItem('qrCheckin', 'false');
      }
      if (json.isQrCheckOut) {
        await AsyncStorage.setItem('qrCheckout', 'true');
      } else {
        await AsyncStorage.setItem('qrCheckout', 'false');
      }
      if (json.isFaceAuthenticationRequired) {
        await AsyncStorage.setItem('isFaceRequired', 'true');
      } else {
        await AsyncStorage.setItem('isFaceRequired', 'false');
      }
      await addBoundaries(json.staffNumber, json.instituteId);
    }
  } catch (e) {
    console.log('error', e);
  }
};

export const imageSizeBasedOnCameraPixel = (pixel) => {
  if (pixel >= 13) {
    return {
      width: 80,
      height: 80,
    };
  }
  return {
    width: 300,
    height: 300,
  };
};

export const getCameraPixel = () => {
  CameraMP.getBackCameraResolutionInMp();
};
