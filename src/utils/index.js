import React, {useState} from 'react';
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';
import {GOOGLE_MAPS_APIKEY} from './configs/Constants';
Geocoder.init(GOOGLE_MAPS_APIKEY);
export const coordinatestoaddress = () => {
  Geolocation.getCurrentPosition(
    (position) => {
      var coordinates = {
        latitude: parseFloat(position.coords.latitude),
        longitude: parseFloat(position.coords.longitude),
      };
      Geocoder.from(coordinates.latitude, coordinates.longitude)
        .then((json) => {
          return json.results[0].formatted_address;
        })
        .catch((error) => console.log(error));
    },
    (error) => {},
    {enableHighAccuracy: true, timeout: 2000, maximumAge: 0},
  );
};
