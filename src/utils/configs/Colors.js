export const Colors = {
  header: '#f05760',
  white: '#ffffff',
  overlay: 'rgba(0.5, 0.25, 0, 0.2)',
  maroon: '#cb4154',
};
