import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../components/Home';
import FaceRegister from '../components/Face/FaceRegister';
import FaceRegister1 from '../components/Face/FaceBackup';
import Checkin from '../components/Face/Checkin';
import LiveCheckin from '../components/Face/LiveCheckin';
import LiveCheckout from '../components/Face/LiveCheckOut';
import OtherCheckin from '../components/Face/OtherCheckIn';
import Checkout from '../components/Face/Checkout';
import OtherCheckout from '../components/Face/OtherCheckOut';
import StaffTasks from '../components/Face/StaffTasks';
import OtherStaffTasks from '../components/Face/OtherStaffTasks';
import MainNavigator from './MainNavigator';
import MapFencing from '../components/MapFencing';
import Circular from '../components/Circular/Circular';
import CircularStep2 from '../components/Circular/CircularStep2';
import CircularStep3 from '../components/Circular/CircularStep3';
import CircularList from '../components/Circular/CircularList';
import Privacy from '../components/Privacy';
import SelectInstitute from '../components/SelectInstitute';
import SaveFencing from '../components/SaveFencing';
import FencingList from '../components/FencingList';
//import EditMapFencing from '../components/EditMapFencing';
import MyProfile from '../components/MyProfile';
import CircleMapView from '../components/Profile/CircleMapView';
import PolygonMapView from '../components/Profile/PolygonMapView';
import StaffReports from '../components/Face/StaffReports';
import Reports from '../components/Face/Reports';
import LocateMenu from '../components/LocateStaff/LocateMenu';
import LocateMenuStep1 from '../components/LocateStaff/LocateMenuStep1';
import LocateMenuStep2 from '../components/LocateStaff/LocateMenuStep2';
import DistanceMapView from '../components/LocateStaff/DistanceMapView';
import ImageViewer from '../components/ImageViewer';
//import FileDownload from '../components/FileDownload';
import StaffAttendance from '../components/ManagerReports/StaffAttendance';
import AllStaffPerShiftReport from '../components/ManagerReports/AllStaffPerShiftReport';
import StaffActivities from '../components/ManagerReports/StaffActivities';
import QRCodeGenerator from '../components/Face/QRCodeGenerator';
import StaffLocation from '../components/ManagerReports/StaffLocation';

import LeaveRequest from '../components/Requests/LeaveRequest';
import OutsideDuty from '../components/Requests/OutsideDuty';
import Permission from '../components/Requests/Permission';
import Complaints from '../components/Requests/Complaints';
import Punch from '../components/Requests/Punch';
import Leaves from '../components/Requests/Leaves';

const Stack = createStackNavigator();
export default function AppStack() {
  return (
    <Stack.Navigator headerMode="none" initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="FaceRegister" component={FaceRegister} />
      <Stack.Screen name="FaceRegister1" component={FaceRegister1} />
      <Stack.Screen name="Checkin" component={Checkin} />
      <Stack.Screen name="LiveCheckin" component={LiveCheckin} />
      <Stack.Screen name="OtherCheckin" component={OtherCheckin} />
      <Stack.Screen name="Checkout" component={Checkout} />
      <Stack.Screen name="LiveCheckOut" component={LiveCheckout} />
      <Stack.Screen name="OtherCheckout" component={OtherCheckout} />
      <Stack.Screen name="StaffTasks" component={StaffTasks} />
      <Stack.Screen name="OtherStaffTasks" component={OtherStaffTasks} />
      <Stack.Screen name="MainNavigator" component={MainNavigator} />
      <Stack.Screen name="MapFencing" component={MapFencing} />
      <Stack.Screen name="Circular" component={Circular} />
      <Stack.Screen name="CircularStep2" component={CircularStep2} />
      <Stack.Screen name="CircularStep3" component={CircularStep3} />
      <Stack.Screen name="CircularList" component={CircularList} />
      <Stack.Screen name="Privacy" component={Privacy} />
      <Stack.Screen name="SelectInstitute" component={SelectInstitute} />
      <Stack.Screen name="SaveFencing" component={SaveFencing} />
      <Stack.Screen name="FencingList" component={FencingList} />

      <Stack.Screen name="MyProfile" component={MyProfile} />
      <Stack.Screen name="CircleMapView" component={CircleMapView} />
      <Stack.Screen name="PolygonMapView" component={PolygonMapView} />
      <Stack.Screen name="Reports" component={Reports} />
      <Stack.Screen name="QRCodeGenerator" component={QRCodeGenerator} />
      <Stack.Screen name="ManagerReports" component={StaffReports} />
      <Stack.Screen name="StaffReports" component={AllStaffPerShiftReport} />
      <Stack.Screen name="StaffAttendance" component={StaffAttendance} />
      <Stack.Screen name="StaffActivities" component={StaffActivities} />
      <Stack.Screen name="StaffLocation" component={StaffLocation} />
      <Stack.Screen name="LocateMenu" component={LocateMenu} />
      <Stack.Screen name="LocateMenuStep1" component={LocateMenuStep1} />
      <Stack.Screen name="LocateMenuStep2" component={LocateMenuStep2} />
      <Stack.Screen name="DistanceMapView" component={DistanceMapView} />
      <Stack.Screen name="ImageViewer" component={ImageViewer} />

      <Stack.Screen name="LeaveRequest" component={LeaveRequest} />
      <Stack.Screen name="OutsideDuty" component={OutsideDuty} />
      <Stack.Screen name="Permission" component={Permission} />
      <Stack.Screen name="Complaints" component={Complaints} />
      <Stack.Screen name="Punch" component={Punch} />
      <Stack.Screen name="Leaves" component={Leaves} />
      {/* <Stack.Screen name="FileDownload" component={FileDownload} /> */}
    </Stack.Navigator>
  );
}
