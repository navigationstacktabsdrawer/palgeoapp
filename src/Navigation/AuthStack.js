import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../components/Auth/Login';
import RegisterPin from '../components/Auth/RegisterPin'; 
import MainNavigator from './MainNavigator';
import OtpVerification from '../components/Auth/OtpVerification'; 
import SetMpin from '../components/Auth/SetMpin'; 
import Privacy from '../components/Privacy';
import StaffDetails from '../components/Auth/StaffDetails'; 
import SearchCountry from '../components/Auth/SearchCountry'; 
const Stack = createStackNavigator()
export default class AuthStack extends React.Component { 
    render(){
        return (
            <Stack.Navigator headerMode="none" initialRouteName="Login">
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="MainNavigator" component={MainNavigator} />
                <Stack.Screen name="RegisterPin" component={RegisterPin} />
                <Stack.Screen name="OtpVerification" component={OtpVerification} />
                <Stack.Screen name="SetMpin" component={SetMpin} />
                <Stack.Screen name="Privacy" component={Privacy} /> 
                <Stack.Screen name="StaffDetails" component={StaffDetails} /> 
                <Stack.Screen name="SearchCountry" component={SearchCountry} />
            </Stack.Navigator> 
         )   
    }
}
   