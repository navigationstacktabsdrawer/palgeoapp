import React, {Component} from 'react';
import {StyleSheet, Image, TouchableWithoutFeedback} from 'react-native';
import {
  Container,
  List,
  ListItem,
  Text,
  Content,
  Right,
  Left,
} from 'native-base';
import SubHeader from './common/SubHeader';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from './common/Loader';
import Const from './common/Constants';
import AwesomeAlert from 'react-native-awesome-alerts';
export default class FencingList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      data: [],
      bearer_token: '',
      showAlert1: false,
      error_message: '',
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('bearer_token').then((bearer_token) => {
      this.setState({bearer_token: bearer_token});
      this.getList(this.props.route.params.institute_id, bearer_token);
    });
  }
  getList = (institute, bearer_token) => {
    if (institute) {
      this.setState({loader: true});
      fetch(Const + 'api/GeoFencing/' + institute, {
        method: 'GET',
        withCredentials: true,
        credentials: 'include',
        headers: {
          Authorization: 'Bearer ' + bearer_token,
          Accept: 'application/json, text/plain',
          'Content-Type': 'application/json-patch+json',
        },
      })
        .then((response) => response.json())
        .then((json) => {
          this.setState({loader: false});
          if (json.rows.length > 0) {
            this.setState({data: json.rows});
          } else {
            this.setState({
              data: [],
              showAlert1: true,
              error_message: 'No data found',
              showAlert: false,
            });
          }
        })
        .catch((error) => {
          if (Platform.OS == 'ios') {
            alert('Unknown error occured');
            setTimeout(() => {
              this.setState({showAlert: false});
            }, 1200);
          } else {
            this.setState({
              showAlert1: true,
              error_message: 'Unknown error occured',
              showAlert: false,
            });
          }
        });
    } else {
      if (Platform.OS == 'ios') {
        alert('Please click on the list icon to view geo fencing list');
        setTimeout(() => {
          this.setState({showAlert: false});
        }, 1200);
      } else {
        this.setState({
          showAlert1: true,
          error_message:
            'Please click on the list icon to view geo fencing list',
          showAlert: false,
        });
      }
    }
  };
  removeFence = (id, index1) => {
    var tempArr = this.state.data;
    tempArr[index1].isDeleted = true;
    this.setState({data: tempArr}, () => {
      this.saveFence();
    });
  };
  saveFence = () => {
    this.setState({loader: true});
    fetch(
      Const +
        'api/GeoFencing/addOrUpdate/geoFencing/' +
        this.props.route.params.institute_id,
      {
        method: 'POST',
        withCredentials: true,
        credentials: 'include',
        headers: {
          Authorization: 'Bearer ' + this.state.bearer_token,
          Accept: 'application/json, text/plain',
          'Content-Type': 'application/json-patch+json',
        },
        body: JSON.stringify(this.state.data),
      },
    )
      .then((response) => response.json())
      .then((json) => {
        this.setState({loader: false});
        if (json.status) {
          if (Platform.OS == 'ios') {
            alert('Fencing removed successfully');
            this.getList(
              this.props.route.params.institute_id,
              this.state.bearer_token,
            );
            setTimeout(() => {
              this.setState({showAlert: false});
            }, 1200);
          } else {
            this.setState(
              {
                showAlert1: true,
                error_message: 'Fencing removed successfully',
                showAlert: false,
              },
              () => {
                this.getList(
                  this.props.route.params.institute_id,
                  this.state.bearer_token,
                );
              },
            );
          }
        } else {
          if (Platform.OS == 'ios') {
            alert(json.message);
            setTimeout(() => {
              this.setState({showAlert: false});
            }, 1200);
          } else {
            this.setState({
              showAlert1: true,
              error_message: json.message,
              showAlert: false,
            });
          }
        }
      })
      .catch((error) => {
        if (Platform.OS == 'ios') {
          alert('Unknown error occured');
          setTimeout(() => {
            this.setState({showAlert: false});
          }, 1200);
        } else {
          this.setState({
            showAlert1: true,
            error_message: 'Unknown error occured',
            showAlert: false,
          });
        }
      });
  };
  EditFence = (item) => {
    this.props.navigation.navigate('EditMapFencing', {
      data: item,
      institute_id: this.props.route.params.institute_id,
    });
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    return (
      <Container>
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <SubHeader
          title="Geo Fencing Saved List"
          showBack={true}
          backScreen="SelectInstitute"
          navigation={this.props.navigation}
        />
        <Content>
          <List>
            {this.state.data.map((item, index) => {
              return (
                <ListItem key={item.id}>
                  <Left>
                    <Text style={styles.name}>{item.accessLocation}</Text>
                  </Left>

                  <Right style={{flexDirection: 'row'}}>
                    {/* <TouchableWithoutFeedback onPress={()=>this.EditFence(item)}>
                                        <Image source={require('../assets/ic_edit.png')}  style={{width:28,height:28}}   />
                                    </TouchableWithoutFeedback> */}

                    {/* <TouchableWithoutFeedback onPress={()=>this.removeFence(item.id,index)}>
                                        <Image source={require('../assets/ic_delete.png')}  style={{width:28,height:28,marginLeft : '10%'}}   />
                                    </TouchableWithoutFeedback> */}
                  </Right>
                </ListItem>
              );
            })}
          </List>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  name: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
  },
  date: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12.5,
    margin: '1%',
    color: '#909090',
  },
});
