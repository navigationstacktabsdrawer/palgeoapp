import {Container, Content, Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SubHeader from '../common/SubHeader';
import CustomPicker from '../common/CustomPicker';
import {CustomList} from '../common/CustomList';
import {CustomButton} from '../common/CustomButton';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {CustomList2} from '../common/CustomList2';
import {CustomTextArea} from '../common/CustomTextArea';
import {ApprovalStages} from '../common/ApprovalStages';
import {CalenderView} from '../common/CalenderView';
import axios from 'axios';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
const SubThemeColor = '#FFEDEE';
const ThemeColor = '#F15761';
const baseUrl = 'http://demoworks.in/php/nav/';
export default class Complaints extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveType: '',
      ApprovalStages: [],
      SuggestionSummary: '',
      Complaint: '',
      Attachment: '',
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        //alert(value);
        this.setState({StaffCode: value}, function () {
          //this.GetMasterLeaveApproverDetails();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };
  GetMasterLeaveApproverDetails = async () => {
    //const url =
    //  Const +
    //  'api​/Leave​/GetMasterLeaveApproverDetails' +
    //  '​/' +
    //  parseInt(this.state.institute_id) +
    //  '​/' +
    //  this.state.StaffCode +
    //  '​/1';
    const url =
      'https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetails/2/PW100/1026';
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {
          Loader: false,
          // GetMasterLeaveApproverDetails​: response.data
        },
        function () {},
      );
    } catch (error) {
      alert(error.message);
    }
  };
  AddUpdateComplaint = async () => {
    const url = Const + 'api/Leave/AddUpdateComplaint';
    console.log(url);
    //const headers = {
    //  Accept: "application/json, text/plain",
    //  "Content-Type": "application/json-patch+json",
    //};

    let body = {
      Id: 0,
      RequestTypeId: 0,
      StaffCode: this.state.StaffCode.toString(),
      //StaffCode: 'PW100',

      InstituteId: parseInt(this.state.institute_id),
      CreatedDate: new Date().toISOString(),
      ModifiedDate: new Date().toISOString(),
      ComplaintSummary: this.state.Complaint,
      SuggestionSummary: this.state.SuggestionSummary,
    };
    console.log(JSON.stringify(body));
    try {
      const response = await axios.post(url, body);
      console.log(JSON.stringify(response.data));
      this.setState(
        {
          Loader: false,
          //AddUpdateComplaint: response.data
        },
        function () {
          alert(response.data.message);
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Complaints"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View style={{marginTop: 20}}>
              <Text style={[styles.label, {margin: 10}]}>Complaint</Text>

              <TextInput
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                style={{
                  alignSelf: 'center',
                  width: '90%',
                  height: 40,
                  padding: 10,
                  borderRadius: 10,
                }}
                placeholder="Name a complaint"
                value={this.state.Complaint}
                blurOnSubmit={true}
                onChangeText={(text) => {
                  this.setState({Complaint: text});
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <CustomTextArea
                onPress2={() => {}}
                SelectedImage={(Attachment) => {
                  this.setState({Attachment});
                }}
                text={this.state.Attachment.FileName || 'No files attached'}
                title="Suggestion"
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                width="95%"
                placeholder="Write your suggestion"
                value={this.state.SuggestionSummary}
                onChangeText={(text) => {
                  this.setState({SuggestionSummary: text});
                }}
                onPress={() => {
                  this.AddUpdateComplaint();
                }}
              />
            </View>
            <View style={{marginVertical: 20}}>
              {this.state.ApprovalStages.length > 0 && (
                <ApprovalStages
                  onPress={() => {}}
                  width="95%"
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.ApprovalStages}
                />
              )}
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
});
