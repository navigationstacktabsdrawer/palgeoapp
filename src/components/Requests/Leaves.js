import {Container, Content, Thumbnail, Fab, Icon, Button} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
  FlatList,
} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {CustomTabs} from '../common/CustomTabs';
import SubHeader from '../common/SubHeader';
import Nodata from '../common/NoData';
const SubThemeColor = '#FFEDEE';
const ThemeColor = '#F15761';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Leaves extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveType: '',
      options: [{type: 'Sick Leave', id: 0}],
      LeaveArr: [
        {type: 'Sick Leave', pending: 1, total: 2},
        {type: 'Casual Leave', pending: 2, total: 2},
        {type: 'Medical Leave', pending: 2, total: 3},
        {type: 'Personal Leave', pending: 1, total: 1},
      ],
      ApprovalStages: [],
      ActiveTab: 'Need to approve',
    };
  }
  renderItem = ({item, index}) => {
    return (
      <View
        key={index}
        style={{
          width: '95%',
          alignSelf: 'center',
          margin: 5,
          borderRadius: 10,
          backgroundColor: index % 2 ? 'white' : SubThemeColor,
        }}>
        <View
          style={{
            width: '100%',
            alignSelf: 'center',
            flexDirection: 'row',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <View
            style={[
              styles.headerTitleContainer,
              {
                alignItems: 'flex-start',
                paddingLeft: 20,
                flex: 0.2,
              },
            ]}>
            <Text style={[styles.text, {color: 'black'}]}>1</Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-evenly',
            }}>
            <Thumbnail
              small
              square
              style={{borderRadius: 10}}
              source={require('../../assets/taskall.jpg')}
            />
            <View>
              <Text style={[styles.text, {color: ThemeColor}]}>
                Catherine Wats
              </Text>
              <Text style={[styles.text, {color: ThemeColor, fontSize: 12}]}>
                Project Manager
              </Text>
            </View>
          </View>

          <View
            style={[
              styles.headerTitleContainer,
              {alignItems: 'center', flex: 0.5},
            ]}>
            <Text style={[styles.text, {color: ThemeColor}]}>Leave</Text>
            <Text style={[styles.text, {color: 'black'}]}>2/4</Text>
          </View>
        </View>
        <View style={{width: '58%', alignSelf: 'center'}}>
          <Text
            numberOfLines={4}
            style={[
              styles.text,
              {
                color: '#7B7B7B',
                fontSize: 12,
                marginRight: 20,
                alignSelf: 'center',
                marginTop: 5,
              },
            ]}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry standard dummy text ever
            since the 1500s, when an unknown printer took a galley of type and
            scrambled it to make a type specimen book. It has survived not only
            five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={[
              styles.headerTitleContainer,
              {
                alignItems: 'flex-start',
                paddingLeft: 10,
                flex: 0.5,
              },
            ]}></View>
          <View
            style={[styles.headerTitleContainer, {alignItems: 'flex-start'}]}>
            <Text
              style={[
                styles.text,
                {
                  color: 'black',
                  fontSize: 12,
                  marginRight: 20,
                  marginVertical: 5,
                },
              ]}>
              No. of days 4
            </Text>
            <Text
              style={[
                styles.text,
                {
                  color: ThemeColor,
                  fontSize: 12,
                  marginRight: 20,
                  marginBottom: 5,
                },
              ]}>
              25th May,2021
            </Text>
          </View>
          <View
            style={[
              styles.headerTitleContainer,
              {alignItems: 'flex-end', marginRight: 20},
            ]}>
            <View
              style={{
                backgroundColor: '#D8F4F4',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
                paddingHorizontal: 8,
                paddingVertical: 4,
              }}>
              <Text style={[styles.text, {color: '#23C4D7', fontSize: 13}]}>
                Pending
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Leave Dashboard"
            showBack={true}
            backScreen="Home"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <CustomTabs
              borderRadius={20}
              height={50}
              color={ThemeColor}
              backgroundColor={SubThemeColor}
              ActiveTab={this.state.ActiveTab}
              tab1="Need to approve"
              tab2="Requested"
              onPress={(value) => {
                this.setState({ActiveTab: value}, function () {
                  console.log('Selected tab = ', value);
                });
              }}
            />
            <View
              style={{
                width: '95%',
                alignSelf: 'center',
                backgroundColor: SubThemeColor,
                borderRadius: 10,
              }}>
              <View
                style={[styles.headerContainer, {backgroundColor: ThemeColor}]}>
                <View
                  style={[
                    styles.headerTitleContainer,
                    {alignItems: 'flex-start', paddingLeft: 10, flex: 0.5},
                  ]}>
                  <Text style={styles.text}>Sr.No</Text>
                </View>
                <View style={styles.headerTitleContainer}>
                  <Text style={styles.text}>Need to approve</Text>
                </View>
                <View
                  style={[styles.headerTitleContainer, {alignItems: 'center'}]}>
                  <Text style={styles.text}>Type/Stage</Text>
                </View>
              </View>
              <FlatList
                data={this.state.LeaveArr}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index}
                contentContainerStyle={{
                  backgroundColor: '#ffffff',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                ListEmptyComponent={
                  <View style={{marginTop: hp('40')}}>
                    <Nodata title="No Data Found" />
                  </View>
                }
              />
            </View>
          </Content>
        </Container>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{
            backgroundColor: this.state.active ? 'white' : 'transparent',
            minWidth: 180,
            marginRight: -10,
            paddingTop: 20,
            borderRadius: 10,
            marginTop: 10,
            alignItems: 'flex-end',
          }}
          style={{backgroundColor: ThemeColor}}
          position="bottomRight"
          onPress={() => this.setState({active: !this.state.active})}>
          <Icon name="add" />
          <Button
            onPress={() => {
              this.props.navigation.navigate('LeaveRequest');
            }}
            style={styles.buttonStyle}>
            <Image
              style={styles.imageStyle}
              source={require('../../assets/leave.png')}
            />
            <Text style={styles.text2}>Leave</Text>
          </Button>
          <Button
            onPress={() => {
              this.props.navigation.navigate('OutsideDuty');
            }}
            style={styles.buttonStyle}>
            <Image
              style={styles.imageStyle}
              source={require('../../assets/od.png')}
            />
            <Text style={styles.text2}>Outside Duty</Text>
          </Button>
          <Button
            onPress={() => {
              this.props.navigation.navigate('Permission');
            }}
            style={styles.buttonStyle}>
            {/*<Icon name="mail" />*/}
            <Image
              style={styles.imageStyle}
              source={require('../../assets/permission.png')}
            />
            <Text style={styles.text2}>Permission</Text>
          </Button>
          <Button
            onPress={() => {
              this.props.navigation.navigate('Complaints');
            }}
            style={styles.buttonStyle}>
            <Image
              style={styles.imageStyle}
              source={require('../../assets/complaint.png')}
            />
            <Text style={styles.text2}>Grievance</Text>
          </Button>
          <Button
            onPress={() => {
              this.props.navigation.navigate('Punch');
            }}
            style={styles.buttonStyle}>
            <Image
              style={styles.imageStyle}
              source={require('../../assets/punch.png')}
            />
            <Text style={styles.text2}>Missed Punch</Text>
          </Button>
        </Fab>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'white',
  },
  text2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'black',
    fontWeight: 'bold',
  },
  buttonStyle: {
    backgroundColor: SubThemeColor,
    //backgroundColor: "rgba(0, 0, 0, 0.1)",
    width: 200,
    //marginLeft: -100,
    justifyContent: 'flex-start',
  },
  imageStyle: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginHorizontal: 10,
  },
  headerContainer: {
    width: '100%',
    //height: 40,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    //paddingHorizontal: 10,
    paddingVertical: 10,
  },
  headerTitleContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
