import {Container, Content, Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SubHeader from '../common/SubHeader';
//import CustomPicker from '../common/CustomPicker';
//import {CustomList} from '../common/CustomList';
//import {CustomButton} from '../common/CustomButton';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
//import {CustomList2} from '../common/CustomList2';
import {CustomTextArea} from '../common/CustomTextArea';
import {ApprovalStages} from '../common/ApprovalStages';
import {CalenderView} from '../common/CalenderView';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Const from '../common/Constants';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
const SubThemeColor = '#FFEDEE';
const ThemeColor = '#F15761';
const screenWidth = Dimensions.get('window').width;
export default class Permission extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveType: '',
      date: moment().format('YYYY-MM-DD'),
      time: new Date(),
      time2: new Date(),
      ApprovalStages: [],
      showFirstTime: false,
      showSecondTime: false,
      Attachment: {
        FileName: '',
        FileType: '',
        Attachment: '',
      },
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        //alert(value);
        this.setState({StaffCode: value}, function () {
          // this.GetMasterLeaveApproverDetails();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };
  GetMasterLeaveApproverDetails = async () => {
    //const url =
    //  Const +
    //  'api​/Leave​/GetMasterLeaveApproverDetails' +
    //  '​/' +
    //  parseInt(this.state.institute_id) +
    //  '​/' +
    //  this.state.StaffCode +
    //  '​/1';
    const url =
      'https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetails/2/PW100/1026';
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {
          Loader: false,
          // GetMasterLeaveApproverDetails​: response.data
        },
        function () {
          alert(response?.data?.message || '');
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };
  AddOrUpdateLeaveRequest = async () => {
    const url = Const + 'api/Leave/AddOrUpdateLeaveRequest';
    console.log(url);
    let bodyData = {
      Id: 0,
      InstituteId: parseInt(this.state.institute_id),
      StaffCode: this.state.StaffCode.toString(),
      LeaveId: 0,
      LeaveTypeId: 0,
      //FromDate: "2021-07-07T03:00:21.436Z",
      //EndDate: "2021-07-07T03:00:21.436Z",
      FromDate: this.state.time,
      EndDate: this.state.time2,
      FromTime: this.state.time,
      EndTime: this.state.time2,
      Reason: this.state.Reason,
      LeaveAttachment: this.state.Attachment,
      NoOfDays: 1,
      Status: 0,
      CreatedDate: new Date(),
      ModifiedDate: new Date(),
    };
    console.log(JSON.stringify(bodyData));
    try {
      const response = await axios.post(url, bodyData);
      console.log('working');
      console.log(JSON.stringify(response.data));
      this.setState(
        {Loader: false, AddUpdateLeaveRequestNew: response.data},
        function () {
          alert(response.data.message);
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };

  handleConfirm1 = (time) => {
    console.log('time', time);
    this.setState({time}, () => this.hideDatePicker1());
  };
  handleConfirm2 = (time) => {
    this.setState({time2: time}, () => this.hideDatePicker2());
  };

  hideDatePicker1 = () => {
    this.setState({showFirstTime: false});
  };
  hideDatePicker2 = () => {
    this.setState({showSecondTime: false});
  };

  render() {
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Permission"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '95%',
                flexDirection: 'row',
                marginVertical: 10,
              }}>
              <CalenderView
                mainTitle="Total availed"
                title="PERMISSION"
                date="03"
                backgroundColor="#FD8991"
                width={120}
              />
              <CalenderView
                mainTitle="Total availed"
                title="PERMISSION"
                date="10"
                backgroundColor="#FD8991"
                width={120}
              />
            </View>
            {/* <View style={[styles.labelContainer, {marginLeft: 10}]}>
              <Text style={styles.label}>Date</Text>
            </View> */}
            {/* <DatePicker
              showIcon={false}
              style={{width: '45%%', marginLeft: 10}}
              date={this.state.date}
              mode="date"
              placeholderTextColor={ThemeColor}
              placeholder={moment(this.state.date)
                .format('DD/MM/YYYY')
                .toString()}
              format="YYYY-MM-DD"
              minDate="2016-05-01"
              maxDate={moment().format('YYYY-MM-DD').toString()}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateInput: {
                  //marginLeft: 36
                  borderColor: 'transparent',
                  borderRadius: 10,
                  marginTop: 10,
                  //  marginRight: 36,
                  padding: 0,
                  color: ThemeColor,
                  backgroundColor: SubThemeColor,
                },
                dateText: {
                  color: ThemeColor,
                  textAlign: 'center',
                  fontFamily: 'Poppins-Regular',
                },
                //... You can check the source to find the other keys.
              }}
              onDateChange={(date) => {
                this.setState({date: date});
              }}
            /> */}

            <View
              style={{
                width: '95%',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                flexDirection: 'row',
                alignSelf: 'center',
                marginVertical: 15,
                //marginTop: "4%",
              }}>
              <TouchableOpacity
                onPress={() => this.setState({showFirstTime: true})}>
                <View style={{width: '100%'}}>
                  <View style={styles.labelContainer}>
                    <Text style={styles.label}>From Time</Text>
                    <View
                      style={{
                        width: screenWidth / 2.3,
                        backgroundColor: SubThemeColor,
                        height: 40,
                        borderRadius: 5,
                        justifyContent: 'center',
                        //alignItems: 'center',
                        paddingLeft: 10,
                      }}>
                      <Text style={[styles.label, {color: ThemeColor}]}>
                        {moment(this.state.time).format('h:mm a')}
                      </Text>
                    </View>
                  </View>
                  <DateTimePickerModal
                    isVisible={this.state.showFirstTime}
                    mode="time"
                    onConfirm={this.handleConfirm1}
                    onCancel={this.hideDatePicker1}
                  />

                  {/* <DatePicker
                  showIcon={false}
                  style={{width: '95%'}}
                  date={this.state.time}
                  mode="datetime"
                  placeholderTextColor={ThemeColor}
                  placeholder={moment(this.state.time)
                    .format('hh:mm')
                    .toString()}
                  //format="hh:mm"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      borderColor: 'transparent',
                      borderRadius: 10,
                      marginTop: 10,
                      //  marginRight: 36,
                      padding: 0,
                      backgroundColor: SubThemeColor,
                    },
                    dateText: {
                      color: ThemeColor,
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                    },
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(time) => {
                    console.log(time);
                    this.setState({time: time});
                  }}
                /> */}
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.setState({showSecondTime: true})}>
                <View style={{width: '100%'}}>
                  <View style={styles.labelContainer}>
                    <Text style={styles.label}>To Time</Text>
                    <View
                      style={{
                        width: screenWidth / 2.3,
                        backgroundColor: SubThemeColor,
                        height: 40,
                        borderRadius: 5,
                        justifyContent: 'center',
                        paddingLeft: 10,
                        //alignItems: 'center',
                      }}>
                      <Text style={[styles.label, {color: ThemeColor}]}>
                        {moment(this.state.time2).format('h:mm a')}
                      </Text>
                    </View>
                  </View>

                  {/* <DatePicker
                  showIcon={false}
                  style={{width: '100%'}}
                  date={this.state.time2}
                  mode="datetime"
                  placeholderTextColor={ThemeColor}
                  placeholder={moment(this.state.time2)
                    .format('hh:mm')
                    .toString()}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      //marginLeft: 36
                      borderColor: 'transparent',
                      borderRadius: 10,
                      marginTop: 10,
                      //  marginRight: 36,
                      padding: 0,
                      backgroundColor: SubThemeColor,
                    },
                    dateText: {
                      color: ThemeColor,
                      textAlign: 'center',
                      fontFamily: 'Poppins-Regular',
                    },
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(time2) => {
                    console.log(time2);
                    this.setState({time2: time2});
                  }}
                /> */}
                  <DateTimePickerModal
                    isVisible={this.state.showSecondTime}
                    mode="time"
                    onConfirm={this.handleConfirm2}
                    onCancel={this.hideDatePicker2}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={{marginTop: 20}}>
              <CustomTextArea
                onPress2={() => {}}
                SelectedImage={(Attachment) => {
                  this.setState({Attachment});
                }}
                title="Reason"
                text={this.state.Attachment.FileName || 'No files attached'}
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                width="95%"
                placeholder="Write reason here"
                value={this.state.Reason}
                onChangeText={(text) => {
                  this.setState({Reason: text});
                }}
                onPress={() => {
                  this.AddOrUpdateLeaveRequest();
                }}
              />
            </View>
            <View style={{marginVertical: 20}}>
              {this.state.ApprovalStages.length > 0 && (
                <ApprovalStages
                  onPress={() => {}}
                  width="95%"
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.ApprovalStages}
                />
              )}
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
});
