import {Container, Content, Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SubHeader from '../common/SubHeader';
import CustomPicker from '../common/CustomPicker';
import {CustomList} from '../common/CustomList';
import {CustomButton} from '../common/CustomButton';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {CustomList2} from '../common/CustomList2';
import {CustomTextArea} from '../common/CustomTextArea';
import {ApprovalStages} from '../common/ApprovalStages';
import {CalenderView} from '../common/CalenderView';
import {CustomTabs} from '../common/CustomTabs';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Const from '../common/Constants';
import {CustomCalendar} from '../common/CustomCalendar';
const SubThemeColor = '#FFEDEE';
const ThemeColor = '#F15761';
export default class OutsideDuty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveType: '',
      Attachment: {
        FileName: '',
        FileType: '',
        Attachment: '',
      },

      ApprovalStages: [],

      ActiveTab: 'From',
      LeaveType: 0,
      FromDate: '',
      ToDate: '',
      //FromDate: new Date().toISOString(),
      //ToDate: new Date().toISOString(),
      GetLeaveTypes: [],
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {
          this.GetLeaveTypes();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        //alert(value);
        this.setState({StaffCode: value}, function () {
          //this.GetMasterLeaveApproverDetails();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };
  GetLeaveTypes = async () => {
    const url = Const + 'api/Leave/GetLeaveTypes';
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {Loader: false, GetLeaveTypes: response.data},
        function () {},
      );
    } catch (error) {
      alert(error.message);
    }
  };
  GetMasterLeaveApproverDetails = async () => {
    //const url =
    //  Const +
    //  'api​/Leave​/GetMasterLeaveApproverDetails' +
    //  '​/' +
    //  parseInt(this.state.institute_id) +
    //  '​/' +
    //  this.state.StaffCode +
    //  '​/1';
    const url =
      'https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetails/2/PW100/1026';
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {
          Loader: false,
          // GetMasterLeaveApproverDetails​: response.data
        },
        function () {
          alert(response.data.message);
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };
  AddOrUpdateLeaveRequest = async () => {
    const url = Const + 'api/Leave/AddOrUpdateLeaveRequest';
    console.log(url);
    let bodyData = {
      Id: 0,
      InstituteId: parseInt(this.state.institute_id),
      StaffCode: this.state.StaffCode.toString(),
      LeaveId: 0,
      LeaveTypeId: this.state.LeaveType,
      //FromDate: "2021-07-07T03:00:21.436Z",
      //EndDate: "2021-07-07T03:00:21.436Z",
      FromDate: this.state.FromDate,
      EndDate: this.state.ToDate,
      Reason: this.state.Reason,
      LeaveAttachment: this.state.Attachement,
      NoOfDays: 1,
      Status: 0,
      CreatedDate: new Date(),
      ModifiedDate: new Date(),
    };
    console.log(JSON.stringify(bodyData));
    try {
      const response = await axios.post(url, bodyData);
      console.log('working');
      console.log(JSON.stringify(response.data));
      this.setState(
        {Loader: false, AddUpdateLeaveRequestNew: response.data},
        function () {
          alert(response.data.message);
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Outside Duty"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '95%',
                flexDirection: 'row',
                marginVertical: 10,
              }}>
              <CalenderView
                mainTitle="Total availed"
                title="CURRENT MONTHS"
                date="03"
                backgroundColor="#FD8991"
                width={120}
              />
              <CalenderView
                mainTitle="Total availed"
                title="CURRENT YEARS"
                date="10"
                backgroundColor="#FD8991"
                width={120}
              />
            </View>
            <CustomTabs
              borderRadius={20}
              height={45}
              color={ThemeColor}
              backgroundColor={SubThemeColor}
              ActiveTab={this.state.ActiveTab}
              tab1="From"
              tab2="To"
              onPress={(value) => {
                this.setState({ActiveTab: value}, function () {
                  console.log('Selected tab = ', value);
                });
              }}
            />
            {this.state.ActiveTab == 'From' ? (
              <CustomCalendar
                onPress={(value) => {
                  console.log('From =', value);
                  this.setState({FromDate: value}, function () {
                    //this.GetDetailedLeaves();
                  });
                }}
                color={SubThemeColor}
                themeColor={ThemeColor}
              />
            ) : (
              <CustomCalendar
                onPress={(value) => {
                  console.log('ToDate = ', value);
                  this.setState({ToDate: value}, function () {
                    //this.GetDetailedLeaves();
                  });
                }}
                color={SubThemeColor}
                themeColor={ThemeColor}
              />
            )}
            {/*<View
              style={{
                width: "95%",
                justifyContent: "space-evenly",
                alignItems: "center",
                flexDirection: "row",
                alignSelf: "center",
                marginVertical: 15,
                //marginTop: "4%",
              }}
            >
              <View style={{ width: "50%" }}>
                <View style={styles.labelContainer}>
                  <Text style={styles.label}>From</Text>
                </View>
                <DatePicker
                  showIcon={false}
                  style={{ width: "95%" }}
                  date={this.state.date1}
                  mode="date"
                  placeholder={moment(this.state.date1)
                    .format("DD/MM/YYYY")
                    .toString()}
                  format="YYYY-MM-DD"
                  minDate="2016-05-01"
                  maxDate={moment().format("YYYY-MM-DD").toString()}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      borderColor: "transparent",
                      borderRadius: 10,
                      marginTop: 10,
                      //  marginRight: 36,
                      padding: 0,
                      backgroundColor: ThemeColor,
                    },
                    dateText: {
                      color: "white",
                      textAlign: "center",
                      fontFamily: "Poppins-Regular",
                    },
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date) => {
                    this.setState({ date1: date });
                  }}
                />
              </View>

              <View style={{ width: "50%" }}>
                <View style={styles.labelContainer}>
                  <Text style={styles.label}>To</Text>
                </View>
                <DatePicker
                  showIcon={false}
                  style={{ width: "100%" }}
                  date={this.state.date}
                  mode="date"
                  placeholder={moment(this.state.date)
                    .format("DD/MM/YYYY")
                    .toString()}
                  format="YYYY-MM-DD"
                  minDate="2016-05-01"
                  maxDate={moment().format("YYYY-MM-DD").toString()}
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      //marginLeft: 36
                      borderColor: "transparent",
                      borderRadius: 10,
                      marginTop: 10,
                      //  marginRight: 36,
                      padding: 0,
                      backgroundColor: ThemeColor,
                    },
                    dateText: {
                      color: "white",
                      textAlign: "center",
                      fontFamily: "Poppins-Regular",
                    },
                    // ... You can check the source to find the other keys.
                  }}
                  onDateChange={(date) => {
                    this.setState({ date: date });
                  }}
                />
              </View>
            </View>*/}
            {/* <Text style={[styles.text, {fontWeight: '600', margin: 10}]}>
                Type of OD
              </Text>
              <CustomPicker
                label="You can choose type here"
                selectedValue={this.state.LeaveType}
                options={this.state.GetLeaveTypes}
                onValueChange={(value) => {
                  this.setState({LeaveType: value});
                }}
              /> */}
            <View style={{marginTop: 20}}>
              <CustomTextArea
                onPress2={() => {}}
                SelectedImage={(Attachment) => {
                  this.setState({Attachment});
                }}
                text={this.state.Attachment.FileName || 'No files attached'}
                title="Reason"
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                width="95%"
                placeholder="Write reason here"
                value={this.state.Reason}
                onChangeText={(text) => {
                  this.setState({Reason: text});
                }}
                onPress={() => {
                  this.AddOrUpdateLeaveRequest();
                }}
              />
            </View>
            <View style={{marginVertical: 20}}>
              {this.state.ApprovalStages.length > 0 && (
                <ApprovalStages
                  onPress={() => {}}
                  width="95%"
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.ApprovalStages}
                />
              )}
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
});
