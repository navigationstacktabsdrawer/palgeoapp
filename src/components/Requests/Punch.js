import {Container, Content, Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SubHeader from '../common/SubHeader';
import CustomPicker from '../common/CustomPicker';
import {CustomList} from '../common/CustomList';
import {CustomButton} from '../common/CustomButton';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {CustomList2} from '../common/CustomList2';
import {CustomTextArea} from '../common/CustomTextArea';
import {ApprovalStages} from '../common/ApprovalStages';
import {CalenderView} from '../common/CalenderView';
import axios from 'axios';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
const SubThemeColor = '#FFEDEE';
const ThemeColor = '#F15761';
export default class Punch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveType: '',
      ApproveInTime: false,
      ApproveOutTime: false,
      options: [{type: 'Meeting', id: 0}],
      LeaveArr: [
        {type: 'Sick Leave', pending: 1, total: 2},
        {type: 'Casual Leave', pending: 2, total: 2},
        {type: 'Medical Leave', pending: 2, total: 3},
        {type: 'Personal Leave', pending: 1, total: 1},
      ],
      ApprovalStages: [],
      Attachment: {
        FileName: '',
        FileType: '',
        Attachment: '',
      },
      LeaveArr2: [
        {
          date: '01.01.2021',
          session1: 'Morning',
          session2: 'Noon',
          holiday: null,
        },
        {
          date: '02.01.2021',
          session1: 'Morning',
          session2: 'Noon',
          holiday: 'Week Off',
        },
        {
          date: '03.01.2021',
          session1: 'Morning',
          session2: 'Noon',
          holiday: 'General Holiday',
        },
        {
          date: '04.01.2021',
          session1: 'Morning',
          session2: 'Noon',
          holiday: null,
        },
      ],
      PunchArr: [
        {
          date: '01.01.2021',
          time1: '09:15',
          time2: '06:15',
        },
        {
          date: '02.01.2021',
          time1: '09:00',
          time2: '05:45',
        },
        {
          date: '03.01.2021',
          time1: '09:15',
          time2: '05:15',
        },
        {
          date: '04.01.2021',
          time1: '09:15',
          time2: '-',
        },
      ],
      FromDate: new Date().toISOString(),
      ToDate: new Date().toISOString(),
      AddUpdatePunch: [
        // {
        //   AccessDate: '2021-07-07T11:44:09.509Z',
        //   ScheduledCheckinTime: {
        //     Ticks: 0,
        //     Days: 0,
        //     Hours: 0,
        //     Milliseconds: 0,
        //     Minutes: 0,
        //     Seconds: 0,
        //     TotalDays: 0,
        //     TotalHours: 0,
        //     TotalMilliseconds: 0,
        //     TotalMinutes: 0,
        //     TotalSeconds: 0,
        //   },
        //   ScheduledCheckoutTime: {
        //     Ticks: 0,
        //     Days: 0,
        //     Hours: 0,
        //     Milliseconds: 0,
        //     Minutes: 0,
        //     Seconds: 0,
        //     TotalDays: 0,
        //     TotalHours: 0,
        //     TotalMilliseconds: 0,
        //     TotalMinutes: 0,
        //     TotalSeconds: 0,
        //   },
        //   ActualCheckinTime: {
        //     Ticks: 0,
        //     Days: 0,
        //     Hours: 0,
        //     Milliseconds: 0,
        //     Minutes: 0,
        //     Seconds: 0,
        //     TotalDays: 0,
        //     TotalHours: 0,
        //     TotalMilliseconds: 0,
        //     TotalMinutes: 0,
        //     TotalSeconds: 0,
        //   },
        //   ActualCheckoutTime: {
        //     Ticks: 0,
        //     Days: 0,
        //     Hours: 0,
        //     Milliseconds: 0,
        //     Minutes: 0,
        //     Seconds: 0,
        //     TotalDays: 0,
        //     TotalHours: 0,
        //     TotalMilliseconds: 0,
        //     TotalMinutes: 0,
        //     TotalSeconds: 0,
        //   },
        //   CheckinRowId: 0,
        //   CheckoutRowId: 0,
        //   CheckinDelayTime: {
        //     Ticks: 0,
        //     Days: 0,
        //     Hours: 0,
        //     Milliseconds: 0,
        //     Minutes: 0,
        //     Seconds: 0,
        //     TotalDays: 0,
        //     TotalHours: 0,
        //     TotalMilliseconds: 0,
        //     TotalMinutes: 0,
        //     TotalSeconds: 0,
        //   },
        //   CheckoutEarlyTime: {
        //     Ticks: 0,
        //     Days: 0,
        //     Hours: 0,
        //     Milliseconds: 0,
        //     Minutes: 0,
        //     Seconds: 0,
        //     TotalDays: 0,
        //     TotalHours: 0,
        //     TotalMilliseconds: 0,
        //     TotalMinutes: 0,
        //     TotalSeconds: 0,
        //   },
        // },
      ],
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        //alert(value);
        this.setState({StaffCode: value}, function () {
          this.AddUpdatePunch();
          //this.GetMasterLeaveApproverDetails();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };
  AddUpdatePunch = async () => {
    let date = new Date();
    const url = Const + 'api/Leave/IdentifyMissedPunchTimestamps';
    let bodyData = {
      InstituteId: parseInt(this.state.institute_id),
      StaffCode: this.state.StaffCode,
      FromDate: new Date(date.getFullYear(), date.getMonth(), 2).toISOString(),
      ToDate: this.state.ToDate,

      // InstituteId: 0,
      // StaffCode: 'string',
      // FromDate: '2021-07-07T11:44:09.417Z',
      // ToDate: '2021-07-07T11:44:09.417Z',
    };
    console.log(url, bodyData);
    try {
      const response = await axios.post(url, bodyData);
      console.log(JSON.stringify('missed_puches =>>', response.data));
      this.setState(
        {
          Loader: false,
          AddUpdatePunch: response.data,
        },
        function () {},
      );
    } catch (error) {
      alert(error.message);
    }
  };
  AddLeave = async () => {
    const url = Const + 'api/Leave/RequestToModifyMissedPunchTimings';
    let bodyData = {
      InstituteId: parseInt(this.state.institute_id),
      RequestTypeId: 0,
      StaffCode: this.state.StaffCode,
      PunchDetails: [
        {
          AccessDate: this.state.SelectedData.AccessDate,
          CheckinRowId: parseInt(this.state.SelectedData.CheckinRowId),
          CheckoutRowId: parseInt(this.state.SelectedData.CheckoutRowId),
          ApproveInTime: this.state.ApproveInTime,
          ApproveOutTime: this.state.ApproveOutTime,
        },
      ],
    };
    console.log(url);
    console.log(bodyData);
    try {
      const response = await axios.post(url, bodyData);
      console.log(JSON.stringify(response.data));
      this.setState(
        {
          Loader: false,
          // AddUpdatePunch: response.data
        },
        function () {
          alert(response.data.message);
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };
  GetMasterLeaveApproverDetails = async () => {
    //const url =
    //  Const +
    //  'api​/Leave​/GetMasterLeaveApproverDetails' +
    //  '​/' +
    //  parseInt(this.state.institute_id) +
    //  '​/' +
    //  this.state.StaffCode +
    //  '​/1';
    const url =
      'https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetails/2/PW100/1026';
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {
          Loader: false,
          // GetMasterLeaveApproverDetails​: response.data
        },
        function () {
          alert(response.data.message);
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Punch"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View
              style={{
                width: '95%',
                alignSelf: 'center',
                marginTop: 20,
                backgroundColor: SubThemeColor,
                borderRadius: 10,
                minHeight: 200,
                maxHeight: 310,
              }}>
              <View
                style={[
                  styles.headerContainer,
                  {backgroundColor: ThemeColor, height: 60},
                ]}>
                <View
                  style={[
                    styles.headerTitleContainer,
                    {flex: 1, alignItems: 'flex-start'},
                  ]}>
                  <Text style={styles.text}>Missed/Correction</Text>
                  <Text style={[styles.text, {textAlign: 'left'}]}>Punch</Text>
                </View>
                <View style={[styles.headerTitleContainer, {flex: 0.5}]}>
                  <Text style={styles.text}>9AM</Text>
                  <Text style={styles.text}>Check in</Text>
                </View>
                <View
                  style={[
                    styles.headerTitleContainer,
                    {alignItems: 'center', flex: 0.5, marginLeft: 10},
                  ]}>
                  <Text style={styles.text}>6PM</Text>
                  <Text style={styles.text}>Check out</Text>
                </View>
              </View>
              <ScrollView>
                {this.state.AddUpdatePunch.map((item, index) => {
                  return (
                    <View
                      key={index}
                      style={[styles.headerContainer, {marginTop: 10}]}>
                      <View
                        style={[
                          styles.headerTitleContainer,
                          {
                            flex: 1,
                            alignItems: 'flex-start',
                          },
                        ]}>
                        <Text style={[styles.text, {color: 'black'}]}>
                          {moment(item.accessDate).format('DD.MM.YYYY')}
                        </Text>
                      </View>
                      <TouchableOpacity
                        onPress={() => {
                          if (this.state.ApproveInTime) {
                            this.setState({
                              SelectedData: '',
                              ApproveInTime: false,
                            });
                          } else {
                            this.setState({
                              SelectedData: item,
                              ApproveInTime: true,
                            });
                          }
                        }}
                        style={[
                          styles.headerTitleContainer,
                          {
                            //flex: 0.5,
                            width: 30,
                            backgroundColor: this.state.ApproveInTime
                              ? ThemeColor
                              : 'transparent',
                            borderRadius: 10,
                          },
                        ]}>
                        <Text
                          style={[
                            styles.text,
                            {
                              color: this.state.ApproveInTime
                                ? 'white'
                                : 'black',
                            },
                          ]}>
                          {/*{item.ScheduledCheckinTime.Hours}*/}
                          09:15
                        </Text>
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() => {
                          if (this.state.ApproveOutTime) {
                            this.setState({
                              SelectedData: '',
                              ApproveOutTime: false,
                            });
                          } else {
                            this.setState({
                              SelectedData: item,
                              ApproveOutTime: true,
                            });
                          }
                        }}
                        style={[
                          styles.headerTitleContainer,
                          {
                            alignItems: 'center',
                            justifyContent: 'center',
                            //flex: 0.5,
                            width: 30,
                            borderRadius: 10,
                            backgroundColor: this.state.ApproveOutTime
                              ? ThemeColor
                              : 'transparent',
                            marginLeft: 10,
                          },
                        ]}>
                        <Text
                          style={[
                            styles.text,
                            {
                              color: this.state.ApproveOutTime
                                ? 'white'
                                : 'black',
                              marginHorizontal: 10,
                            },
                          ]}>
                          06:00
                          {/*{item.scheduledCheckoutTime.Hours}*/}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  );
                })}
              </ScrollView>
            </View>
            <View style={{marginTop: 20}}>
              <CustomTextArea
                onPress2={() => {}}
                SelectedImage={(Attachment) => {
                  this.setState({Attachment});
                }}
                text={this.state.Attachment.FileName || 'No file attached'}
                title="Reason"
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                width="95%"
                placeholder="Write reason here"
                value={this.state.Reason}
                onChangeText={(text) => {
                  this.setState({Reason: text});
                }}
                onPress={() => {
                  this.AddLeave();
                }}
              />
            </View>
            <View style={{marginVertical: 20}}>
              {this.state.ApprovalStages.length > 0 && (
                <ApprovalStages
                  onPress={() => {}}
                  width="95%"
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.ApprovalStages}
                />
              )}
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    textAlign: 'center',
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'white',
  },
  headerContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  headerTitleContainer: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
