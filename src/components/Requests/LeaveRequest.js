import {Container, Content, Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import SubHeader from '../common/SubHeader';
import CustomPicker from '../common/CustomPicker';
import {CustomList} from '../common/CustomList';
import {CustomButton} from '../common/CustomButton';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import {CustomList2} from '../common/CustomList2';
import {CustomTextArea} from '../common/CustomTextArea';
import {ApprovalStages} from '../common/ApprovalStages';
import {CustomTabs} from '../common/CustomTabs';
import {CustomCalendar} from '../common/CustomCalendar';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Const from '../common/Constants';
const SubThemeColor = '#FFEDEE';
const ThemeColor = '#F15761';
export default class LeaveRequest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      LeaveType: '',
      LeaveDate: new Date(),
      GetLeaveTypes: [],
      GetInstituteStaffLeavesNew: [],
      GetDetailedLeaves: [],
      Morning: false,
      Noon: false,
      LeaveId: '',
      Reason: '',
      FromDate: '',
      ToDate: '',
      //FromDate: new Date().toISOString(),
      //ToDate: new Date().toISOString(),
      Attachment: {
        FileName: '',
        FileType: '',
        Attachment: '',
      },
      ApprovalStages: [],
      LeaveArr2: [],
      ActiveTab: 'From',
    };
  }
  componentDidMount() {
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('bearer_token');
      if (value !== null) {
        //alert(value);
        this.setState({Token: value}, function () {
          //this.GetLeaveTypes();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('institute_id');
      if (value !== null) {
        console.log('institute id =', value);
        this.setState({institute_id: value}, function () {});
      }
    } catch (error) {
      alert('Error retrieving data');
    }
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        console.log('StaffCode  =', value);
        //alert(value);
        this.setState({StaffCode: value}, function () {
          this.GetInstituteStaffLeavesNew();
          //this.GetMasterLeaveApproverDetails();
        });
      }
    } catch (error) {
      alert('Error retrieving data');
    }
  };
  //fetch(url, {
  //  method: 'GET',
  //  headers: {
  //    Accept: 'application/json',
  //    'Content-Type': 'application/json',
  //    Authorization: 'Bearer ' + this.state.Token,
  //  },
  //})
  GetLeaveTypes = async () => {
    const url = Const + 'api/Leave/GetLeaveTypes';
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {Loader: false, GetLeaveTypes: response.data},
        function () {},
      );
    } catch (error) {
      alert(error.message);
    }
  };
  GetInstituteStaffLeavesNew = async () => {
    this.setState({LeaveType: 3});
    const url =
      Const +
      'api/Leave/GetInstituteStaffLeavesNew/' +
      this.state.institute_id +
      '/' +
      this.state.StaffCode;
    console.log('GetInstituteStaffLeavesNew', url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {Loader: false, GetInstituteStaffLeavesNew: response.data},
        function () {},
      );
    } catch (error) {
      alert(error.message);
    }
  };
  GetDetailedLeaves = async () => {
    console.log('from and to ==>', this.state.FromDate, this.state.ToDate);
    const url =
      Const +
      'api/Leave/GetDetailedLeaves/' +
      this.state.FromDate +
      '/' +
      this.state.ToDate +
      '/' +
      //+
      parseInt(this.state.LeaveType) +
      '/' +
      parseInt(this.state.institute_id) +
      '/' +
      this.state.StaffCode.toString();
    //"2";
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState(
        {Loader: false, GetDetailedLeaves: response.data},
        function () {},
      );
    } catch (error) {
      alert(error.message);
    }
  };
  GetMasterLeaveApproverDetails = async () => {
    const url =
      Const +
      'api​/Leave​/GetMasterLeaveApproverDetails' +
      '​/' +
      parseInt(this.state.institute_id) +
      '​/' +
      this.state.StaffCode;
    // const url =
    //   'https://insproplus.com/palgeoapi/api/Leave/GetMasterLeaveApproverDetails/2/PW100/1026';
    console.log(url);
    try {
      const response = await axios.get(url);
      console.log(JSON.stringify(response.data));
      this.setState({
        Loader: false,
        ApprovalStages: response.data,
      });
    } catch (error) {
      alert('approver_error ==>' + error.message);
    }
  };
  AddUpdateLeaveRequestNew = async () => {
    const device_token = await AsyncStorage.getItem('device_token');
    const url = Const + 'api/Leave/AddUpdateLeaveRequestNew';
    console.log(url);
    const fromDate = new Date(this.state.FromDate).getTime();
    const endDate = new Date(this.state.ToDate).getTime();
    const diffSeconds = endDate - fromDate;
    const diffeHours = diffSeconds / 3600;
    const difference = parseInt(diffeHours.toString());
    console.log('dd', difference);
    // let bodyData = {
    //   Id: 0,
    //   MobileDeviceToken: '3f0e0882-f2f3-4d4d-97b8-01cce6c65cf9',
    //   InstituteId: 2,
    //   StaffCode: 'PW100',
    //   LeaveTypeId: 1023,
    //   LeaveDates: [
    //     {
    //       Id: 0,
    //       DatesSelected: '2021-07-09T10:52:45.044Z',
    //       FirstHalf: true,
    //       SecondHalf: false,
    //     },
    //   ],
    //   FromDate: '2021-07-09T10:52:45.044Z',
    //   EndDate: '2021-07-09T10:52:45.044Z',
    //   FromTime: '2021-07-09T10:53:11.503Z',
    //   ToTime: '2021-07-09T10:53:11.503Z',
    //   Reason: 'Ghd',
    //   LeaveAttachment: {
    //     FileName: 'string',
    //     FileType: 'string',
    //     Attachment: 'string',
    //   },
    //   NoOfDays: 0,
    //   Status: 0,
    //   CreatedDate: '2021-07-09T10:53:11.503Z',
    //   ModifiedDate: '2021-07-09T10:53:11.503Z',
    // };
    let bodyData = {
      Id: 0,
      MobileDeviceToken: device_token,
      InstituteId: parseInt(this.state.institute_id),
      StaffCode: this.state.StaffCode.toString(),
      //LeaveId: 1026,
      //LeaveId: parseInt(this.state.LeaveId),
      LeaveTypeId: parseInt(this.state.LeaveType),
      //LeaveTypeId: 8,
      LeaveDates: [
        {
          Id: 0,
          DatesSelected: this.state.LeaveDate.toISOString(),
          FirstHalf: this.state.Morning,
          SecondHalf: this.state.Noon,
        },
      ],
      FromDate: this.state.FromDate,
      EndDate: this.state.ToDate,
      // FromTime: '',
      // ToTime: '',

      Reason: this.state.Reason,
      LeaveAttachment: this.state.Attachment,
      //NoOfDays: difference,
      NoOfDays: 1,
      Status: 0,
      CreatedDate: new Date().toISOString(),
      ModifiedDate: new Date().toISOString(),
    };
    //const headers = {
    //  Accept: "application/json",
    //  "Content-Type": "application/json",
    //  Authorization: "Bearer " + this.state.Token,
    //};
    console.log(JSON.stringify(bodyData));
    try {
      const response = await axios.post(url, bodyData);
      console.log(response);
      //console.log(JSON.stringify(response.data));
      this.setState(
        {
          Loader: false,
          //AddUpdateLeaveRequestNew: response.data
        },
        function () {
          alert(response.data.message);
        },
      );
    } catch (error) {
      alert(error.message);
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <Container>
          <SubHeader
            title="Leave Request"
            showBack={true}
            backScreen="Leaves"
            showBell={false}
            navigation={this.props.navigation}
          />

          <Content>
            {/* <Text style={[styles.text, {fontWeight: '600', margin: 10}]}>
              Request Type
            </Text>
            <CustomPicker
              label="Select Leave Type"
              selectedValue={this.state.LeaveType}
              options={this.state.GetLeaveTypes}
              onValueChange={(value) => {
                console.log('leave id =', value);
                this.setState({LeaveId: value}, function () {});
              }}
            />
            {this.state.LeaveId ? ( */}
            <CustomList
              onPress={(item) => {
                console.log('leave type id', item);
                this.setState({LeaveType: item});
              }}
              width="95%"
              title1="SR No."
              title2="Eligible Leave"
              title3="Apply"
              color={SubThemeColor}
              headerColor={ThemeColor}
              Arr={this.state.GetInstituteStaffLeavesNew}
            />
            {/* ) : null} */}
            {this.state.LeaveType ? (
              <CustomTabs
                borderRadius={20}
                height={45}
                color={ThemeColor}
                backgroundColor={SubThemeColor}
                ActiveTab={this.state.ActiveTab}
                tab1="From"
                tab2="To"
                onPress={(value) => {
                  this.setState({ActiveTab: value}, function () {
                    console.log('Selected tab = ', value);
                  });
                }}
              />
            ) : null}
            {this.state.LeaveType ? (
              this.state.ActiveTab == 'From' ? (
                <CustomCalendar
                  onPress={(value) => {
                    console.log('From =', value);
                    this.setState(
                      {FromDate: value, ActiveTab: 'To'},
                      function () {
                        //this.GetDetailedLeaves();
                      },
                    );
                  }}
                  color={SubThemeColor}
                  themeColor={ThemeColor}
                />
              ) : (
                <CustomCalendar
                  onPress={(value) => {
                    console.log('ToDate = ', value);
                    this.setState({ToDate: value}, function () {
                      //this.GetDetailedLeaves();
                    });
                  }}
                  color={SubThemeColor}
                  themeColor={ThemeColor}
                />
              )
            ) : null}
            {this.state.SelectedDate ? (
              <View style={{marginTop: 20}}>
                <CustomList2
                  onPress={(Morning, LeaveDate) => {
                    console.log(Morning);
                    console.log(LeaveDate);
                    this.setState({
                      Morning,
                      // LeaveDate
                    });
                  }}
                  onPress2={(Noon) => {
                    console.log(Noon);
                    this.setState({Noon});
                  }}
                  width="95%"
                  title1="Date"
                  title2="Session 1"
                  title3="Session 2"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.GetDetailedLeaves}
                />
              </View>
            ) : null}
            <View style={{marginTop: 20}}>
              <CustomTextArea
                onPress2={() => {}}
                SelectedImage={(Attachment) => {
                  this.setState({Attachment});
                }}
                text={this.state.Attachment.FileName || 'No file attached'}
                title="Reason For Leave"
                backgroundColor={SubThemeColor}
                placeholderTextColor={ThemeColor}
                width="95%"
                placeholder="Write reason for leave"
                value={this.state.Reason}
                onChangeText={(text) => {
                  console.log('reson =', text);
                  this.setState({Reason: text});
                }}
                onPress={() => {
                  if (this.state.LeaveType == '') {
                    alert('Please Select Leave Type');
                  } else if (this.state.FromDate == '') {
                    alert('Please Select From Date');
                  } else if (this.state.ToDate == '') {
                    alert('Please Select To Date');
                  } else if (this.state.Reason == '') {
                    alert('Please Enter The Reason For Leave');
                  } else {
                    this.AddUpdateLeaveRequestNew();
                  }
                }}
              />
            </View>
            <View style={{marginVertical: 20}}>
              {this.state.ApprovalStages.length > 0 && (
                <ApprovalStages
                  onPress={() => {}}
                  width="95%"
                  title="Approval Stages"
                  color={SubThemeColor}
                  headerColor={ThemeColor}
                  Arr={this.state.ApprovalStages}
                />
              )}
            </View>
          </Content>
          {/*<View style={{ flex: 1 }}>*/}

          {/*</View>*/}
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  item: {
    borderRadius: 15,
    margin: 10,
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 0.5,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  dateContainer: {
    backgroundColor: ThemeColor,
  },
});
