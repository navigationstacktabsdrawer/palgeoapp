import React from 'react';
import BackgroundTimer from 'react-native-background-timer';
export default function BackgroundTask(){
    // Start a timer that runs continuous after X milliseconds
    const intervalId = BackgroundTimer.setInterval(() => {
        // this will be executed every 200 ms
        // even when app is the the background
        console.log('tic');
    }, 2000);

    // Cancel the timer when you are done with it
    BackgroundTimer.clearInterval(intervalId);
    return null
}