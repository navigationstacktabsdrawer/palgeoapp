import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Container, Content, Card} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
export default class StaffDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      showAlert: false,
      details: {},
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('user_id').then((user_id) => {
      console.log(user_id);
      AsyncStorage.getItem('institute_id').then((institute_id) => {
        this.staffDetails(user_id, institute_id);
      });
    });
  }
  staffDetails = (user_id, institute_id) => {
    this.setState({showAlert: false});
    fetch(Const + 'api/Staff/information/' + institute_id + '/' + user_id, {
      method: 'GET',
      headers: {
        Accept: 'text/plain',
        'content-type': 'application/json-patch+json',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        console.log('json_profile', json);
        if (json.status) {
          this.setState({details: json.staffInformation, showAlert: false});
        } else {
          this.setState({details: {}, showAlert: false});
        }
      })
      .catch((error) => {
        this.setState({showAlert: false});
      });
  };
  render() {
    return (
      <Container style={{flex: 1, backgroundColor: '#ffffff'}}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <Content>
          <View style={styles.center}>
            <Card style={styles.walletContainer}>
              <View style={styles.row}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Institute</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.state.details.college}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Department</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.state.details.department}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Designation</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.state.details.designation}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Name</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.state.details.name}
                  </Text>
                </View>
              </View>
              <View style={styles.row}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Staff Type</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.state.details.type}
                  </Text>
                </View>
              </View>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    margin: '1%',
  },
  date: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12.5,
    margin: '1%',
    color: '#909090',
  },
  walletContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    elevation: 5,
    width: wp('90'),
  },
  header: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#67747d',
  },
  headerVal: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#000000',
  },
  row: {
    flexDirection: 'row',
    padding: '3%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  divider: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    margin: '2%',
  },
  divider1: {
    borderWidth: 1,
    borderColor: '#f3f3f3',
    margin: '2%',
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#ffffff',
  },
  upperBackground: {
    position: 'absolute',
    width: wp('100'),
    height: 100,
    backgroundColor: '#f05760',
    top: 37,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '2%',
  },
});
