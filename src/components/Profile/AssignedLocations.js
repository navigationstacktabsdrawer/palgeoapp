import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  //Picker,
} from 'react-native';
import {Container, Content, Item, Card, Picker} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
var moment = require('moment');
function convertUTCToLocal(utcDt, utcDtFormat) {
  var toDt = moment.utc(utcDt, utcDtFormat).toDate();
  return moment(toDt).format('hh:mm A');
}
export default class StaffDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      showAlert: false,
      options: [],
      data: [],
      travelPreference: '',
      showAlert1: false,
      error_message: '',
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('user_id').then((user_id) => {
      AsyncStorage.getItem('institute_id').then((institute_id) => {
        AsyncStorage.getItem('user_id').then((bearer_token) => {
          AsyncStorage.getItem('driving_type').then((driving_type) => {
            console.log('ddd', driving_type);
            switch (driving_type) {
              case 'DRIVING':
                this.setState({travelPreference: 1});
                break;
              case 'BICYCLING':
                this.setState({travelPreference: 2});
                break;
              case 'WALKING':
                this.setState({travelPreference: 3});
                break;
              case 'BUS':
                this.setState({travelPreference: 4});
                break;
              case 'TRAIN':
                this.setState({travelPreference: 5});
                break;

              default:
                this.setState({travelPreference: ''});
                break;
            }
            //this.setState({travelPreference: driving_type_id});
            this.staffDetails(user_id, institute_id);
            this.getTravelPreference(bearer_token);
          });
        });
      });
    });
  }
  getTravelPreference = (bearer_token) => {
    this.setState({showAlert: false});
    fetch(Const + 'api/Staff/traveloptions', {
      method: 'GET',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: 'Bearer ' + bearer_token,
        Accept: 'application/json, text/plain',
        'Content-Type': 'application/json-patch+json',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.length > 0) {
          this.setState({options: json, showAlert: false});
        } else {
          this.setState({options: [], showAlert: false});
        }
      })
      .catch((error) => {
        this.setState({showAlert: false});
      });
  };
  staffDetails = (user_id, institute_id) => {
    console.log(user_id, institute_id);
    this.setState({showAlert: false});
    fetch(Const + 'api/Staff/information/' + institute_id + '/' + user_id, {
      method: 'GET',
      headers: {
        Accept: 'text/plain',
        'content-type': 'application/json-patch+json',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.status) {
          console.log(
            'api/Staff/information/institute_id/user_id ==>',
            json.assignedLocations,
          );
          this.setState({data: json.assignedLocations, showAlert: false});
        } else {
          this.setState({data: [], showAlert: false});
        }
      })
      .catch((error) => {
        this.setState({showAlert: false});
      });
  };
  goToMapView = (type, coordinates, radius) => {
    if (type == 'Circle') {
      this.props.navigation.navigate('CircleMapView', {
        coordinates: coordinates,
        radius: radius,
      });
    } else if (type == 'Polygon') {
      this.props.navigation.navigate('PolygonMapView', {
        coordinates: coordinates,
      });
    }
  };
  onValueChange = (value) => {
    this.updateTravelPreference(value);
  };
  updateTravelPreference = (value) => {
    AsyncStorage.getItem('user_id').then((user_id) => {
      AsyncStorage.getItem('institute_id').then((institute_id) => {
        AsyncStorage.getItem('bearer_token').then((bearer_token) => {
          this.setState({showAlert: true});
          fetch(
            Const +
              '/api/Staff/update/traveloptions/' +
              institute_id +
              '/' +
              user_id +
              '/' +
              value,
            {
              method: 'GET',
              withCredentials: true,
              credentials: 'include',
              headers: {
                Authorization: 'Bearer ' + bearer_token,
                Accept: 'application/json, text/plain',
                'Content-Type': 'application/json-patch+json',
              },
            },
          )
            .then((response) => {
              if (value == '1') {
                var text = 'DRIVING';
              } else if (value == '2') {
                var text = 'BICYCLING';
              } else if (value == '3') {
                var text = 'WALKING';
              } else if (value == '4') {
                var text = 'BUS';
              } else if (value == '5') {
                var text = 'TRAIN';
              }
              try {
                AsyncStorage.setItem('driving_type', text.toString());
                AsyncStorage.setItem('driving_type_id', value.toString());
              } catch (e) {
                console.log(e);
              }
              this.setState({
                showAlert1: true,
                error_message: 'Travel preference updated successfully',
                showAlert: false,
                travelPreference: value,
              });
            })
            .catch((error) => {
              this.setState({showAlert: false});
            });
        });
      });
    });
  };
  render() {
    return (
      <Container>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Content>
          <View style={{marginTop: '3%', marginLeft: '3%', marginRight: '3%'}}>
            <View style={styles.labelContainer}>
              <Text style={styles.label}>Travel Preference</Text>
            </View>
            <View>
              <Item regular style={styles.item}>
                <Picker
                  note
                  mode="dropdown"
                  style={{width: 120}}
                  selectedValue={this.state.travelPreference}
                  onValueChange={(value) => this.onValueChange(value)}>
                  <Picker.Item label="Select" value="" key="" />
                  {this.state.options.map((item) => {
                    return (
                      <Picker.Item
                        label={item.type}
                        value={item.id}
                        key={item.id}
                      />
                    );
                  })}
                </Picker>
              </Item>
            </View>
          </View>
          <View style={styles.center}>
            {this.state.data.map((item, index) => {
              var date = moment().format('YYYY-MM-DD ' + item.checkInTime);
              var local_checkin_time = convertUTCToLocal(date);

              var date1 = moment().format('YYYY-MM-DD ' + item.checkOutTime);
              var local_checkout_time = convertUTCToLocal(date1);

              return (
                <Card style={styles.walletContainer} key={item.id}>
                  <View style={styles.row}>
                    <View style={{width: wp('40'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Access Location</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>
                        {item.accessLocation}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('40'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Face Authentication</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>
                        {item.isFaceRecognitionMandatory
                          ? 'Mandatory'
                          : 'Not Mandatory'}
                      </Text>
                    </View>
                  </View>
                  {item.beaconLocation && item.beaconLocation !== '' && (
                    <View style={styles.row}>
                      <View style={{width: wp('40'), justifyContent: 'center'}}>
                        <Text style={styles.header}>Beacon Location</Text>
                      </View>
                      <View style={{width: wp('50')}}>
                        <Text style={styles.headerVal}>
                          {item.beaconLocation}
                        </Text>
                      </View>
                    </View>
                  )}
                  <View style={styles.row}>
                    <View style={{width: wp('40'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Checkin Time</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      {local_checkin_time == 'Invalid date' ? (
                        <Text style={styles.headerVal}>Not Alloted</Text>
                      ) : (
                        <Text style={styles.headerVal}>
                          {local_checkin_time
                            ? local_checkin_time
                            : 'Not Alloted'}
                        </Text>
                      )}
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('40'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Checkout Time</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      {local_checkout_time == 'Invalid date' ? (
                        <Text style={styles.headerVal}>Not Alloted</Text>
                      ) : (
                        <Text style={styles.headerVal}>
                          {local_checkout_time
                            ? local_checkout_time
                            : 'Not Alloted'}
                        </Text>
                      )}
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('40'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Type</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>{item.type}</Text>
                    </View>
                  </View>
                  {item.type === 'Circle' || item.type === 'Polygon' ? (
                    <View style={styles.row}>
                      <View style={{width: wp('40'), justifyContent: 'center'}}>
                        <Text style={styles.header}>Action</Text>
                      </View>

                      <View style={{width: wp('50')}}>
                        <TouchableWithoutFeedback
                          onPress={() =>
                            this.goToMapView(
                              item.type,
                              item.coordinates,
                              item.radius,
                            )
                          }>
                          <Text style={styles.headerVal5}>View in Map</Text>
                        </TouchableWithoutFeedback>
                      </View>
                    </View>
                  ) : null}
                </Card>
              );
            })}
          </View>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  drivingContainer: {
    paddingTop: 10,
    paddingLeft: 14,
  },
  drivingText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    margin: '1%',
  },
  date: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12.5,
    margin: '1%',
    color: '#909090',
  },
  walletContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    elevation: 5,
    width: wp('93'),
  },
  header: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#67747d',
  },
  headerVal: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#000000',
  },
  row: {
    flexDirection: 'row',
    padding: '3%',
  },
  divider: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    margin: '2%',
  },
  divider1: {
    borderWidth: 1,
    borderColor: '#f3f3f3',
    margin: '2%',
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#ffffff',
  },
  headerVal5: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#f05760',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#67747d',
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    paddingLeft: '5%',
    borderRadius: 10,
    height: hp('6'),
    borderColor: '#f1f1f1',
    borderWidth: 1,
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 1,
    height: hp('6'),
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '2%',
  },
});
