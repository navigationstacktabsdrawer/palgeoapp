import React, {Component} from 'react';
import {View, StyleSheet, Text, Modal} from 'react-native';
import {Item, Input, Icon} from 'native-base';
import MapView, {Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import Loader from './common/Loader';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import AwesomeAlert from 'react-native-awesome-alerts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  TouchableHighlight,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import ModalForAccuracy from './common/Modal';
//import AwesomeAlert from 'react-native-awesome-alerts';

export default class MapFencing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      coordinates: {
        latitude: this.props.route.params.coordinates.latitude || 22.333,
        longitude: this.props.route.params.coordinates.longitude || -122.22,
        latitudeDelta: 0.000922,
        longitudeDelta: 0.000421,
      },
      region: {},
      showAlert: false,
      radius: '10',
      accuracy: '',
      showAlert1: true,
      error_message: 'Please wait.. Fetching location',
      modal: false,
      position: {},
      satellite: false,
    };
  }
  componentDidMount() {
    //this.getCurrentLocation();
    setTimeout(() => this.setState({loader: false}), 1200);
    setTimeout(() => {
      Geolocation.clearWatch(this.watchID);
      this.setState({showAlert1: false});
      if (this.state.accuracy > 20) {
        this.setState({modal: true});
        console.log('Modal true');
        return;
      }
    }, 20000);
    this.watchID = Geolocation.watchPosition(
      (position) => {
        console.log('PROGESS...', position.coords.accuracy);
        this.setState({accuracy: position.coords.accuracy});
        if (position.coords.accuracy <= 10) {
          Geolocation.clearWatch(this.watchID);
          console.log('MATCHED...', position.coords.accuracy);
          const currentLongitude = JSON.stringify(position.coords.longitude);
          const currentLatitude = JSON.stringify(position.coords.latitude);
          var coordinates = {
            latitude: parseFloat(currentLatitude),
            longitude: parseFloat(currentLongitude),
            latitudeDelta: 0.000922,
            longitudeDelta: 0.000421,
          };
          // console.log(coordinates);
          this.setState({coordinates: coordinates, loader: false});
          this.setState({
            accuracy: position.coords.accuracy,
            showAlert1: false,
          });
          return;
        }
      },
      (e) => {
        console.log(e);
        //alert('no current location found');
      },
      {
        enableHighAccuracy: true,
        distanceFilter: 0,
        fastestInterval: 1000,
        interval: 2000,
      },
    );
    //console.log('object===', this.watchId);
  }

  // componentWillUnmount() {
  //   clearInterval(this.interval);
  // }

  goToSaveFencing = () => {
    if (parseFloat(this.state.radius) < 10) {
      return alert('Radius cannot be less than 10 m');
    }
    this.props.navigation.navigate('SaveFencing', {
      id: null,
      accessLocation: null,
      institute_id: this.props.route.params.institute_id,
      radius: this.state.radius,
      coordinates: this.state.coordinates,
    });
  };
  goToFencingList = () => {
    this.props.navigation.navigate('FencingList', {
      institute_id: this.props.route.params.institute_id,
    });
  };
  goToInstitueList = () => {
    this.props.navigation.navigate('SelectInstitute', {
      institute_id: this.props.route.params.institute_id,
    });
  };
  getCurrentLocation = async () => {
    this.callLocation();
    // this.interval = setInterval(() => {
    //   this.callLocation();
    // }, 4000);
  };
  callLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        console.log('accuracy===2', position.coords.accuracy);
        const currentLongitude = JSON.stringify(position.coords.longitude);
        const currentLatitude = JSON.stringify(position.coords.latitude);
        var coordinates = {
          latitude: parseFloat(currentLatitude),
          longitude: parseFloat(currentLongitude),
          latitudeDelta: 0.000922,
          longitudeDelta: 0.000421,
        };
        // console.log(coordinates);
        this.setState({coordinates: coordinates, loader: false});
      },
      (error) => {
        console.log(error);
      },
      {
        // enableHighAccuracy: true,
        // accuracy: 'high',
        // timeout: 15000,
        // maximumAge: 0,
        // distanceFilter: 2,
        enableHighAccuracy: true,
        accuracy: 'high',
        timeout: 20000,
        maximumAge: 0,
      },
    );
  };
  onRegionChange = (region) => {
    console.log('region_change', region);
    this.setState({region: region});
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    console.log('FINAL_ACCURACY', this.state.accuracy);
    // console.log('FINAL', this.state.coordinates);
    return (
      <>
        {this.state.modal ? (
          <ModalForAccuracy
            visible={this.state.modal}
            onClose={() => this.props.navigation.goBack()}
            //onClose={() => this.setState({modal: false})}
            onPress={() => this.props.navigation.goBack()}
            accuracy={Number(this.state.accuracy).toFixed(2)}
            mapfencing
          />
        ) : (
          <View style={styles.container}>
            <AwesomeAlert
              show={this.state.showAlert1}
              showProgress={true}
              title="Attention"
              message={this.state.error_message}
              closeOnTouchOutside={false}
              closeOnHardwareBackPress={false}
              //showCancelButton={true}
              cancelText="Okay"
              onCancelPressed={() => {
                this.setState({showAlert1: false});
              }}
              cancelButtonColor="#f05760"
              cancelButtonTextStyle={{
                fontFamily: 'Poppins-Regular',
                fontSize: 13,
              }}
              messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
              titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
            />

            <MapView
              region={{
                latitude: parseFloat(this.state.coordinates.latitude),
                longitude: parseFloat(this.state.coordinates.longitude),
                latitudeDelta: 0.000009221,
                longitudeDelta: 0.000004211,
              }}
              //onRegionChange={this.onRegionChange}
              style={styles.map}
              mapType={this.state.satellite ? 'satellite' : 'standard'}
              followUserLocation={true}
              cacheEnabled={true}
              initialRegion={{
                latitude: parseFloat(this.state.coordinates.latitude),
                longitude: parseFloat(this.state.coordinates.longitude),
                latitudeDelta: 0.000009221,
                longitudeDelta: 0.000004211,
              }}
              scrollEnabled={false}
              zoomEnabled={true}>
              <Marker
                //draggable
                anchor={{x: 0.5, y: 0.6}}
                onDragEnd={(e) =>
                  this.setState({coordinates: e.nativeEvent.coordinate})
                }
                coordinate={this.state.coordinates}
                onRegionChange={this.onRegionChange}
                title={'Your are here!'}>
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  {/* <View style={styles.textContainer}>
                                <Text style={styles.subtext}>Long press to drag</Text>
                            </View> */}
                  <Icon
                    name="map-marker"
                    type="FontAwesome"
                    style={{color: 'red', fontSize: 30}}
                  />
                </View>
              </Marker>
              <MapView.Circle
                key={(
                  this.state.coordinates.latitude +
                  this.state.coordinates.longitude
                ).toString()}
                center={this.state.coordinates}
                radius={parseFloat(this.state.radius)}
                strokeWidth={2}
                strokeColor={'red'}
                fillColor={'transparent'}
              />
            </MapView>
            <View style={{alignItems: 'center'}}>
              <AwesomeAlert
                show={this.state.showAlert}
                showProgress={true}
                title="Loading"
                closeOnTouchOutside={false}
                closeOnHardwareBackPress={false}
              />
              <View style={styles.searchContainer}>
                <View style={styles.sliderContainer}>
                  <View>
                    <View
                      style={{
                        flexDirection: 'row',
                        margin: '3%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <View style={styles.labelContainer}>
                        <Text style={styles.label}>Circle Radius</Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <TouchableWithoutFeedback
                          onPress={this.goToInstitueList}>
                          <View style={styles.buttonContainer}>
                            <Icon
                              name="angle-left"
                              type="FontAwesome"
                              style={{color: '#f05760', fontSize: 22}}
                            />
                          </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                          onPress={this.goToFencingList}>
                          <View style={styles.buttonContainer1}>
                            <Icon
                              name="list"
                              type="FontAwesome"
                              style={{color: '#f05760', fontSize: 14}}
                            />
                          </View>
                        </TouchableWithoutFeedback>

                        <TouchableWithoutFeedback
                          onPress={this.goToSaveFencing}>
                          <View style={styles.buttonContainer1}>
                            <Icon
                              name="floppy-o"
                              type="FontAwesome"
                              style={{color: '#f05760', fontSize: 15}}
                            />
                          </View>
                        </TouchableWithoutFeedback>
                      </View>
                    </View>
                    <View>
                      <Item regular style={styles.item}>
                        <Input
                          style={styles.input}
                          value={this.state.radius}
                          onChangeText={(radius) => {
                            this.setState({
                              radius: radius ? parseFloat(radius) : 10,
                            });
                          }}
                          keyboardType="numeric"
                        />
                      </Item>
                    </View>
                  </View>
                  <View>
                    <View
                      style={{
                        //flexDirection: 'row',
                        margin: '3%',
                        //justifyContent: 'center',
                        //alignItems: 'center',
                      }}>
                      <View style={styles.labelContainer}>
                        <Text style={styles.label}>Map View</Text>
                      </View>
                    </View>
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({satellite: !this.state.satellite})
                      }>
                      <Item
                        regular
                        style={{...styles.item, width: wp('30%'), padding: 10}}>
                        <Text
                          style={{
                            color: 'black',
                            fontSize: 15,
                            textAlign: 'center',
                            fontWeight: '600',
                            fontFamily: 'Poppins-Regular',
                          }}>
                          {this.state.satellite ? 'Satellite' : 'Standard'}
                        </Text>
                      </Item>
                    </TouchableOpacity>
                  </View>

                  {/* <GooglePlacesAutocomplete
                styles={{
                  textInputContainer: {
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderTopWidth: 0,
                    borderBottomWidth: 0,
                    borderRadius: 10,
                  },
                  textInput: {
                    marginLeft: 0,
                    marginRight: 0,
                    height: 35,
                    fontSize: 16,
                    height: 42,
                    borderWidth: 0,
                    borderColor: 'grey',
                    borderRadius: 10,
                  },
                  listView: {
                    backgroundColor: '#ffffff',
                    width: '100%',
                  },
                  poweredContainer: {
                    backgroundColor: '#ffffff',
                  },
                  predefinedPlacesDescription: {
                    color: '#1faadb',
                  },
                }}
                placeholder="Search Locations..."
                onPress={(data, details = null) => {
                  this.setState({showAlert: true});
                  fetch(
                    'https://maps.googleapis.com/maps/api/geocode/json?place_id=' +
                      data.place_id +
                      '&key=AIzaSyDrH5O4x0LVfDOHIOcuXFj7_cS4Rz4eUxM',
                    {
                      method: 'GET',
                    },
                  )
                    .then((response) => response.json())
                    .then((json) => {
                      try {
                        var coordinates = {
                          latitude: parseFloat(
                            json['results'][0].geometry.location.lat,
                          ),
                          longitude: parseFloat(
                            json['results'][0].geometry.location.lng,
                          ),
                        };
                        this.setState({
                          coordinates: coordinates,
                          showAlert: false,
                        });
                      } catch (e) {}
                    })
                    .catch((error) => {});
                }}
                query={{
                  key: 'AIzaSyDrH5O4x0LVfDOHIOcuXFj7_cS4Rz4eUxM',
                  language: 'en',
                }}
              /> */}
                </View>
              </View>
            </View>
          </View>
        )}
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  textContainer: {
    backgroundColor: '#f05760',
    padding: '4%',
    borderRadius: 5,
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#ffffff',
  },
  subtext: {
    fontFamily: 'Poppins-Regular',
    fontSize: 9,
    color: '#ffffff',
  },
  searchContainer: {
    position: 'absolute',
    zIndex: 99999,
    padding: '4%',
    borderRadius: 5,
    width: '100%',
  },
  sliderContainer: {
    backgroundColor: '#f05760',
    paddingTop: '2%',
    paddingBottom: '5%',
    paddingLeft: '4%',
    paddingRight: '4%',
    borderRadius: 15,
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  label1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#c9c3c5',
    paddingLeft: wp('3'),
  },
  labelContainer: {
    marginLeft: '1.5%',
    width: wp('50'),
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    paddingLeft: '5%',
    borderRadius: 10,
    height: hp('6'),
    backgroundColor: '#ffffff',
  },
  item: {
    borderRadius: 10,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    height: hp('6'),
    backgroundColor: '#ffffff',
  },
  footerContainer: {
    position: 'absolute',
    zIndex: 99999,
    top: 600,
    right: 25,
  },
  buttonContainer: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
  },
  buttonContainer1: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
    marginLeft: 10,
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#ffffff',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    flex: 1,
    margin: 20,
    backgroundColor: 'white',
    width: wp('100%'),
    height: hp('100%'),
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
