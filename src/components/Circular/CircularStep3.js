import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Content, Item, Input, Icon, Textarea} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import RNFetchBlob from 'rn-fetch-blob';
import DocumentPicker from 'react-native-document-picker';
export default class CircularStep3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      showAlert: false,
      isSMS: true,
      isEmail: true,
      isNotification: true,
      circular: '',
      subject: '',
      bearer_token: '',
      institute_id: '',
      showAlert1: false,
      error_message: '',
      org_id: '',
      NotificationAttachment: {
        FileName: '',
        FileType: '',
        Attachment: '',
      },
      EmailAttachment: {
        FileName: '',
        FileType: '',
        Attachment: '',
      },
    };
  }
  componentDidMount() {
    AsyncStorage.getItem('bearer_token').then((bearer_token) => {
      AsyncStorage.getItem('institute_id').then((institute_id) => {
        AsyncStorage.getItem('org_id').then((org_id) => {
          this.setState({
            bearer_token: bearer_token,
            institute_id: institute_id,
            org_id: org_id,
          });
        });
      });
    });
  }
  send = () => {
    if (this.state.subject) {
      if (this.state.circular) {
        this.setState({showAlert: true});
        fetch(Const + 'api/Staff/SendCircular', {
          method: 'POST',
          withCredentials: true,
          credentials: 'include',
          headers: {
            Authorization: this.state.bearer_token,
            Accept: 'text/plain',
            'Content-Type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            list_staff_code: this.props.route.params.data,
            subject: this.state.subject,
            circular: this.state.circular,
            isSMS: this.state.isSMS,
            isEmail: this.state.isEmail,
            isNotification: this.state.isNotification,
            instituteId: this.state.institute_id,
            OrganizationId: this.state.org_id,
            emailAttachment: this.state.EmailAttachment,
            notificationAttachment: this.state.NotificationAttachment,
          }),
        })
          .then((response) => response.json())
          .then((json) => {
            if (json.status) {
              if (Platform.OS == 'ios') {
                alert('Circular sent to all selected staff');
                setTimeout(() => {
                  this.setState({showAlert: false});
                }, 1300);
                setTimeout(() => {
                  this.props.navigation.navigate('Circular');
                }, 1200);
              } else {
                this.setState({
                  showAlert: false,
                  showAlert1: true,
                  error_message: 'Circular sent to all selected staff',
                });
                setTimeout(() => {
                  this.props.navigation.navigate('Circular');
                }, 1200);
              }
            } else {
              if (Platform.OS == 'ios') {
                alert('Unknown error occured');
                setTimeout(() => {
                  this.setState({showAlert: false});
                }, 1200);
              } else {
                this.setState({
                  showAlert: false,
                  showAlert1: true,
                  error_message: 'Unknown error occured.Try later',
                });
              }
            }
          })
          .catch((error) => {
            if (Platform.OS == 'ios') {
              alert('Unknown error occured');
              setTimeout(() => {
                this.setState({showAlert: false});
              }, 1200);
            } else {
              this.setState({
                showAlert: false,
                showAlert1: true,
                error_message: 'Unknown error occured.',
              });
            }
          });
      } else {
        if (Platform.OS == 'ios') {
          alert('Circlar cannot be empty');
          setTimeout(() => {
            this.setState({showAlert: false});
          }, 1200);
        } else {
          this.setState({
            showAlert: false,
            showAlert1: true,
            error_message: 'Circular cannot be empty',
          });
        }
      }
    } else {
      if (Platform.OS == 'ios') {
        alert('Subject cannot be empty');
        setTimeout(() => {
          this.setState({showAlert: false});
        }, 1200);
      } else {
        this.setState({
          showAlert: false,
          showAlert1: true,
          error_message: 'Subject cannot be empty',
        });
      }
    }
  };
  showPassword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };
  handleCheckbox1 = () => {
    var current = this.state.isSMS;
    this.setState({isSMS: !current});
  };
  handleCheckbox2 = () => {
    var current = this.state.isEmail;
    this.setState({isEmail: !current});
  };
  handleCheckbox3 = () => {
    var current = this.state.isNotification;
    this.setState({isNotification: !current});
  };
  SelectCircularImage = async () => {
    try {
      const res = await DocumentPicker.pick({
        includeBase64: true,
        type: [DocumentPicker.types.allFiles],
      });
      let typeIdentifier = res.type.split('/');
      console.log(typeIdentifier[1]);
      //alert(typeIdentifier[1]);
      RNFetchBlob.fs
        .readFile(res.uri, 'base64')
        .then((data) => {
          var Attachment = {
            FileName:
              'CircularAttachment_' +
              new Date().getTime() +
              '.' +
              typeIdentifier[1],
            FileType: res.type,
            Attachment: data,
          };

          this.setState({EmailAttachment: Attachment});
        })
        .catch((err) => {});
    } catch (err) {
      console.log('Unknown Error: ' + JSON.stringify(err));
    }
    // ImagePicker.openPicker({
    //   cropping: true,
    //   includeBase64: true,
    //   compressImageQuality: 0.6,
    // }).then((images) => {
    //   var Attachment = {
    //     FileName: 'CircularAttachment' + new Date().getTime(),
    //     FileType: images.mime,
    //     Attachment: images.data,
    //   };
    //   this.setState({EmailAttachment: Attachment});
    // });
  };
  selectNotificationImage = async () => {
    try {
      const res = await DocumentPicker.pick({
        includeBase64: true,
        type: [DocumentPicker.types.images],
      });
      let typeIdentifier = res.type.split('/');
      console.log(typeIdentifier[1]);
      //alert(typeIdentifier[1]);
      RNFetchBlob.fs
        .readFile(res.uri, 'base64')
        .then((data) => {
          var Attachment = {
            FileName: res.name,
            FileType: res.type,
            Attachment: data,
          };

          this.setState({NotificationAttachment: Attachment});
        })
        .catch((err) => {});
    } catch (err) {
      console.log('Unknown Error: ' + JSON.stringify(err));
    }
    // ImagePicker.openPicker({
    //   cropping: true,
    //   includeBase64: true,
    //   compressImageQuality: 0.6,
    // }).then((images) => {
    //   var Attachment = {
    //     FileName: 'NotificationAttachment' + new Date().getTime(),
    //     FileType: images.mime,
    //     Attachment: images.data,
    //   };
    //   this.setState({NotificationAttachment: Attachment});
    // });
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="E-Circular Step-3"
            showBack={true}
            backScreen="CircularStep2"
            navigation={this.props.navigation}
          />
          <Content>
            <View style={styles.cardContainer}>
              <View
                style={{marginTop: '4%', marginLeft: '4%', marginRight: '4%'}}>
                <View style={{flexDirection: 'row'}}>
                  {/* <View style={{flexDirection: 'row'}}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.isSMS}
                      onClick={this.handleCheckbox1}
                    />
                    <Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>
                      SMS
                    </Text>
                  </View> */}

                  <View style={{flexDirection: 'row', marginLeft: wp('3')}}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.isEmail}
                      onClick={this.handleCheckbox2}
                    />
                    <Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>
                      EMAIL
                    </Text>
                  </View>

                  <View style={{flexDirection: 'row', marginLeft: wp('3')}}>
                    <CheckBox
                      checkedCheckBoxColor="#f05760"
                      isChecked={this.state.isNotification}
                      onClick={this.handleCheckbox3}
                    />
                    <Text style={{fontFamily: 'Poppins-Regular', fontSize: 13}}>
                      SEND NOTIFICATION
                    </Text>
                  </View>
                </View>
              </View>
              <View
                style={{marginTop: '4%', marginLeft: '4%', marginRight: '4%'}}>
                <View style={styles.labelContainer}>
                  <Text style={styles.label}>Subject</Text>
                </View>
                <View>
                  <Item regular style={styles.item}>
                    <Input
                      placeholder=""
                      style={styles.input}
                      value={this.state.subject}
                      onChangeText={(subject) => {
                        this.setState({subject: subject});
                      }}
                    />
                  </Item>
                </View>
              </View>
              {
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '4%',
                    marginRight: '4%',
                  }}>
                  <View style={styles.labelContainer}>
                    <View style={styles.row}>
                      <Text style={styles.label}>Description</Text>
                      {/* <Text style={styles.optional}>
                        (Optional for only sms circular)
                      </Text> */}
                    </View>
                  </View>
                  <View style={styles.item1}>
                    <Textarea
                      rowSpan={7}
                      bordered
                      style={styles.textarea}
                      value={this.state.circular}
                      onChangeText={(circular) => {
                        this.setState({circular: circular});
                      }}
                    />
                  </View>
                </View>
              }

              <View
                style={{marginTop: '4%', marginLeft: '4%', marginRight: '4%'}}>
                <TouchableWithoutFeedback onPress={this.SelectCircularImage}>
                  <View>
                    <Item regular disabled style={styles.item}>
                      <Input
                        placeholder="Upload Circular Attachment"
                        style={styles.input}
                        value={this.state.EmailAttachment.FileName}
                        disabled
                      />
                    </Item>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              {this.state.isNotification && (
                <View
                  style={{
                    marginTop: '4%',
                    marginLeft: '4%',
                    marginRight: '4%',
                  }}>
                  <TouchableWithoutFeedback
                    onPress={this.selectNotificationImage}>
                    <View>
                      <Item regular disabled style={styles.item}>
                        <Input
                          placeholder="Upload Notification Background Image"
                          style={styles.input}
                          value={this.state.NotificationAttachment.FileName}
                          disabled
                        />
                      </Item>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              )}
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: hp('3'),
                }}>
                <TouchableWithoutFeedback onPress={this.send}>
                  <View style={styles.buttonContainer}>
                    <Image
                      source={require('../../assets/ic_send.png')}
                      style={styles.btImage}
                    />
                    <Text style={styles.buttonText}>Send</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  cardContainer: {
    marginTop: '5%',
    marginLeft: '6%',
    marginRight: '6%',
    justifyContent: 'center',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#000000',
  },
  label1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#c9c3c5',
    paddingLeft: wp('3'),
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#ffffff',
    paddingLeft: '5%',
    borderRadius: 10,
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#ffffff',
    height: hp('7'),
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },

  item1: {
    borderRadius: 8,
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  textarea: {
    borderLeftWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderRadius: 8,
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#ffffff',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('30'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  btImage: {
    width: 54,
    height: 39,
  },
  row: {
    flexDirection: 'row',
  },
  optional: {
    fontFamily: 'Poppins-Regular',
    color: 'red',
    fontSize: 12,
    margin: 2,
  },
});
