import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableWithoutFeedback} from 'react-native';
import {
  Container,
  Content,
  List,
  ListItem,
  Icon,
  Item,
  Input,
  Footer,
  Card,
} from 'native-base';
import SubHeader from '../common/SubHeader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Const from '../common/Constants';
import AwesomeAlert from 'react-native-awesome-alerts';
import Loader from '../common/Loader';
import CheckBox from 'react-native-check-box';
import AsyncStorage from '@react-native-community/async-storage';
import Nodata from '../common/NoData';
export default class CircularStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      dataSource: [],
      showAlert: false,
      loader: false,
      isLoading: true,
      text: '',
      bearer_token: '',
      showAlert1: false,
      error_message: '',
    };
    this.arrayholder = [];
  }
  componentDidMount() {
    AsyncStorage.getItem('bearer_token').then((bearer_token) => {
      this.setState({bearer_token: bearer_token}, () => {
        this.getData(bearer_token);
      });
    });
  }
  send = () => {
    var staffArr = [];
    this.state.dataSource.map((item, index) => {
      {
        item.others.map((item1, index1) => {
          item1.member.map((item2, index1) => {
            if (item2.isChecked) {
              staffArr.push(item2.id);
            }
          });
        });
      }
    });
    if (staffArr.length == 0) {
      this.setState({
        showAlert1: true,
        error_message: 'Select staff members',
        showAlert: false,
      });
    } else {
      this.props.navigation.navigate('CircularStep3', {data: staffArr});
    }
  };
  getData = (bearer) => {
    var college = this.props.route.params.collage;
    var department = this.props.route.params.department;
    var designation = this.props.route.params.designation;
    this.setState({loader: true});
    fetch(Const + 'api/StaffTaskAllotment/GetTreeForStaffAllotment', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        Institite_Id: college[0],
        DepartmentList: department,
        DesignationList: designation,
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        if (Object.entries(json).length > 0) {
          var mainArr = [];
          var mainArr1 = [];
          Object.entries(json).map(([make1, type1]) => {
            var arr = [];
            mainArr.push(make1);
            mainArr.map((item, index) => {
              Object.entries(type1).map(([make, type]) => {
                var mem = type;
                var memArr = [];
                for (var a = 0; a < mem.length; a++) {
                  var memObj = {
                    id: '',
                    name: '',
                    isChecked: false,
                  };
                  var sliptmember = mem[a].split('-');
                  var memObj = {
                    id: sliptmember[1],
                    name: sliptmember[0],
                  };
                  memArr.push(memObj);
                }
                var obj = {
                  desig: make,
                  member: memArr,
                  is_checked: false,
                };
                arr.push(obj);
              });
              var newObj = {
                id: item,
                name: item,
                isChecked: false,
                others: arr,
              };
              mainArr1.push(newObj);
            });
          });
          this.arrayholder = mainArr1;
          this.setState({loader: false, dataSource: mainArr1});
        } else {
          this.setState({loader: false, dataSource: []});
        }
      })
      .catch((error) => {
        this.setState({loader: false});
      });
  };
  SearchFilterFunction(text) {
    const newData = this.arrayholder.filter(function (item, index) {
      const itemData = item.desig ? item.desig.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      dataSource: newData,
      text: text,
    });
  }
  handleCheckbox = (mainIndex, secondIndex, index, status) => {
    const newArray = [...this.state.dataSource];
    newArray[mainIndex].others[secondIndex].member[index].isChecked = !status;
    this.setState({dataSource: newArray});
  };
  handleCheckbox1 = (mainIndex, secondIndex, status) => {
    const newArray = [...this.state.dataSource];
    newArray[mainIndex].others[secondIndex].isChecked = !status;
    var IndexMemberArr = newArray[mainIndex].others[secondIndex].member;
    IndexMemberArr.map((item, index) => {
      if (!status) {
        item.isChecked = true;
      } else if (status) {
        item.isChecked = false;
      }
    });
    this.setState({dataSource: newArray});
  };
  handleCheckbox0 = (mainIndex, status) => {
    const newArray = [...this.state.dataSource];
    newArray[mainIndex].isChecked = !status;
    var IndexDesigArr = newArray[mainIndex].others;
    IndexDesigArr.map((item, index) => {
      if (!status) {
        item.isChecked = true;
      } else if (status) {
        item.isChecked = false;
      }
      item.member.map((item1, index1) => {
        if (!status) {
          item1.isChecked = true;
        } else if (status) {
          item1.isChecked = false;
        }
      });
    });
    this.setState({dataSource: newArray});
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    if (this.state.dataSource.length === 0) {
      return (
        <>
          <SubHeader
            title="E-Circular Step-2"
            showBack={true}
            backScreen="Circular"
            navigation={this.props.navigation}
          />
          <Content>
            <View style={{marginTop: hp('37')}}>
              <Nodata title="No Data Found" />
            </View>
          </Content>
        </>
      );
    }
    return (
      <Container>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <SubHeader
          title="E-Circular Step-2"
          showBack={true}
          backScreen="Circular"
          navigation={this.props.navigation}
        />
        <Content>
          <View style={{margin: '1%'}}>
            {/* <View style={{marginTop:'4%',marginLeft:'1%',marginRight:'1%'}}>
                            <View>
                            <Item regular  style={styles.item}>
                                <Input 
                                    placeholder='Search by department...' 
                                    style={styles.input}
                                    value = {this.state.userNameOrEmailAddress}
                                    onChangeText={text => this.SearchFilterFunction(text)}
                                 />
                                <Icon active name='search' type="FontAwesome" style={{fontSize:17,color:'#f05760'}} onPress={this.showPassword} />
                            </Item>
                        </View>
                    </View> */}
            <View style={{marginTop: 15}}>
              {this.state.dataSource.map((item0, index0) => {
                return (
                  <View key={item0.id}>
                    <View style={styles.row}>
                      <CheckBox
                        isChecked={item0.isChecked}
                        onClick={() =>
                          this.handleCheckbox0(index0, item0.isChecked)
                        }
                        checkedCheckBoxColor="#f05760"
                      />
                      <Text
                        style={{
                          fontFamily: 'Poppins-SemiBold',
                          fontSize: 14,
                          marginLeft: 3,
                          marginBottom: 8,
                        }}>
                        {item0.name}
                      </Text>
                    </View>
                    <List>
                      {item0.others.map((item, index1) => {
                        return (
                          <Card
                            style={{
                              margin: '1%',
                              elevation: 4,
                              borderRadius: 10,
                              padding: '1%',
                            }}
                            key={index1}>
                            <ListItem itemDivider style={{borderRadius: 10}}>
                              <CheckBox
                                isChecked={item.isChecked}
                                onClick={() =>
                                  this.handleCheckbox1(
                                    index0,
                                    index1,
                                    item.isChecked,
                                  )
                                }
                                checkedCheckBoxColor="#f05760"
                              />
                              <Text
                                style={{
                                  fontFamily: 'Poppins-SemiBold',
                                  fontSize: 14,
                                }}>
                                {item.desig}
                              </Text>
                            </ListItem>
                            {item.member.map((item1, index) => {
                              return (
                                <ListItem key={item1.id}>
                                  <CheckBox
                                    isChecked={item1.isChecked}
                                    onClick={() =>
                                      this.handleCheckbox(
                                        index0,
                                        index1,
                                        index,
                                        item1.isChecked,
                                      )
                                    }
                                    checkedCheckBoxColor="#f05760"
                                  />
                                  <Text
                                    style={{
                                      fontFamily: 'Poppins-Regular',
                                      fontSize: 12,
                                      marginLeft: wp('2'),
                                    }}>
                                    {item1.name}
                                  </Text>
                                </ListItem>
                              );
                            })}
                          </Card>
                        );
                      })}
                    </List>
                  </View>
                );
              })}
            </View>
          </View>
        </Content>
        <TouchableWithoutFeedback onPress={this.send}>
          <Footer
            style={{
              backgroundColor: '#f05760',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={styles.footerText}>Next Step</Text>
          </Footer>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    margin: '1%',
  },
  date: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12.5,
    margin: '1%',
    color: '#909090',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#c9c3c5',
  },
  label1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#c9c3c5',
    paddingLeft: wp('3'),
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#ffffff',
    paddingLeft: '5%',
    borderRadius: 10,
    borderColor: '#f1f1f1',
    borderWidth: 1,
    borderRightWidth: 0,
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#ffffff',
    height: hp('7'),
    borderColor: '#f1f1f1',
    borderWidth: 1,
    elevation: 2,
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#ffffff',
  },
  row: {
    flexDirection: 'row',
  },
});
