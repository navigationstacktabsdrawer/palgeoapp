import AsyncStorage from '@react-native-community/async-storage';
import {coordinatestoaddress} from '../utils';
import Geolocation from 'react-native-geolocation-service';
var geolib = require('geolib');
// AsyncStorage.getItem('checkin_time').then((checkin_time) => {
//   AsyncStorage.getItem('coordinates').then((coordinates) => {
//     AsyncStorage.getItem('driving_type').then((driving_type) => {
//       AsyncStorage.getItem('start_time').then((start_time) => {
//         if (checkin_time) {
//           var parseCoordinates = JSON.parse(coordinates);
//           Geolocation.getCurrentPosition(
//             (position) => {
//               const currentLongitude = JSON.stringify(
//                 position.coords.longitude,
//               );
//               const currentLatitude = JSON.stringify(position.coords.latitude);
//               var orgin = currentLatitude + ',' + currentLongitude;
//               var destination =
//                 parseCoordinates[0].latitude +
//                 ',' +
//                 parseCoordinates[0].longitude;
//               fetch(
//                 'https://maps.googleapis.com/maps/api/directions/json?origin=' +
//                   orgin +
//                   '&destination=' +
//                   destination +
//                   '&mode=' +
//                   driving_type +
//                   '&key=AIzaSyDrH5O4x0LVfDOHIOcuXFj7_cS4Rz4eUxM',
//                 {
//                   method: 'GET',
//                 },
//               )
//                 .then((response) => response.json())
//                 .then((json) => {
//                   var google_api_seconds =
//                     json.routes[0].legs[0].duration.value;

//                   var google_api_minutes_plus_start_time =
//                     parseInt(Math.floor(google_api_seconds / 60)) +
//                     parseInt(start_time);

//                   var check_in_time = checkin_time;

//                   var checkin_time_with_date = moment().format(
//                     'YYYY-MM-DD ' + check_in_time,
//                   );

//                   var added_twenty_minutes = moment(
//                     checkin_time_with_date,
//                   ).subtract(google_api_minutes_plus_start_time, 'minutes');

//                   var check_in_date_after_subracting_total_minutes = moment(
//                     added_twenty_minutes,
//                   ).format('YYYY-MM-DD  h:mm');

//                   var x = moment(current_date.toString(), 'YYYY-MM-DD h:mm');

//                   var y = moment(
//                     check_in_date_after_subracting_total_minutes.toString(),
//                     'YYYY-MM-DD h:mm',
//                   );

//                   if (x.isSame(y)) {
//                     if (PlatForm.OS == 'android') {
//                       PushNotification.localNotification({
//                         id: 0,
//                         title: 'Attention!',
//                         message:
//                           'You need to start now to check in without any delay',
//                         playSound: true,
//                         soundName: 'default',
//                         priority: 'high',
//                         visibility: 'public',
//                         importance: 'high',
//                       });
//                     } else {
//                       PushNotificationIOS.presentLocalNotification({
//                         id: 0,
//                         title: 'Attention!',
//                         message:
//                           'You need to start now to check in without any delay',
//                         playSound: true,
//                         soundName: 'default',
//                         priority: 'high',
//                         visibility: 'public',
//                         importance: 'high',
//                       });
//                     }
//                   }
//                 })
//                 .catch((error) => {
//                   console.log(error);
//                 });
//             },
//             (error) => {
//               console.log(error);
//             },
//             {enableHighAccuracy: true, timeout: 2000, maximumAge: 0},
//           );
//         }
//       });
//     });
//   });
// });

export const checkCoordinates = async () => {
  const type = await AsyncStorage.getItem('type');
  const checked_out = await AsyncStorage.getItem('checked_out');
  const coordinates = await AsyncStorage.getItem('coordinates');
  const radius = await AsyncStorage.getItem('radius');
  if (checked_out === 'no') {
    if (type && type === 'Circle') {
      Geolocation.getCurrentPosition(
        (position) => {
          const currentLongitude = JSON.stringify(position.coords.longitude);
          const currentLatitude = JSON.stringify(position.coords.latitude);
          var circlepoint = JSON.parse(coordinates);
          const point = geolib.isPointWithinRadius(
            {
              latitude: parseFloat(currentLatitude),
              longitude: parseFloat(currentLongitude),
            },
            {
              latitude: parseFloat(circlepoint[0].latitude),
              longitude: parseFloat(circlepoint[0].longitude),
            },
            radius,
          );
          return point;
          // if (!point) {
          //   //callAPI
          //   return 'You are outside your geofence';
          // } else {

          // }
        },
        (error) => {},
        {enableHighAccuracy: true, maximumAge: 0},
      );
    }
  }
};

// AsyncStorage.getItem('type').then((type) => {
//   AsyncStorage.getItem('coordinates').then((coordinates) => {
//     AsyncStorage.getItem('radius').then((radius) => {
//       if (type) {
//         if (type == 'Circle') {
//           Geolocation.getCurrentPosition(
//             (position) => {
//               const currentLongitude = JSON.stringify(
//                 position.coords.longitude,
//               );
//               const currentLatitude = JSON.stringify(position.coords.latitude);
//               var circlepoint = JSON.parse(coordinates);
//               const point = geolib.isPointWithinRadius(
//                 {
//                   latitude: parseFloat(currentLatitude),
//                   longitude: parseFloat(currentLongitude),
//                 },
//                 {
//                   latitude: parseFloat(circlepoint[0].latitude),
//                   longitude: parseFloat(circlepoint[0].longitude),
//                 },
//                 radius,
//               );
//               if (point) {
//               } else {
//                 if (PlatForm.OS == 'android') {
//                   PushNotification.localNotification({
//                     id: 0,
//                     title: 'Attention!',
//                     message: 'You are outside your assigned location.',
//                     playSound: true,
//                     soundName: 'default',
//                     priority: 'high',
//                     visibility: 'public',
//                     importance: 'high',
//                   });
//                 } else {
//                   PushNotificationIOS.presentLocalNotification({
//                     id: 0,
//                     title: 'Attention!',
//                     message:
//                       'You need to start now to check in without any delay',
//                     playSound: true,
//                     soundName: 'default',
//                     priority: 'high',
//                     visibility: 'public',
//                     importance: 'high',
//                   });
//                 }
//               }
//             },
//             (error) => {},
//             {enableHighAccuracy: true, timeout: 2000, maximumAge: 0},
//           );
//         } else {
//           Geolocation.getCurrentPosition(
//             (position) => {
//               const currentLongitude = JSON.stringify(
//                 position.coords.longitude,
//               );
//               const currentLatitude = JSON.stringify(position.coords.latitude);
//               var response = geolib.isPointInPolygon(
//                 {latitude: currentLatitude, longitude: currentLongitude},
//                 JSON.stringify(coordinates),
//               );
//               if (response) {
//               } else {
//                 if (PlatForm.OS == 'android') {
//                   PushNotification.localNotification({
//                     id: 0,
//                     title: 'Attention!',
//                     message: 'You are outside your assigned location.',
//                     playSound: true,
//                     soundName: 'default',
//                     priority: 'high',
//                     visibility: 'public',
//                     importance: 'high',
//                   });
//                 } else {
//                   PushNotificationIOS.presentLocalNotification({
//                     id: 0,
//                     title: 'Attention!',
//                     message:
//                       'You need to start now to check in without any delay',
//                     playSound: true,
//                     soundName: 'default',
//                     priority: 'high',
//                     visibility: 'public',
//                     importance: 'high',
//                   });
//                 }
//               }
//             },
//             (error) => {},
//             {enableHighAccuracy: true, timeout: 2000, maximumAge: 0},
//           );
//         }
//       }
//     });
//   });
// })
