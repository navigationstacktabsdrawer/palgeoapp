import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  Platform,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Content} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import RNOtpVerify from 'react-native-otp-verify';

export default class OtpVerification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      mobileNumber: '',
      otp: '',
      showAlert: false,
      showAlert1: false,
      error_message: '',
      resendButtonDisabledTime: 15,
      hash: '',
    };
    this.resendOtpTimerInterval;
  }

  startListeningForOtp = () => {
    RNOtpVerify.getOtp()
      .then((p) => RNOtpVerify.addListener(this.otpHandler))
      .catch((p) => console.log(p));
  };
  otpHandler = (message) => {
    console.log('messageOTP', message);
    const otp = /(\d{4})/g.exec(message)[1];
    this.setState({otp: otp});
    RNOtpVerify.removeListener();
    this.verifyOtp(otp);
    Keyboard.dismiss();
  };
  componentDidMount() {
    setTimeout(() => {
      this.setState({loader: false});
    }, 700);
    if (Platform.OS == 'android') {
      if (this.props.route?.params?.message)
        alert(this.props.route.params.message);
      //this.startListeningForOtp();
      this.startResendOtpTimer();
    }
  }

  startResendOtpTimer = () => {
    if (this.resendOtpTimerInterval) {
      clearInterval(this.resendOtpTimerInterval);
    }
    this.resendOtpTimerInterval = setInterval(() => {
      if (this.state.resendButtonDisabledTime <= 0) {
        clearInterval(this.resendOtpTimerInterval);
      } else {
        this.setState({
          resendButtonDisabledTime: this.state.resendButtonDisabledTime - 1,
        });
        //setResendButtonDisabledTime(resendButtonDisabledTime - 1);
      }
    }, 1000);
  };

  verifyOtp = (otp) => {
    if (this.props.route.params.mobile) {
      if (otp) {
        this.setState({showAlert: true});
        fetch(Const + 'api/MobileApp/VerifyOTP', {
          method: 'POST',
          headers: {
            Accept: 'text/plain',
            'content-type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            mobileNumber: this.props.route.params.mobile,
            otp: otp,
          }),
        })
          .then((response) => response.json())
          .then((json) => {
            if (json) {
              if (Platform.OS == 'ios') {
                alert('Otp verified successfully');
                setTimeout(() => {
                  this.setState({showAlert: false});
                }, 1200);
                setTimeout(() => {
                  this.props.navigation.navigate('SetMpin', {
                    mobile: this.props.route.params.mobile,
                  });
                }, 1300);
              } else {
                this.setState({
                  showAlert1: true,
                  error_message: 'Otp verified successfully',
                  showAlert: false,
                });
                setTimeout(() => {
                  this.props.navigation.navigate('SetMpin', {
                    mobile: this.props.route.params.mobile,
                  });
                }, 1200);
              }
            } else {
              if (Platform.OS == 'ios') {
                alert('Enter valid otp');
                setTimeout(() => {
                  this.setState({showAlert: false});
                }, 1200);
              } else {
                this.setState({
                  showAlert1: true,
                  error_message: 'Enter valid otp',
                  showAlert: false,
                });
              }
            }
          })
          .catch((error) => {
            if (Platform.OS == 'ios') {
              alert(
                'You might not be connected to Internet. Please check your internet connection.',
              );
              setTimeout(() => {
                this.setState({showAlert: false});
              }, 1200);
            } else {
              this.setState({
                showAlert1: true,
                error_message:
                  'You might not be connected to Internet. Please check your internet connection.',
                showAlert: false,
              });
            }
          });
      } else {
        if (Platform.OS == 'ios') {
          alert('Otp is required');
          setTimeout(() => {
            this.setState({showAlert: false});
          }, 1200);
        } else {
          this.setState({
            showAlert1: true,
            error_message: 'Otp is required',
            showAlert: false,
          });
        }
      }
    } else {
      if (Platform.OS == 'ios') {
        alert('Invalid Request');
        setTimeout(() => {
          this.setState({showAlert: false});
        }, 1200);
        setTimeout(() => {
          this.props.navigation.navigate('RegisterPin');
        }, 1300);
      } else {
        this.setState({
          showAlert1: true,
          error_message: 'Invalid request.Try again',
          showAlert: false,
        });
        setTimeout(() => {
          this.props.navigation.navigate('RegisterPin');
        }, 1200);
      }
    }
  };

  onResendOtpButtonPress = () => {
    if (Platform.OS == 'android') {
      this.getHashd();
    }
    this.setState({resendButtonDisabledTime: 15, otp: ''});
    this.setState({showAlert: true});
    fetch(Const + 'api/MobileApp/SendVerificationOTP', {
      method: 'POST',
      headers: {
        Accept: 'text/plain',
        'content-type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        mobileNo: this.props.route.params.mobile,
        haskKey: this.state.hash.toString(),
        CountryCode: this.props.route.params.CountryCode,
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        console.log('[JSON]', json);
        if (json.status) {
          this.setState({showAlert: false});
          this.setState({showAlert1: true});
          this.setState({error_message: 'Otp sent successfully'});
          alert(json.message);
          //TODO
          //Remove the above alert message and uncomment below line in next release
          //this.startResendOtpTimer();
        }
      })
      .catch((error) => alert('Something went wrong. Please try again'));
  };

  getHashd = () => {
    RNOtpVerify.getHash()
      .then((hash) => {
        this.setState({hash: hash});
      })
      .catch(console.log);
  };

  showPassword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };
  registerMpin = () => {
    this.props.navigation.navigate('RegisterPin');
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    const {resendButtonDisabledTime} = this.state;
    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="MPIN Register Step-2"
            showBack={true}
            backScreen="RegisterPin"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View style={styles.firstContainer}>
              <Image
                source={require('../../assets/ic_welcome.png')}
                style={styles.loginImage}
                resizeMode="contain"
              />
            </View>
            <View style={styles.secondContainer}>
              <Text style={styles.otpHeader}>Verification code</Text>
              <Text style={styles.otpSub}>
                Otp has been sent to your mobile{' '}
              </Text>
              <Text style={styles.otpSub}>number.Please verify. </Text>
              <OTPInputView
                style={{width: wp('80'), height: 80}}
                pinCount={4}
                autoFocusOnLoad={false}
                secureTextEntry={true}
                codeInputFieldStyle={styles.inputFeilds}
                codeInputHighlightStyle={styles.inputFeildsFocus}
                onCodeFilled={(code) => this.setState({otp: code})}
              />

              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: hp('3'),
                }}>
                <TouchableWithoutFeedback
                  onPress={() => this.verifyOtp(this.state.otp)}>
                  <View style={styles.buttonContainer}>
                    <Image
                      source={require('../../assets/bt.png')}
                      style={styles.btImage}
                    />
                    <Text style={styles.buttonText}>Verify</Text>
                  </View>
                </TouchableWithoutFeedback>
                {resendButtonDisabledTime > 0 ? (
                  <Text style={styles.invisibleText}>
                    Resend OTP in {resendButtonDisabledTime} sec
                  </Text>
                ) : (
                  <TouchableOpacity onPress={this.onResendOtpButtonPress}>
                    <Text style={styles.invisibleText}>Resend OTP</Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: '6%',
    backgroundColor: '#4dbd74',
    height: hp('5.8'),
    borderRadius: 5,
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#ffffff',
  },
  inputFeilds: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    color: '#000000',
    borderRadius: 10,
  },
  inputFeildsFocus: {
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#f05760',
    color: '#000000',
    borderRadius: 10,
  },
  otpHeader: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    color: '#000',
    margin: '1%',
  },
  otpSub: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#000000',
  },
  firstContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('30'),
    backgroundColor: '#f05760',
  },
  secondContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: hp('6'),
  },
  loginImage: {
    width: 250,
    height: 200,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('30'),
    paddingRight: wp('7'),
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  btImage: {
    width: 54,
    height: 39,
  },
  invisibleText: {
    color: 'red',
    fontSize: 12,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 20,
  },
});
