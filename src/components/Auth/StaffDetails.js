import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {Container, Content, Card} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import RNOtpVerify from 'react-native-otp-verify';
export default class StaffDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      mobileNo: '',
      hash: '',
      showAlert: false,
      showAlert1: false,
      error_message: '',
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({loader: false});
    }, 700);
    if (Platform.OS == 'android') {
      this.getHashd();
    }
  }

  getHashd = () => {
    RNOtpVerify.getHash()
      .then((hash) => {
        this.setState({hash: hash});
      })
      .catch(console.log);
  };
  sendOtp = () => {
    if (this.props.route.params.mobile) {
      this.setState({showAlert: true});
      fetch(Const + 'api/MobileApp/SendVerificationOTP', {
        method: 'POST',
        headers: {
          Accept: 'text/plain',
          'content-type': 'application/json-patch+json',
        },
        body: JSON.stringify({
          mobileNo: this.props.route.params.mobile,
          haskKey: this.state.hash.toString(),
          CountryCode: this.props.route.params.CountryCode,
        }),
      })
        .then((response) => response.json())
        .then((json) => {
          console.log('OTP_JSON ==>', json);
          if (json.status) {
            this.setState({showAlert: false});
            this.props.navigation.navigate('OtpVerification', {
              mobile: this.props.route.params.mobile,
              CountryCode: this.props.route.params.CountryCode,
              message: json.message,
            });
          } else {
            if (Platform.OS == 'ios') {
              alert(json.message);
              setTimeout(() => {
                this.setState({showAlert: false});
              }, 1200);
            } else {
              this.setState({
                showAlert1: true,
                error_message: json.message,
                showAlert: false,
              });
            }
          }
        })
        .catch((error) => {
          if (Platform.OS == 'ios') {
            alert(error.toString());
            setTimeout(() => {
              this.setState({showAlert: false});
            }, 1200);
          } else {
            this.setState({
              showAlert1: true,
              error_message: error.toString(),
              showAlert: false,
            });
          }
        });
    } else {
      if (Platform.OS == 'ios') {
        alert('Enter your mobile number');
        setTimeout(() => {
          this.props.navigation.navigate('RegisterPin');
        }, 1200);
      } else {
        this.setState({
          showAlert1: true,
          error_message: 'Enter your mobile number',
          showAlert: false,
        });
        setTimeout(() => {
          this.props.navigation.navigate('RegisterPin');
        }, 1200);
      }
    }
  };
  render() {
    //console.log(this.props.route.params.CountryCode);
    return (
      <Container style={{backgroundColor: '#ffffff'}}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <SubHeader
          title="Staff Details"
          showBack={true}
          backScreen="RegisterPin"
          navigation={this.props.navigation}
        />
        <View style={styles.upperBackground}></View>
        <Content>
          <View
            style={{
              backgroundColor: '#ffffff',
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40,
            }}>
            <View style={styles.boxContainer}>
              <Card style={styles.card}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Name</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.props.route.params.name}
                  </Text>
                </View>
              </Card>
              <Card style={styles.card}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Institute Name</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.props.route.params.institute}
                  </Text>
                </View>
              </Card>
              <Card style={styles.card}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Department</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.props.route.params.department}
                  </Text>
                </View>
              </Card>
              <Card style={styles.card}>
                <View style={{width: wp('30'), justifyContent: 'center'}}>
                  <Text style={styles.header}>Designation</Text>
                </View>
                <View style={{width: wp('50')}}>
                  <Text style={styles.headerVal}>
                    {this.props.route.params.designation}
                  </Text>
                </View>
              </Card>
            </View>
            <View style={styles.center}>
              <TouchableWithoutFeedback onPress={this.sendOtp}>
                <View style={styles.buttonContainer}>
                  <Text style={styles.footerText}>Send Otp</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    margin: '1%',
  },
  date: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12.5,
    margin: '1%',
    color: '#909090',
  },
  boxContainer: {
    margin: '5%',
    backgroundColor: '#ffffff',
  },
  header: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#a29b9c',
  },
  headerVal: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#170607',
  },
  row: {
    flexDirection: 'row',
  },
  divider: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    margin: '2%',
  },
  divider1: {
    borderWidth: 1,
    borderColor: '#f3f3f3',
    margin: '2%',
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#ffffff',
  },
  upperBackground: {
    position: 'absolute',
    width: wp('100'),
    height: 100,
    backgroundColor: '#f05760',
    top: 37,
  },
  card: {
    padding: '2%',
    flexDirection: 'row',
    borderRadius: 5,
    elevation: 4,
  },
  buttonContainer: {
    width: wp('80'),
    backgroundColor: '#f05760',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    borderRadius: 30,
    marginTop: hp('1'),
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#ffffff',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
