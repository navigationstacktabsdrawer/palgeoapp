import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Content} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import OTPInputView from '@twotalltotems/react-native-otp-input';
export default class SetMpin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: true,
      mobileNumber: '',
      mpin: '',
      mpin1: '',
      device_token: '',
      autoFocus: false,
      showAlert: false,
      showAlert1: false,
      error_message: '',
    };
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({loader: false});
    }, 700);
    AsyncStorage.getItem('device_token').then((device_token) => {
      this.setState({device_token: device_token});
      console.log('device_tok', device_token);
    });
  }
  setMpin = () => {
    this.setState({showAlert: true});
    if (this.props.route.params.mobile) {
      if (this.state.mpin && this.state.mpin1) {
        if (this.state.mpin === this.state.mpin1) {
          fetch(Const + 'api/MobileApp/RegisterMPIN', {
            method: 'POST',
            headers: {
              Accept: 'text/plain',
              'content-type': 'application/json-patch+json',
            },
            body: JSON.stringify({
              mobileNumber: this.props.route.params.mobile,
              mpin: this.state.mpin1,
              deviceToken: this.state.device_token,
            }),
          })
            .then((response) => response.json())
            .then((json) => {
              if (json) {
                if (Platform.OS == 'ios') {
                  alert('Mpin registered successfully.Please login');
                  setTimeout(() => {
                    this.setState({showAlert: false});
                  }, 1300);
                  try {
                    AsyncStorage.setItem(
                      'mobile',
                      this.props.route.params.mobile.toString(),
                    );
                    AsyncStorage.setItem('mpin', this.state.mpin.toString());
                    setTimeout(() => {
                      this.props.navigation.navigate('Login');
                    }, 1200);
                  } catch (e) {
                    console.log(e);
                  }
                } else {
                  this.setState({
                    showAlert1: true,
                    error_message: 'Mpin registered successfully.Please login',
                    showAlert: false,
                  });
                  try {
                    AsyncStorage.setItem(
                      'mobile',
                      this.props.route.params.mobile.toString(),
                    );
                    setTimeout(() => {
                      this.props.navigation.navigate('Login');
                    }, 1200);
                  } catch (e) {
                    console.log(e);
                  }
                }
              } else {
                if (Platform.OS == 'ios') {
                  alert(
                    'You might not be connected to Internet. Please check your internet connection..Try later',
                  );
                  setTimeout(() => {
                    this.setState({showAlert: false});
                  }, 1200);
                } else {
                  this.setState({
                    showAlert1: true,
                    error_message:
                      'You might not be connected to Internet. Please check your internet connection..Try later',
                    showAlert: false,
                  });
                }
              }
            })
            //        }else{
            //             if(Platform.OS == "ios"){
            //                 alert('Unknown error occured.Try later');
            //                 setTimeout(()=>{this.setState({showAlert:false})},1200)
            //             }else{
            //                 this.setState({showAlert1:true,error_message:'Unknown error occured.Try later',showAlert : false});
            //             }
            //         }
            //   })
            //   .catch((error) => {
            //         if(Platform.OS == "ios"){
            //             alert('Unknown error occured.Try later');
            //             setTimeout(()=>{this.setState({showAlert:false})},1200)
            //         }else{
            //             this.setState({showAlert1:true,error_message:'Unknown error occured.Try later',showAlert : false});
            //         }
            //  });
            // }else{
            //     if(Platform.OS == "ios"){
            //         alert('Please enter same values');
            //         setTimeout(()=>{this.setState({showAlert:false})},1200)
            //     }else{
            //         this.setState({showAlert1:true,error_message:'Please enter same values',showAlert : false});
            //     }
            // }
            .catch((error) => {
              console.log(error);
              if (Platform.OS == 'ios') {
                alert(
                  'You might not be connected to Internet. Please check your internet connection..Try later',
                );
                setTimeout(() => {
                  this.setState({showAlert: false});
                }, 1200);
              } else {
                this.setState({
                  showAlert1: true,
                  error_message:
                    'You might not be connected to Internet. Please check your internet connection..Try later',
                  showAlert: false,
                });
              }
            });
        } else {
          if (Platform.OS == 'ios') {
            alert('Please enter same values');
            setTimeout(() => {
              this.setState({showAlert: false});
            }, 1200);
          } else {
            this.setState({
              showAlert1: true,
              error_message: 'Please enter same values',
              showAlert: false,
            });
          }
        }
      } else {
        if (Platform.OS == 'ios') {
          alert('Mpin cannot be empty');
          setTimeout(() => {
            this.setState({showAlert: false});
          }, 1200);
        } else {
          this.setState({
            showAlert1: true,
            error_message: 'Mpin cannot be empty',
            showAlert: false,
          });
        }
      }
    } else {
      if (Platform.OS == 'ios') {
        alert('Invalid Request');
        setTimeout(() => {
          this.setState({showAlert: false});
        }, 1200);
        setTimeout(() => {
          this.props.navigation.navigate('RegisterPin');
        }, 1300);
      } else {
        this.setState({
          showAlert1: true,
          error_message: 'Invalid Request',
          showAlert: false,
        });
        setTimeout(() => {
          this.props.navigation.navigate('RegisterPin');
        }, 1200);
      }
    }
  };
  showPassword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };
  registerMpin = () => {
    this.props.navigation.navigate('RegisterPin');
  };
  handleCode = (code) => {
    console.log('hiii');
    this.setState({mpin: code, autoFocus: true});
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="MPIN Register Step-3"
            showBack={true}
            backScreen="OtpVerification"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View style={styles.cardContainer}>
              <Text style={styles.otpHeader}>Enter your Mpin</Text>
              <OTPInputView
                style={{width: wp('80'), height: 80}}
                pinCount={4}
                autoFocusOnLoad={false}
                secureTextEntry={true}
                codeInputFieldStyle={styles.inputFeilds}
                codeInputHighlightStyle={styles.inputFeildsFocus}
                onCodeFilled={(code) => {
                  this.handleCode(code);
                }}
              />

              {this.state.mpin ? (
                <View style={{alignItems: 'center'}}>
                  <Text style={styles.otpHeader}>Re-Enter your MPIN</Text>
                  <OTPInputView
                    style={{width: wp('80'), height: 80}}
                    pinCount={4}
                    autoFocusOnLoad={this.state.autoFocus}
                    secureTextEntry={true}
                    codeInputFieldStyle={styles.inputFeilds}
                    codeInputHighlightStyle={styles.inputFeildsFocus}
                    onCodeFilled={(code1) => {
                      this.setState({mpin1: code1});
                    }}
                  />
                </View>
              ) : null}
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: hp('1'),
                }}>
                <TouchableWithoutFeedback onPress={this.setMpin}>
                  <View style={styles.buttonContainer}>
                    <Image
                      source={require('../../assets/bt.png')}
                      style={styles.btImage}
                    />
                    <Text style={styles.buttonText}>Submit</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  cardContainer: {
    marginTop: '30%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputFeilds: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    color: '#000000',
    borderRadius: 10,
  },
  inputFeildsFocus: {
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#f05760',
    color: '#000000',
    borderRadius: 10,
  },
  otpHeader: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000',
    margin: '1%',
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: '#ffffff',
  },
  otpSub: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#000000',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('30'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  btImage: {
    width: 54,
    height: 39,
  },
});
