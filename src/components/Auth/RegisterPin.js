import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Platform,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Content, Item, Input, Icon} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
export default class RegisterPin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      mobileNo: '',
      hash: '',
      showAlert1: false,
      error_message: '',
      CountryCode: '+91',
    };
  }
  componentDidMount() {
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      if (this.props.route.params != undefined) {
        this.setState({CountryCode: this.props.route.params.CountryCode});
      }
    });
  }

  sendOtp = () => {
    console.log('mobile', this.state.mobileNo);
    if (this.state.mobileNo) {
      if (this.state.mobileNo.length !== 10) {
        return alert('Enter a 10 digit mobile number');
      }
      this.setState({showAlert: true});
      fetch(
        Const +
          'api/MobileApp/StaffDetailsByMobileNumber?mobileNumber=' +
          this.state.mobileNo,
        {
          method: 'GET',
          headers: {
            Accept: 'text/plain',
            'content-type': 'application/json-patch+json',
          },
        },
      )
        .then((response) => response.json())
        .then((json) => {
          console.log(json);
          if (json.length > 0) {
            this.setState({showAlert: false});
            this.props.navigation.navigate('StaffDetails', {
              name: json[0].name,
              institute: json[0].institute,
              department: json[0].department,
              designation: json[0].designation,
              mobile: json[0].mobileNumber,
              CountryCode: this.state.CountryCode,
            });
          } else {
            if (Platform.OS == 'ios') {
              alert('Invalid mobile number');
              setTimeout(() => {
                this.setState({showAlert: false});
              }, 1200);
            } else {
              this.setState({
                showAlert1: true,
                error_message: 'Invalid mobile number',
                showAlert: false,
              });
            }
          }
        })
        .catch((error) => {
          console.log('error', error);
          if (Platform.OS == 'ios') {
            alert(
              'You might not be connected to Internet. Please check your internet connection.',
            );
            setTimeout(() => {
              this.setState({showAlert: false});
            }, 1200);
          } else {
            this.setState({
              showAlert1: true,
              error_message:
                'You might not be connected to Internet. Please check your internet connection.',
              showAlert: false,
            });
          }
        });
    } else {
      if (Platform.OS == 'ios') {
        alert('Enter mobile number');
        setTimeout(() => {
          this.setState({showAlert: false});
        }, 1200);
      } else {
        this.setState({
          showAlert1: true,
          error_message: 'Enter mobile number',
          showAlert: false,
        });
      }
    }
  };
  goToSearchCountry = () => {
    this.props.navigation.navigate('SearchCountry');
  };
  render() {
    console.log(this.state.showAlert1);
    if (this.state.loader) {
      return <Loader />;
    }
    console.log(this.state.CountryCode);
    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          style={{zIndex: 999999}}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="Register Mpin Step one"
            showBack={true}
            backScreen="Login"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View style={styles.firstContainer}>
              <Image
                source={require('../../assets/ic_welcome.png')}
                style={styles.loginImage}
                resizeMode="contain"
              />
            </View>
            <View style={styles.secondContainer}>
              <Text style={styles.otpHeader}>MPIN Register Step one</Text>
              <View
                style={{marginTop: '4%', marginLeft: '4%', marginRight: '4%'}}>
                <View style={styles.labelContainer}>
                  <Text style={styles.label}>Mobile number</Text>
                </View>
                <View>
                  <Item regular style={styles.item}>
                    <TouchableWithoutFeedback onPress={this.goToSearchCountry}>
                      <Text style={styles.code}>{this.state.CountryCode}</Text>
                    </TouchableWithoutFeedback>
                    <Input
                      placeholder=""
                      keyboardType="number-pad"
                      style={styles.input}
                      value={this.state.mobileNo}
                      maxLength={10}
                      onChangeText={(mobileNo) => {
                        this.setState({mobileNo: mobileNo});
                      }}
                    />
                    <Icon
                      active
                      name="phone"
                      type="FontAwesome"
                      style={{fontSize: 17, color: '#f05760'}}
                      onPress={this.showPassword}
                    />
                  </Item>
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: hp('3'),
                }}>
                <TouchableWithoutFeedback onPress={this.sendOtp}>
                  <View style={styles.buttonContainer}>
                    <Image
                      source={require('../../assets/bt.png')}
                      style={styles.btImage}
                    />
                    <Text style={styles.buttonText}>Submit</Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#000',
  },
  label1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#c9c3c5',
    paddingLeft: wp('3'),
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#f1f1f1',
    paddingLeft: '5%',
    borderRadius: 10,
    height: hp('7'),
    width: wp('80'),
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#f1f1f1',
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    height: hp('7'),
    width: wp('80'),
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('30'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  preview: {
    flex: 1,
  },
  firstContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('30'),
    backgroundColor: '#f05760',
  },
  secondContainer: {
    alignItems: 'center',
    marginTop: hp('6'),
  },
  loginImage: {
    width: 250,
    height: 200,
  },
  otpHeader: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000',
  },
  btImage: {
    width: 54,
    height: 39,
  },
  row: {
    flexDirection: 'row',
  },
  code: {
    fontFamily: 'Poppins-Regular',
    marginLeft: '3%',
  },
});
