import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Platform,
  AppState,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Content, Icon, Input, Item} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import AwesomeAlert from 'react-native-awesome-alerts';
import Const from '../common/Constants';
import AsyncStorage from '@react-native-community/async-storage';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Geolocation from 'react-native-geolocation-service';
import {addBoundaries} from '../../utils/helperFunctions';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      mobileNumber: null,
      device_token: '',
      showAlert1: false,
      error_message: '',
      app_status: '',
      CountryCode: '+91',
    };
  }
  async componentDidMount() {
    const mobile = await AsyncStorage.getItem('mobile');
    const app_state = AppState.currentState;
    this.setState({app_status: app_state});
    console.log('current appState', this.state.app_status);
    const device_token = await AsyncStorage.getItem('device_token');
    this.setState({mobileNumber: mobile, device_token: device_token});
    this._unsubscribe = this.props.navigation.addListener('focus', () => {
      AsyncStorage.getItem('mobile').then((mobile) => {
        console.log('[async_mobile]', mobile);
        AsyncStorage.getItem('device_token').then((device_token) => {
          this.setState({mobileNumber: mobile, device_token: device_token});
        });
      });
    });
  }

  componentWillUnmount() {
    this.props.navigation.removeListener(this._unsubscribe);
    //Geolocation.clearWatch(this.watchID);
  }
  handleLogin = (code) => {
    if (this.state.mobileNumber) {
      if (this.state.mobileNumber.length !== 10) {
        return alert('Enter a 10 digit mobile number');
      }
      if (code) {
        this.setState({showAlert: true});
        fetch(Const + 'api/Security/Mobilelogin', {
          method: 'POST',
          headers: {
            Accept: 'text/plain',
            'content-type': 'application/json-patch+json',
          },
          body: JSON.stringify({
            mobileNumber: this.state.mobileNumber,
            mpin: code.toString(),
            device_token: this.state.device_token,
          }),
        })
          .then((response) => response.json())
          .then(async (json) => {
            console.log('body', {
              mobileNumber: this.state.mobileNumber,
              mpin: code.toString(),
            });
            console.log(
              '[LOGIN_JSON => api/Security/Mobilelogin]',
              json.staffNumber,
            );
            if (json.isAuthenticated) {
              fetch(Const + 'api/MobileApp/RegisterMPIN', {
                method: 'POST',
                headers: {
                  Accept: 'text/plain',
                  'content-type': 'application/json-patch+json',
                },
                body: JSON.stringify({
                  mobileNumber: this.state.mobileNumber,
                  mpin: code.toString(),
                  deviceToken: this.state.device_token,
                }),
              })
                .then((response) => response.json())
                .then((json) => {
                  //
                })
                .catch((e) => console.log(e));
              await AsyncStorage.setItem('menus', JSON.stringify(json.menus));
              if (Platform.OS == 'ios') {
                this.setState({showAlert: false});
                try {
                  await AsyncStorage.setItem(
                    'app_status',
                    this.state.app_status,
                  );
                  await AsyncStorage.setItem('user_id', json.staffNumber);
                  await AsyncStorage.setItem('bearer_token', json.bearerToken);
                  await AsyncStorage.setItem(
                    'institute_id',
                    json.instituteId.toString(),
                  );
                  await AsyncStorage.setItem(
                    'org_id',
                    json.organisationId.toString(),
                  );
                  await AsyncStorage.setItem(
                    'driving_type',
                    json.travelPreference.toString(),
                  );
                  await AsyncStorage.setItem(
                    'start_time',
                    json.staffTravelStartTime.toString(),
                  );
                  await AsyncStorage.setItem(
                    'profile_pic',
                    json.profilePic ? json.profilePic : '',
                  );
                  await AsyncStorage.setItem(
                    'base64Data',
                    json.profilePicBase64 ? json.profilePicBase64 : '',
                  );
                  if (json.isTravelCheckInAvailable) {
                    await AsyncStorage.setItem('travel_check_in', 'yes');
                  } else {
                    await AsyncStorage.setItem('travel_check_in', 'no');
                  }
                  if (json.isQrCheckIn) {
                    await AsyncStorage.setItem('qrCheckin', 'true');
                  } else {
                    await AsyncStorage.setItem('qrCheckin', 'false');
                  }
                  if (json.isQrCheckOut) {
                    await AsyncStorage.setItem('qrCheckout', 'true');
                  } else {
                    await AsyncStorage.setItem('qrCheckout', 'false');
                  }
                  if (json.isCCTVFaceRegister) {
                    await AsyncStorage.setItem('isCCTVFaceRegister', 'true');
                  } else {
                    await AsyncStorage.setItem('isCCTVFaceRegister', 'false');
                  }
                  await AsyncStorage.setItem('idealCount', '0');
                  await addBoundaries(json.staffNumber, json.instituteId);
                  this.props.navigation.replace('MainNavigator');
                } catch (err) {
                  this.setState({
                    showAlert: false,
                    showAlert1: true,
                    //error_message: 'You might not be connected to Internet. Please check your internet connection.',
                    error_message:
                      'You might not be connected to Internet. Please check your internet connection.',
                  });
                }
              } else {
                this.setState({
                  showAlert1: true,
                  error_message: 'Logged in successfully',
                  showAlert: false,
                });
                try {
                  await AsyncStorage.setItem(
                    'app_status',
                    this.state.app_status,
                  );
                  await AsyncStorage.setItem('user_id', json.staffNumber);
                  await AsyncStorage.setItem('bearer_token', json.bearerToken);
                  await AsyncStorage.setItem(
                    'institute_id',
                    json.instituteId.toString(),
                  );
                  await AsyncStorage.setItem(
                    'org_id',
                    json.organisationId.toString(),
                  );
                  await AsyncStorage.setItem(
                    'driving_type',
                    json.travelPreference.toString(),
                  );
                  await AsyncStorage.setItem(
                    'start_time',
                    json.staffTravelStartTime.toString(),
                  );
                  await AsyncStorage.setItem(
                    'profile_pic',
                    json.profilePic ? json.profilePic : '',
                  );
                  await AsyncStorage.setItem(
                    'base64Data',
                    json.profilePicBase64 ? json.profilePicBase64 : '',
                  );
                  if (json.isTravelCheckInAvailable) {
                    await AsyncStorage.setItem('travel_check_in', 'yes');
                  } else {
                    await AsyncStorage.setItem('travel_check_in', 'no');
                  }
                  if (json.isQrCheckIn) {
                    await AsyncStorage.setItem('qrCheckin', 'true');
                  } else {
                    await AsyncStorage.setItem('qrCheckin', 'false');
                  }
                  if (json.isQrCheckOut) {
                    await AsyncStorage.setItem('qrCheckout', 'true');
                  } else {
                    await AsyncStorage.setItem('qrCheckout', 'false');
                  }
                  if (json.isFaceAuthenticationRequired) {
                    await AsyncStorage.setItem('isFaceRequired', 'true');
                  } else {
                    await AsyncStorage.setItem('isFaceRequired', 'false');
                  }
                  if (json.isCCTVFaceRegister) {
                    await AsyncStorage.setItem('isCCTVFaceRegister', 'true');
                  } else {
                    await AsyncStorage.setItem('isCCTVFaceRegister', 'false');
                  }
                  await AsyncStorage.setItem('idealCount', '0');
                  await addBoundaries(json.staffNumber, json.instituteId);
                  setTimeout(() => {
                    this.props.navigation.replace('MainNavigator');
                  }, 1500);
                } catch (err) {
                  this.setState({
                    showAlert: false,
                    showAlert1: true,
                    error_message:
                      'You might not be connected to Internet. Please check your internet connection.',
                  });
                }
              }
            } else {
              this.setState({
                showAlert1: true,
                error_message: 'Enter valid Mpin',
                showAlert: false,
              });
            }
          })
          .catch((error) => {
            console.log('error', error);
            this.setState({
              showAlert: false,
              showAlert1: true,
              //error_message: 'You might not be connected to Internet. Please check your internet connection.',
              error_message:
                'You might not be connected to Internet. Please check your internet connection.',
            });
          });
      } else {
        this.setState({
          showAlert1: true,
          error_message: 'Mpin cannot be empty',
          showAlert: false,
        });
      }
    } else {
      this.setState({
        showAlert1: true,
        error_message: 'Register Mpin on this device',
        showAlert: false,
      });
    }
  };
  showPassword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };
  registerMpin = () => {
    this.props.navigation.navigate('RegisterPin');
  };
  goToPrivacy = () => {
    this.props.navigation.navigate('Privacy', {backScreen: 'Login'});
  };

  goToSearchCountry = () => {
    this.props.navigation.navigate('SearchCountry');
  };

  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    console.log('[mobile_no]', this.state.mobileNumber);
    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={false}
          title="Attention"
          message={this.state.error_message}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false});
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="Login"
            showBack={false}
            backScreen=""
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            <View style={styles.firstContainer}>
              <Image
                source={require('../../assets/ic_login.png')}
                style={styles.loginImage}
                resizeMode="contain"
              />
            </View>
            <View style={styles.secondContainer}>
              <Text style={styles.otpHeader}>
                Enter your mobile number and MPIN to Login!
              </Text>
              <View>
                <Item regular style={styles.item}>
                  <TouchableWithoutFeedback onPress={this.goToSearchCountry}>
                    <Text style={styles.code}>{this.state.CountryCode}</Text>
                  </TouchableWithoutFeedback>
                  <Input
                    placeholder=""
                    keyboardType="number-pad"
                    style={styles.input}
                    value={this.state.mobileNumber}
                    maxLength={10}
                    onChangeText={(mobileNumber) => {
                      this.setState({mobileNumber: mobileNumber});
                    }}
                    onEndEditing={() =>
                      AsyncStorage.setItem('mobile', this.state.mobileNumber)
                    }
                  />
                  <Icon
                    active
                    name="phone"
                    type="FontAwesome"
                    style={{fontSize: 17, color: '#f05760'}}
                    onPress={this.showPassword}
                  />
                </Item>
              </View>

              <OTPInputView
                style={{width: wp('80'), height: 80}}
                pinCount={4}
                autoFocusOnLoad={false}
                secureTextEntry={true}
                codeInputFieldStyle={styles.inputFeilds}
                codeInputHighlightStyle={styles.inputFeildsFocus}
                onCodeFilled={(code) => {
                  this.handleLogin(code);
                }}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  width: wp('90'),
                  justifyContent: 'space-around',
                }}>
                <TouchableWithoutFeedback onPress={this.goToPrivacy}>
                  <Text style={styles.privacy}>Privacy Policy</Text>
                </TouchableWithoutFeedback>
                {this.state.mobileNumber !== null && (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.props.navigation.navigate('SetMpin', {
                        mobile: this.state.mobileNumber,
                      })
                    }>
                    <Text style={styles.privacy}>Forgot MPIN?</Text>
                  </TouchableWithoutFeedback>
                )}
                {this.state.mobileNumber !== null && (
                  <TouchableWithoutFeedback
                    onPress={() => {
                      this.setState({mobileNumber: null}, () => {
                        AsyncStorage.clear().then(() => {
                          AsyncStorage.setItem('locationPermissions', 'yes');
                          AsyncStorage.setItem(
                            'device_token',
                            this.state.device_token,
                          );
                          this.props.navigation.navigate('RegisterPin');
                        });
                      });
                    }}>
                    <Text style={styles.privacy}>New Register?</Text>
                  </TouchableWithoutFeedback>
                )}
              </View>
              <TouchableWithoutFeedback
                disabled={
                  this.state.mobileNumber !== null &&
                  this.state.mobileNumber?.length === 10
                }
                onPress={this.registerMpin}>
                <View
                  style={{
                    ...styles.buttonContainer,
                    backgroundColor:
                      this.state.mobileNumber === null ||
                      this.state.mobileNumber?.length !== 10
                        ? '#f05760'
                        : 'grey',
                  }}>
                  <Text style={styles.footerText}>Register Mpin</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  privacy: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#2460a5',
  },
  firstContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('30'),
    backgroundColor: '#f05760',
  },
  secondContainer: {
    alignItems: 'center',
    marginTop: hp('6'),
  },
  inputFeilds: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    color: '#000000',
    borderRadius: 10,
  },
  inputFeildsFocus: {
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#f05760',
    color: '#000000',
    borderRadius: 10,
  },
  otpHeader: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000',
    margin: '1%',
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#ffffff',
  },
  loginImage: {
    width: 200,
    height: 200,
  },
  buttonContainer: {
    width: wp('80'),
    // backgroundColor: '#f05760',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    borderRadius: 30,
    marginTop: hp('6'),
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    //borderBottomWidth:0.5,
    //backgroundColor: '#f1f1f1',
    paddingLeft: '5%',
    borderRadius: 10,
    height: hp('7'),
    width: wp('80'),
  },
  item: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'black',
    //backgroundColor: '#f1f1f1',
    // borderLeftWidth: 0,
    // borderRightWidth: 0,
    // borderTopWidth: 0,
    // borderBottomWidth: 0,
    height: hp('7'),
    width: wp('80'),
  },
  code: {
    fontFamily: 'Poppins-Regular',
    marginLeft: '3%',
  },
});
