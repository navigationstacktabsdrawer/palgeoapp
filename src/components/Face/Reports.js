import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  FlatList,
} from 'react-native';
import {Container, Toast, Card} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Loader from '../common/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import Const from '../common/Constants';
import SubHeader from '../common/SubHeader';
import AwesomeAlert from 'react-native-awesome-alerts';
import DatePicker from 'react-native-datepicker'
var moment = require('moment');
var current = new Date();
var p = current.setDate(current.getDate() - 7);
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(p),
      date1: new Date(),
      loader: true,
      StaffNo: '',
      showAlert: false,
      report: [],
    };
    this.setDate = this.setDate.bind(this);
    this.setDate1 = this.setDate1.bind(this);
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({loader: false});
    }, 700);
    AsyncStorage.getItem('user_id').then((user_id) => {
      this.setState({StaffNo: user_id}, () => {
        this.getStaffReport(user_id);
      });
    });
  }
  getStaffReport = async (user_id) => {
    const bearer_token = await AsyncStorage.getItem('bearer_token')
    var from_date = moment(this.state.date).format('YYYY-MM-DD');
    var to_date = moment(this.state.date1).format('YYYY-MM-DD');

    this.setState({showAlert: true});
    var url = `${Const}api/Staff/GetStaffEntryExitReport/${user_id}/${from_date}/${to_date}`;
    fetch(url, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        if (json.length > 0) {
          //console.log('reports', json);
          this.setState({report: json, showAlert: false});
        } else {
          Toast.show({
            text: 'No entry exit report found',
            duration: 3500,
            type: 'warning',
            textStyle: {
              fontFamily: 'Poppins-Regular',
              color: '#ffffff',
              textAlign: 'center',
              fontSize: 14,
            },
          });
          this.setState({report: [], showAlert: false});
        }
      })
      .catch((error) => {
        this.setState({showAlert: false});
        console.error(error);
      });
  };
  openDrawer = () => {
    this.drawer._root.open();
  };
  setDate(newDate) {
    this.setState({date: newDate});
  }
  setDate1(newDate) {
    this.setState({date1: newDate});
  }
  goToPaperView = () => {
    this.props.navigation.navigate('QuestionPaperView');
  };
  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        <Container>
          <SubHeader
            title="Reports"
            showBack={true}
            backScreen="Home"
            navigation={this.props.navigation}
          />
          <View
            style={{
              alignItems: 'center',
              flexDirection: 'row',
              marginTop: '4%',
              marginLeft: '3%',
              marginRight: '2%',
            }}>
            <View style={{width: wp('35')}}>
              <View style={styles.labelContainer}>
                <Text style={styles.label}>From Date</Text>
              </View>
              <View style={styles.dateContainer}>
                {/* <DatePicker
                  locale={'en'}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={'fade'}
                  formatChosenDate={(date) =>
                    moment(date).format('DD/MM/YYYY').toString()
                  }
                  placeHolderText={moment(this.state.date)
                    .format('DD/MM/YYYY')
                    .toString()}
                  textStyle={{color: '#959394'}}
                  placeHolderTextStyle={{color: '#959394'}}
                  onDateChange={this.setDate}
                  disabled={false}
                  format="YYYY-MM-DD"
                /> */}
                <DatePicker
                showIcon = {false}
        style={{width: wp('40')}}
        date={this.state.date}
        mode="date"
        placeholder= {moment(this.state.date)
          .format('DD/MM/YYYY')
          .toString()}
        format="YYYY-MM-DD"
        minDate="2016-05-01"
        maxDate={moment()
          .format('YYYY-MM-DD')
          .toString()}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateInput: {
            //marginLeft: 36
            borderColor: 'white',
            marginTop: 10,
            marginRight: 36,
            padding:0
           
          },
          dateText: {
            color: '#959394',
            textAlign: 'center',
            fontFamily: 'Poppins-Regular'
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date: date})}}
      />
              </View>
            </View>
            <View style={{width: wp('35'), marginLeft: wp('1')}}>
              <View style={styles.labelContainer}>
                <Text style={styles.label}>To Date</Text>
              </View>
              <View style={styles.dateContainer}>
                {/* <DatePicker
                  locale={'en'}
                  formatChosenDate={(date) =>
                    moment(date).format('DD/MM/YYYY').toString()
                  }
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={'fade'}
                  placeHolderText={moment(this.state.date1)
                    .format('DD/MM/YYYY')
                    .toString()}
                  textStyle={{color: '#959394'}}
                  placeHolderTextStyle={{color: '#959394'}}
                  onDateChange={this.setDate1}
                  disabled={false}
                /> */}
                <DatePicker
                showIcon={false}
        style={{width: wp('40')}}
        date={this.state.date1}
        mode="date"
        placeholder= {moment(this.state.date1)
          .format('DD/MM/YYYY')
          .toString()}
        format="YYYY-MM-DD"
        minDate="2016-05-01"
        maxDate={moment()
          .format('YYYY-MM-DD')
          .toString()}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateInput: {
            marginRight: 36,
            marginTop:10,
            borderColor: 'white'
          },
          dateText: {
            color: '#959394',
            textAlign: 'center',
            fontFamily: 'Poppins-Regular'
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {this.setState({date1: date})}}
      />
              </View>
            </View>
            <View
              style={{
                width: wp('20'),
                marginTop: hp('3.5'),
                marginLeft: wp('2'),
              }}>
              <TouchableWithoutFeedback
                onPress={() => this.getStaffReport(this.state.StaffNo)}>
                <View style={styles.buttonContainer}>
                  <Text style={styles.buttonText1}>Go</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <FlatList
            data={this.state.report}
            renderItem={({item, index}) => {
              let time_in = '';
              let time_out = '';

              var gmtInTime = moment.utc(item.time_in, 'YYYY-MM-DD HH:mm:ss');
              time_in = gmtInTime.local().format('h:mm A');
              if (item.time_Out) {
                var gmtOutTime = moment.utc(
                  item.time_Out,
                  'YYYY-MM-DD HH:mm:ss',
                );
                time_out = gmtOutTime.local().format('h:mm A');
              }
              return (
                <Card style={styles.walletContainer} key={item.id}>
                  <View style={styles.row}>
                    <View style={{width: wp('50'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Sno</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>{index + 1}</Text>
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('50'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Date</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>
                        {moment(item.inDate).format('DD-MM-YYYY')}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('50'), justifyContent: 'center'}}>
                      <Text style={styles.header}>In</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>{time_in}</Text>
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('50'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Out</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>
                        {time_out === '' ? '----' : time_out}
                        {/* {time_out} */}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('50'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Location</Text>
                    </View>
                    <View style={{width: wp('50')}}>
                      <Text style={styles.headerVal}>{item.location}</Text>
                    </View>
                  </View>
                  <View style={styles.row}>
                    <View style={{width: wp('50'), justifyContent: 'center'}}>
                      <Text style={styles.header}>Status</Text>
                    </View>
                    <View style={{width: wp('30')}}>
                      <Text style={styles.buttonText}>{item.att}</Text>
                    </View>
                  </View>
                </Card>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
          />
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 14,
    color: '#000000',
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#f1f1f1',
    paddingLeft: '5%',
    borderRadius: 10,
    height: hp('7'),
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#f1f1f1',
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    height: hp('7'),
  },
  dateContainer: {
    borderWidth: 1.5,
    borderColor: '#f1f1f1',
    height: hp('6.7'),
    borderRadius: 8,
    backgroundColor: '#ffffff',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: '8%',
    backgroundColor: '#f05760',
    height: hp('6.2'),
    borderRadius: 5,
  },
  buttonText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
    color: '#f05760',
  },
  buttonText1: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
    color: '#ffffff',
  },
  walletContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    elevation: 5,
    width: wp('94'),
    paddingTop: '3%',
    paddingLeft: '4%',
    paddingRight: '4%',
    paddingBottom: '3%',
  },
  header: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#67747d',
    margin: '1%',
  },
  headerVal: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#000000',
    margin: '1%',
  },
  headerVal1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#000000',
    paddingLeft: '3%',
  },
  row: {
    flexDirection: 'row',
    paddingLeft: '4%',
    paddingTop: '1%',
    paddingBottom: '1%',
  },
  divider: {
    borderWidth: 1,
    borderColor: '#e2e2e2',
    margin: '2%',
  },
  divider1: {
    borderWidth: 1,
    borderColor: '#f3f3f3',
    margin: '2%',
  },
  statusContainer: {
    backgroundColor: '#f86c6b',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '5%',
    height: hp('4.5'),
    borderRadius: 5,
    width: wp('23'),
  },
  statusContainer1: {
    backgroundColor: '#63c2de',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '5%',
    height: hp('4.5'),
    borderRadius: 5,
    width: wp('23'),
  },
  actionContainer: {
    backgroundColor: '#63c2de',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '5%',
    height: hp('4.5'),
    borderRadius: 5,
    width: wp('25'),
  },
  selectText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
