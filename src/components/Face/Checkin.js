import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Button,
  AppState,
  Alert,
  NativeEventEmitter,
  NativeModules,
  Modal,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Card, Content} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import Geolocation from 'react-native-geolocation-service';
import Const from '../common/Constants';
import AwesomeAlert from 'react-native-awesome-alerts';
import AsyncStorage from '@react-native-community/async-storage';
import ModalForAccuracy from '../common/Modal';
import CameraComp from '../common/CameraComp';
import RNLocation from 'react-native-location';

import BleManager from 'react-native-ble-manager';
import axios from 'axios';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import KeepAwake from 'react-native-keep-awake';
import ImageResizer from 'react-native-image-resizer';

import {RNCamera} from 'react-native-camera';
import Geocoder from 'react-native-geocoding';
import Location from '../common/Location';
import {GOOGLE_MAPS_APIKEY} from '../../utils/configs/Constants';

var geolib = require('geolib');

export default class Checkin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      coordinates: {
        latitude: '',
        longitude: '',
        accuracy: '',
      },
      showAlert: false,
      StaffNo: '',
      bearer_token: '',
      institute_id: '',
      showAlert1: false,
      error_message: '',
      travel_check_in: this.props.route?.params?.travel || 'no',
      qrCheckin: this.props.route?.params?.qrCheckin || 'false',
      uri: '',
      travelCheckIn: false,
      selected: false,
      notInFence: false,
      modal: false,
      show: this.props.route?.params?.show || false,
      success: false,
      //show: false,
      showTimer: false,
      count: 0,
      subCount: 0,
      btnCount: 1,
      pressed: false,
      base64Data: null,
      isScanning: false,
      beaconID: '',
      beaconDetails: {},
      show1: false,
      show2: false,
      travelCheckInAddress: '',
      showAlert2: false,
    };
  }

  startScan = () => {
    if (!this.state.isScanning) {
      BleManager.scan([], 5, true)
        .then((results) => {
          console.log('Scanning...');
          this.setState({isScanning: true});
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };

  openCamera1 = async (camera) => {
    const options1 = {quality: 1, base64: false};
    const body = new FormData();

    try {
      const image1 = await camera.takePictureAsync(options1);
      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      )
        .then(async (response) => {
          console.log('size of image ===>', response.size);
          this.setState({showAlert: true});
          if (response.uri) {
            body.append('StaffNo', this.state.StaffNo);
            body.append('InstituteId', this.state.institute_id);
            body.append('File', {
              uri: response.uri,
              name: 'photo.png',
              type: 'image/png',
            });
            body.append('AccessLocationId', Number(this.state.data.locationId));
            body.append('QrCodeOwnerStaffCode', this.state.data.StaffNo);
            const response = await axios({
              method: 'POST',
              url: Const + 'api/staff/QrCheckIn',
              data: body,
              headers: {'Content-Type': 'multipart/form-data'},
            });
            this.setState({show1: false, showAlert: false});
            console.log('JSON ==>', response.data);
            const json = response.data;
            if (!json.response.status) {
              return alert(response.data.response.message);
            }
            //alert(response.data.response.message)
            const geofence = json.geofencingClients.find(
              (item) => item.distance === 0,
            );
            console.log('Checked_in_geofence ==>', geofence);

            AsyncStorage.setItem('checked_out', 'no');
            if (this.state.travelCheckIn) {
              AsyncStorage.setItem('current_travel_checkin', 'running');
            }

            if (Platform.OS === 'android') {
              this.setState({
                showAlert: false,
                showAlert1: true,
                error_message: json.response.message,
                count: 0,
              });
            } else {
              alert(json.response.message);
              this.setState({showAlert: false, count: 0});
            }
            AsyncStorage.setItem('geo_id', geofence.id.toString());
            if (geofence.type == 'Circle') {
              AsyncStorage.setItem(
                'coordinates',
                JSON.stringify(geofence.coordinates),
              );
              AsyncStorage.setItem('radius', geofence.radius.toString());
              AsyncStorage.setItem('type', geofence.type.toString());
              AsyncStorage.setItem(
                'checkin_time',
                geofence.CheckInTime ? geofence.CheckInTime : '',
              );
            } else {
              AsyncStorage.setItem(
                'coordinates',
                JSON.stringify(geofence.coordinates),
              );
              AsyncStorage.setItem(
                'checkin_time',
                geofence.CheckInTime ? geofence.CheckInTime : '',
              );
              AsyncStorage.setItem('type', geofence.type.toString());
            }
            // ReactNativeForegroundService.start({
            //   id: 144,
            //   title: 'Background Location Tracking',
            //   message: 'You are being tracked!',
            // });

            KeepAwake.activate();
            return setTimeout(() => {
              this.props.navigation.navigate('StaffTasks', {disable: true});
            }, 4000);
          }
          // response.uri is the URI of the new image that can now be displayed, uploaded...
          // response.path is the path of the new image
          // response.name is the name of the new image with the extension
          // response.size is the size of the new image
        })
        .catch((err) => {
          console.log('error in resizing', err);
          // Oops, something went wrong. Check that the filename is correct and
          // inspect err to get more details.
        });
    } catch (e) {
      console.log(e);
      alert(e.toString());
      this.setState({show1: false, showAlert: false});
    }
  };

  handleStopScan = () => {
    console.log('Scan is stopped');
    this.setState({isScanning: false});
  };

  handleDiscoverPeripheral = async (peripheral) => {
    console.log('peripheral', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'unknown';
    }

    if (this.state.beaconDetails.uniqueId === peripheral.id) {
      this.setState({isScanning: false});
      alert(
        `Got beacon ${
          peripheral.id
        } with signal strength ${peripheral.rssi.toString()}`,
      );
      this.setState({beaconID: peripheral.id, beaconDetails: peripheral});
    }

    // alert(
    //   `Got beacon ${
    //     peripheral.id
    //   } with signal strength ${peripheral.rssi.toString()}`,
    // );
    // this.setState({beaconID: peripheral.id, beaconDetails: peripheral});
  };

  async componentDidMount() {
    Geocoder.init(GOOGLE_MAPS_APIKEY);
    const user_id = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const checkBeacon = await this.checkBeacon(user_id, institute_id);
    if (checkBeacon) {
      this.enableBluetooth();
    }
    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);

    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const travel_check_in = await AsyncStorage.getItem('travel_check_in');
    //console.log('ttt', travel_check_in);
    this.setState({
      bearer_token,
      StaffNo: user_id,
      institute_id,
      travel_check_in,
    });
  }

  checkBeacon = async (user_id, institute_id) => {
    //this.setState({showAlert: false});
    return fetch(
      Const + 'api/Staff/information/' + institute_id + '/' + user_id,
      {
        method: 'GET',
        headers: {
          Accept: 'text/plain',
          'content-type': 'application/json-patch+json',
        },
      },
    )
      .then((response) => response.json())
      .then((json) => {
        if (json.status) {
          const beacon = json.assignedLocations.find(
            (item) => item.uniqueId !== null && item.uniqueId !== '', //this is accepting travel check in also
          );
          if (beacon) {
            this.setState({beaconDetails: beacon});
            return true;
          }
          return false;
        }
        return false;
      })
      .catch((error) => {
        this.setState({showAlert: false});
        return false;
      });
  };

  componentWillUnmount() {
    clearInterval(this.interval);
    //clearInterval(this.interval2);
    Geolocation.clearWatch(this.watchID);
    bleManagerEmitter.removeListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.removeListener('BleManagerStopScan', this.handleStopScan);
  }

  enableBluetooth = () => {
    BleManager.enableBluetooth()
      .then(() => {
        // Success code
        console.log('The bluetooth is already enabled or the user confirm');
        //this.startConnect();
        BleManager.start({showAlert: false});
        this.startScan();
      })
      .catch((error) => {
        //Failure code
        Alert.alert('Attention', 'Please turn on your bluetooth', [
          {
            text: 'OK',
            onPress: () => {
              BleManager.enableBluetooth();
            },
          },
        ]);
        return;
      });
  };

  openCamera = async (camera) => {
    //this.setState({pressed: true});

    //const options = {maxDuration: 8, maxFileSize: 2 * 1024 * 1024};
    const options1 = {quality: 1, base64: false, width: 800};
    const image1 = await camera.takePictureAsync(options1);
    //const image = await camera.recordAsync(options);
    //const base64Image = await AsyncStorage.getItem('base64Image');
    console.log(image1);
    //this.setState({show: false});

    if (image1.uri) {
      //this.startScan();
      this.setState({show: false});

      this.setState({
        showAlert2: true,
        //error_message: 'Fetching location...',
        showAlert: false,
        showTimer: true,
      });

      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      )
        .then((response) => {
          console.log('size of image ===>', response.size);
          this.getCurrentLocationWithAccuracy(response.uri);
          // response.uri is the URI of the new image that can now be displayed, uploaded...
          // response.path is the path of the new image
          // response.name is the name of the new image with the extension
          // response.size is the size of the new image
        })
        .catch((err) => {
          console.log('error in resizing', err);
          // Oops, something went wrong. Check that the filename is correct and
          // inspect err to get more details.
        });
      //this.getCurrentLocationWithAccuracy(image1.uri);
    } else {
      this.setState({
        showAlert1: true,
        error_message: 'Please re authenticate your face',
        showAlert: false,
        showTimer: false,
      });
    }
  };

  getCurrentLocationWithAccuracy = (uri) => {
    // this.interval = setInterval(() => {
    //   this.setState((prev) => ({count: prev.count + 1}));
    // }, 1000);
    let coordinatesTemp = {latitude: '', longitude: '', accuracy: ''};
    this.main(uri);
    // setTimeout(async () => {
    //   Geolocation.clearWatch(this.watchID);
    //   if (this.state.coordinates.accuracy > 10) {
    //     //console.log('Entry_Time', new Date());
    //     this.setState({showAlert2: false, showAlert: true});
    //     await this.CheckIn(uri, this.state.coordinates);
    //     return;
    //   } else {
    //     if (this.state.coordinates.accuracy === '') {
    //       this.setState({showAlert2: false, showAlert: true});
    //       //console.log('accuracy', this.state.coordinates.accuracy);
    //       Geolocation.getCurrentPosition(
    //         (pos) => this.CheckIn(uri, pos.coords),
    //         () => {},
    //         {enableHighAccuracy: true, maximumAge: 0},
    //       );
    //     }
    //     //console.log('Accuracy achieved');
    //   }
    // }, 10000);
    this.watchID = Geolocation.watchPosition(
      async (position) => {
        //this.setState((prev) => ({count: prev.count + 1})),
        // console.log('progress_location', position);
        this.setState({
          coordinates: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            accuracy: position.coords.accuracy,
          },
        });
        coordinatesTemp.latitude = position.coords.latitude;
        coordinatesTemp.longitude = position.coords.longitude;
        coordinatesTemp.accuracy = position.coords.accuracy;
        if (position.coords.accuracy <= 10) {
          Geolocation.clearWatch(this.watchID);
          await this.CheckIn(uri, coordinatesTemp);
          return;
        }
      },
      (e) => {
        console.log(e.message);
      },
      {
        enableHighAccuracy: true,
        distanceFilter: 0,
        interval: 1000,
        fastestInterval: 1000,
      },
    );
  };

  wait = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms));

  main = async (uri) => {
    await this.wait(10000);
    Geolocation.clearWatch(this.watchId);
    if (this.state.coordinates.accuracy > 10) {
      console.log('accuracy_0', this.state.coordinates.accuracy);
      this.setState({showAlert2: false, showAlert: true});
      await this.CheckIn(uri, this.state.coordinates);
      return;
    } else {
      if (this.state.coordinates.accuracy === '') {
        console.log('accuracy', this.state.coordinates.accuracy);
        this.setState({showAlert2: false, showAlert: true});
        Geolocation.getCurrentPosition(
          (pos) => this.CheckIn(uri, pos.coords),
          () => {},
          {enableHighAccuracy: true, maximumAge: 0, distanceFilter: 0},
        );
      }
      console.log('Accuracy achieved');
    }
  };

  checkDistance = (locations) => {
    let check = true;
    if (locations.length === 0) {
      check = false;
    } else {
      locations.forEach((item) => {
        if (parseInt(item.distance) > 40) {
          check = false;
        } else {
          check = true;
        }
      });
    }
    return check;
  };

  CheckIn = async (uri, coordinates) => {
    Geocoder.from(coordinates.latitude, coordinates.longitude)
      .then((json) =>
        this.setState(
          {travelCheckInAddress: json.results[0].formatted_address},
          async () => {
            if (this.state.subCount > 0) {
              // console.log('==> I ran again', this.state.subCount);
            }

            let uploadData = new FormData();
            //console.log('Process time', new Date());
            uploadData.append('file', {
              uri: uri,
              name: 'photo.png',
              type: 'image/png',
            });
            //uploadData.append('file', uri);
            uploadData.append('StaffNo', this.state.StaffNo);
            uploadData.append('Latitude', Number(coordinates.latitude));
            uploadData.append('Longitude', Number(coordinates.longitude));
            uploadData.append('InstituteId', this.state.institute_id);
            uploadData.append('Accuracy', Number(coordinates.accuracy));
            uploadData.append('IsTravelCheckIn', this.state.travelCheckIn);
            uploadData.append(
              'TravelCheckInAddress',
              this.state.travelCheckIn ? this.state.travelCheckInAddress : '',
            );
            console.log('uploadData', uploadData);
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            setTimeout(() => {
              source.cancel('Request cancel after timeout');
            }, 30000);
            try {
              const response = await axios({
                method: 'post',
                url: Const + 'api/Staff/CheckInWithFormFile',
                data: uploadData,
                headers: {'Content-Type': 'multipart/form-data'},
                timeout: 30000,

                cancelToken: source.token,
              });
              console.log('End time', new Date());
              const json = response.data;
              this.setState({showAlert2: false, showAlert: true});
              clearInterval(this.interval);
              Geolocation.clearWatch(this.watchID);
              console.log('JSON', json);
              const totalBeacons = json.geofencingClients.filter(
                (item, index) => item.uniqueId,
              );
              console.log('total_beacons', totalBeacons);
              const beacon = json.geofencingClients.find(
                (item, index) =>
                  item.uniqueId === this.state.beaconID &&
                  Math.abs(this.state.beaconDetails.rssi) <= 65,
              );

              if (beacon) {
                AsyncStorage.setItem('beacon', JSON.stringify(beacon));
              }
              console.log(
                'check',
                beacon,
                'totalBeacons_count',
                totalBeacons.length,
              );
              if (json.response.status) {
                this.setState({success: true});
                BluetoothStateManager.disable().then((result) => {
                  // do something...
                  console.log('blettoth', result);
                });
                if (this.state.travelCheckIn) {
                  AsyncStorage.setItem(
                    'currentTravelLocation',
                    JSON.stringify(coordinates),
                  );
                  AsyncStorage.setItem('current_travel_checkin', 'running');
                  AsyncStorage.setItem('geo_location', 'Travel Check In');
                }
                if (!this.state.travelCheckIn) {
                  const geofence = json.geofencingClients.find(
                    (item) => item.distance === 0,
                  );
                  console.log('Checked_in_geofence ==>', geofence);
                  AsyncStorage.setItem('geo_id', geofence.id.toString());
                  AsyncStorage.setItem('geo_location', geofence.accessLocation);
                  if (geofence.type == 'Circle') {
                    AsyncStorage.setItem(
                      'coordinates',
                      JSON.stringify(geofence.coordinates),
                    );
                    AsyncStorage.setItem('radius', geofence.radius.toString());
                    AsyncStorage.setItem('type', geofence.type.toString());
                    AsyncStorage.setItem(
                      'checkin_time',
                      geofence.CheckInTime ? geofence.CheckInTime : '',
                    );
                  } else {
                    AsyncStorage.setItem(
                      'coordinates',
                      JSON.stringify(geofence.coordinates),
                    );
                    AsyncStorage.setItem(
                      'checkin_time',
                      geofence.CheckInTime ? geofence.CheckInTime : '',
                    );
                    AsyncStorage.setItem('type', geofence.type.toString());
                  }
                }
                AsyncStorage.setItem('checked_out', 'no');
                AsyncStorage.setItem('isInside', 'yes');

                this.setState({
                  showAlert: false,
                  showAlert1: true,
                  error_message: json.response.message,
                });

                KeepAwake.activate();

                // return setTimeout(() => {
                //   this.props.navigation.navigate('StaffTasks', {disable: true});
                // }, 5200);
              } else if (!json.response.status) {
                //this.enableBluetooth()
                this.setState({success: false});
                let isWithinBoundary = this.checkDistance(
                  json.geofencingClients,
                );
                console.log('boundary', isWithinBoundary);
                if (json.geofencingClients.length === 0) {
                  this.setState({
                    showAlert: false,
                    showAlert1: true,
                    error_message:
                      'No assigned locations. Please contact your administrator for further details.',
                  });
                  return;
                }
                if (totalBeacons.length > 0) {
                  if (
                    !beacon &&
                    json.geofencingClients.length > 0 &&
                    !this.state.travelCheckIn
                  ) {
                    this.setState({
                      showAlert1: true,
                      showAlert: false,
                      error_message: `Either you are not in your assigned location or you are not close enough to the assigned beacon. Please move closer to the beacon location for successful check-in.`,
                    });
                    return setTimeout(
                      () => this.props.navigation.navigate('Home'),
                      5000,
                    );
                  }
                  if (
                    beacon &&
                    json.geofencingClients.length > 0 &&
                    !this.state.travelCheckIn
                  ) {
                    let uploadData1 = new FormData();
                    console.log('Process time', new Date());
                    uploadData1.append('file', {
                      uri: uri,
                      name: 'photo.png',
                      type: 'image/png',
                    });
                    //uploadData1.append('file', uri);
                    uploadData1.append('StaffNo', this.state.StaffNo);
                    uploadData1.append(
                      'Latitude',
                      Number(coordinates.latitude),
                    );
                    uploadData1.append(
                      'Longitude',
                      Number(coordinates.longitude),
                    );
                    uploadData1.append('InstituteId', this.state.institute_id);
                    uploadData1.append(
                      'Accuracy',
                      Number(coordinates.accuracy),
                    );
                    uploadData1.append(
                      'IsTravelCheckIn',
                      this.state.travelCheckIn,
                    );
                    uploadData1.append('IsBeaconVerified', true);
                    uploadData1.append('AccessLocationId', beacon.id);
                    fetch(Const + 'api/Staff/CheckInWithFormFile', {
                      method: 'POST',
                      body: uploadData1,
                    })
                      .then((response) => response.json())
                      .then((json) => {
                        clearInterval(this.interval);
                        this.setState({success: true});
                        BluetoothStateManager.disable().then((result) => {
                          // do something...
                          console.log('blettoth', result);
                        });
                        Geolocation.clearWatch(this.watchID);
                        console.log('beacon_json', json);
                        AsyncStorage.setItem('checked_out', 'no');
                        AsyncStorage.setItem('isInside', 'yes');
                        if (this.state.travelCheckIn) {
                          AsyncStorage.setItem(
                            'current_travel_checkin',
                            'running',
                          );
                        }
                        this.setState({
                          showAlert: false,
                          showAlert1: true,
                          error_message: json.response.message,
                        });
                        // try {
                        if (beacon.type == 'Circle') {
                          AsyncStorage.setItem(
                            'coordinates',
                            JSON.stringify(beacon.coordinates),
                          );
                          AsyncStorage.setItem(
                            'radius',
                            beacon.radius.toString(),
                          );
                          AsyncStorage.setItem('type', beacon.type.toString());
                          AsyncStorage.setItem(
                            'checkin_time',
                            beacon.CheckInTime ? beacon.CheckInTime : '',
                          );
                        } else {
                          AsyncStorage.setItem(
                            'coordinates',
                            JSON.stringify(beacon.coordinates),
                          );
                          AsyncStorage.setItem(
                            'checkin_time',
                            beacon.CheckInTime ? beacon.CheckInTime : '',
                          );
                          AsyncStorage.setItem('type', beacon.type.toString());
                        }

                        return setTimeout(() => {
                          this.props.navigation.navigate('StaffTasks', {
                            disable: true,
                          });
                        }, 5200);
                        // } catch (e) {
                        //   console.log(e);
                        // }
                      })
                      .catch((error) => {
                        console.log('beacon_error', error);
                        this.setState({success: false});
                      });
                    return;
                  }
                }
                if (
                  this.state.coordinates.accuracy > 10 &&
                  json.geofencingClients.length > 0 &&
                  !this.state.travelCheckIn &&
                  isWithinBoundary
                ) {
                  this.setState({
                    showAlert: false,
                    modal: true,
                    count: 0,
                  });
                } else if (
                  this.state.coordinates.accuracy <= 10 &&
                  json.geofencingClients.length > 0 &&
                  isWithinBoundary &&
                  !this.state.travelCheckIn
                ) {
                  Geolocation.getCurrentPosition(
                    (position) => {
                      let coordinates = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        accuracy: position.coords.accuracy,
                      };
                      this.setState({count: 0});
                      if (this.state.subCount < 2) {
                        this.CheckIn(uri, coordinates);
                        console.log('==> I ran');
                      } else {
                        // this.setState({
                        //   showAlert: false,
                        //   showAlert1: true,
                        //   error_message:
                        //     'Please check-in from assigned location or retry again.',
                        // });
                        alert(
                          'Please check-in from assigned location or retry again.',
                        );
                        setTimeout(
                          () => this.props.navigation.navigate('Home'),
                          1200,
                        );
                      }
                      setTimeout(
                        () =>
                          this.setState({subCount: this.state.subCount + 1}),
                        1200,
                      );
                    },
                    (e) => {
                      console.log(e);
                    },
                    {
                      enableHighAccuracy: true,
                      maximumAge: 0,
                    },
                  );
                } else {
                  this.setState({
                    showAlert1: true,
                    error_message: json.response.message,
                    showAlert: false,
                    count: 0,
                    success: false,
                  });
                }
              }
            } catch (error) {
              clearInterval(this.interval);
              Geolocation.clearWatch(this.watchID);
              if (axios.isCancel(error)) {
                console.log('Request canceled', error.message);
                this.setState({
                  showAlert: false,
                  showAlert2: false,
                  showAlert1: true,
                  error_message:
                    'Request taking too long to respond. Refresh internet connection and retry.',
                  count: 0,
                  success: false,
                });
                return;
              }
              console.log(error);
              this.setState({
                showAlert: false,
                showAlert2: false,
                showAlert1: true,
                error_message:
                  'Sorry! Unhandled exception occured while checking-in.',

                count: 0,
                success: false,
              });
            }
          },
        ),
      )
      .catch((e) => {
        console.log(e);
        alert('Unable to find location');
      });
    // console.log('uri==>', uri, coordinates, this.state.StaffNo);
  };

  CheckinWithBarCode = async ({data}) => {
    console.log('data', data);
    let parsedDate = JSON.parse(data);
    let now1 = new Date().valueOf();
    let then1 = new Date(parsedDate.timeStamp).valueOf();
    const difference = (now1 - then1) / 1000;
    console.log({
      now: now1,
      then1,
      difference,
    });
    if (difference > 20) {
      this.setState({show2: false}, () => {
        return alert('Time exceeded for check in');
      });
      return;
    }

    this.setState({
      data: parsedDate,
      show2: false,
      show1: true,
    });
  };

  goToHome = () => {
    this.props.navigation.navigate('Home');
  };

  goToStaffTasks = () => {
    this.props.navigation.navigate('StaffTasks', {disable: true});
  };

  launchTipsModal = () => {
    this.setState({modal: true, notInFence: false});
  };

  goTo = () => {
    if (this.state.success) {
      this.props.navigation.navigate('StaffTasks', {disable: true});
    } else {
      this.props.navigation.navigate('Home');
    }
  };

  render() {
    if (this.state.loader) {
      return <Loader />;
    }

    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />

        {this.state.showAlert2 && (
          <Location
            accuracy={Number(this.state.coordinates.accuracy).toFixed(2)}
            latitude={Number(this.state.coordinates.latitude)}
            longitude={Number(this.state.coordinates.longitude)}
            isVisible={this.state.showAlert2}
            text={'Please wait... Trying to check-in...'}
            //count={this.state.count}
          />
        )}

        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={
            this.state.error_message === 'Fetching location...' ? true : false
          }
          title={
            this.state.error_message === 'Fetching location...'
              ? this.state.count.toString()
              : 'Attention'
          }
          //title={'Attention'}
          message={this.state.error_message}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={
            this.state.error_message === 'Fetching location...' ? false : true
          }
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false, count: 0}, () => this.goTo());
            if (
              this.state.error_message ===
              'Either you are not in your assigned location or you are not close enough to the assigned beacon. Please move closer to the beacon location for successful check-in.'
            ) {
              return this.props.navigation.navigate('Home');
            }
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="Check In"
            showBack={true}
            backScreen="Home"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            {/* {this.state.travel_check_in == 'yes' ? ( */}
            <View style={{marginTop: 20, marginLeft: 15, marginRight: 15}}>
              <Text style={styles.heading}>
                Please select your Check IN type
              </Text>
              <View style={styles.center}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    this.setState({travelCheckIn: false, show: true})
                  }>
                  <Card style={styles.mainCardStyle}>
                    <Text style={styles.checkinText}>Check IN</Text>
                  </Card>
                </TouchableWithoutFeedback>
                {this.state.travel_check_in == 'yes' && (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({travelCheckIn: true, show: true})
                    }>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>Travel Check IN</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
                {this.state.qrCheckin === 'true' && (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({qrCodeScanner: true, show2: true})
                    }>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>QR Code Check IN</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
              </View>
            </View>

            {/* ) :
            <View style={{marginTop: 20, marginLeft: 15, marginRight: 15}}>
                <Text style={styles.heading}>
                  Please select your Check IN type
                </Text>
            <View style={styles.center}>
            <TouchableWithoutFeedback
              onPress={() =>
                this.setState({travelCheckIn: false, show: true})
              }>
              <Card style={styles.mainCardStyle}>
                <Text style={styles.checkinText}>Check IN</Text>
              </Card>
            </TouchableWithoutFeedback>
            
            <TouchableWithoutFeedback
              onPress={() =>
                this.setState({qrCodeScanner: true, show2: true})
              }>
              <Card style={styles.mainCardStyle}>
                <Text style={styles.checkinText}>QR Code Check IN</Text>
              </Card>
            </TouchableWithoutFeedback>
          </View>
          </View>
          } */}

            {this.state.show && (
              <CameraComp
                visible={this.state.show}
                onClose={() => this.setState({show: false})}
                onPress={(camera) => this.openCamera(camera)}
                btnText={
                  !this.state.pressed
                    ? 'check-in'
                    : this.state.btnCount.toString()
                }
                onRecordingStart={(event) => {
                  //console.log('event', event);
                  this.interval2 = setInterval(() => {
                    this.setState((prev) => ({btnCount: prev.btnCount + 1}));
                  }, 1000);
                }}
                onRecordingEnd={() => {
                  clearInterval(this.interval2);
                  this.setState({
                    show: false,
                    btnCount: 0,
                    pressed: false,
                    showAlert: true,
                  });
                }}
              />
            )}
            {this.state.show2 && (
              <CameraComp
                visible={this.state.show2}
                onClose={() => this.setState({show2: false})}
                onPress={(camera) => {}}
                btnText={'QR Code Scanner'}
                onBarCodeRead={this.CheckinWithBarCode}
                //type = {RNCamera.Constants.Type.back}
              />
            )}

            {this.state.show1 && (
              <CameraComp
                visible={this.state.show1}
                onClose={() => this.setState({show1: false})}
                onPress={(camera) => this.openCamera1(camera)}
                btnText={'check-in'}
              />
            )}

            {this.state.modal && (
              <ModalForAccuracy
                visible={this.state.modal}
                onClose={() => this.setState({modal: false})}
                onPress={() => this.props.navigation.goBack()}
                accuracy={Math.round(
                  Number(this.state.coordinates.accuracy),
                ).toString()}
                checkin
              />
            )}

            {/* <Button title="Get ccurrent location" onPress={this.CheckIn} /> */}
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  cardContainer: {
    marginTop: '10%',
    marginLeft: '6%',
    marginRight: '6%',
    justifyContent: 'center',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#c9c3c5',
  },
  label1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#c9c3c5',
    paddingLeft: wp('3'),
  },
  labelContainer: {
    margin: '1.5%',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#f1f1f1',
    paddingLeft: '5%',
    borderRadius: 10,
    height: hp('7'),
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#f1f1f1',
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    height: hp('7'),
  },
  card: {
    borderRadius: 10,
    elevation: 4,
    paddingTop: '3%',
    paddingBottom: '3%',
    height: hp('65'),
  },
  preview: {
    flex: 1,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('35'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonContainer1: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('40'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#ffffff',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#ffffff',
    paddingRight: '3%',
  },
  btImage: {
    width: 50,
    height: 39,
    borderRadius: 20,
  },
  userImage: {
    width: 120,
    height: 120,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  note: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
    marginBottom: 10,
  },
  mainCardStyle: {
    borderRadius: 10,
    elevation: 4,
    padding: 20,
    width: wp('85'),
  },
  checkinText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  heading: {
    fontFamily: 'Poppins-SemiBold',
    margin: 12,
  },
});
