import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Alert,
  NativeModules,
  NativeEventEmitter,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Container, Card, Content} from 'native-base';
import SubHeader from '../common/SubHeader';
import Loader from '../common/Loader';
import Geolocation from 'react-native-geolocation-service';
import Const from '../common/Constants';
import AwesomeAlert from 'react-native-awesome-alerts';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import ModalForAccuracy from '../common/Modal';
import CameraComp from '../common/CameraComp';
import BleManager from 'react-native-ble-manager';
import axios from 'axios';
import Boundary, {Events} from 'react-native-boundary';
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import KeepAwake from 'react-native-keep-awake';
import ImageResizer from 'react-native-image-resizer';
import Geocoder from 'react-native-geocoding';
import Location from '../common/Location';
import {GOOGLE_MAPS_APIKEY} from '../../utils/configs/Constants';

export default class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      coordinates: {
        latitude: '',
        longitude: '',
        accuracy: '',
      },
      showAlert: false,
      StaffNo: '',
      bearer_token: '',
      institute_id: '',
      showAlert1: false,
      error_message: '',
      travel_check_in: this.props.route?.params?.travel_check_in || 'no',
      qrCheckout: this.props.route?.params?.qrCheckin || 'false',
      notInFence: false,
      modal: false,
      show: this.props.route?.params?.show || false,
      count: 0,
      btnCount: 1,
      pressed: false,
      base64Data: null,
      isScanning: false,
      beaconID: '',
      beaconDetails: {},
      rssi: null,
      isBluetoothOn: false,
      travelCheckOut: false,
      current_travel_check_in: '',
      showAlert2: false,
    };
  }
  async componentDidMount() {
    Geocoder.init(GOOGLE_MAPS_APIKEY);
    const beacon = await AsyncStorage.getItem('beacon');
    const current_travel_check_in = await AsyncStorage.getItem(
      'current_travel_checkin',
    );
    this.setState({current_travel_check_in});
    console.log('f', beacon);
    if (beacon) {
      this.setState({beaconDetails: JSON.parse(beacon), isBluetoothOn: true});
      this.enableBluetooth();
    }

    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);

    AsyncStorage.getItem('user_id').then((user_id) => {
      AsyncStorage.getItem('travel_check_in').then((travel_check_in) => {
        AsyncStorage.getItem('bearer_token').then((bearer_token) => {
          AsyncStorage.getItem('institute_id').then((institute_id) => {
            this.setState({
              StaffNo: user_id,
              bearer_token: bearer_token,
              institute_id: institute_id,
              //travel_check_in: travel_check_in,
            });
          });
        });
      });
    });
  }
  startScan = () => {
    if (!this.state.isScanning) {
      BleManager.scan([], 5, true)
        .then((results) => {
          console.log('Scanning...');
          this.setState({isScanning: true});
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };

  enableBluetooth = () => {
    BleManager.enableBluetooth()
      .then(() => {
        // Success code
        console.log('The bluetooth is already enabled or the user confirm');
        //this.startConnect();
        BleManager.start({showAlert: false});
        this.startScan();
      })
      .catch((error) => {
        //Failure code
        Alert.alert('Attention', 'Please turn on your bluetooth', [
          {
            text: 'OK',
            onPress: () => {
              BleManager.enableBluetooth();
            },
          },
        ]);
        return;
      });
  };

  handleStopScan = () => {
    console.log('Scan is stopped');
    this.setState({isScanning: false});
  };

  handleDiscoverPeripheral = async (peripheral) => {
    console.log('peripheral', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'unknown';
    }

    if (
      this.state.beaconDetails &&
      this.state.beaconDetails.uniqueId === peripheral.id &&
      Math.abs(peripheral.rssi) <= 65
    ) {
      return alert(
        `Got beacon ${
          peripheral.id
        } with signal strength ${peripheral.rssi.toString()}`,
      );
      this.setState({beaconID: peripheral.id, rssi: Math.abs(peripheral.rssi)});
    }

    // BleManager.connect(peripheral.id).then(() => {
    //   BleManager.readRSSI(peripheral.id).then((rssi) => {
    //     //console.log('rssi', rssi)
    //     this.setState({beaconID: peripheral.id, beaconDetails: peripheral});
    //     return alert(
    //       `Got beacon ${peripheral.id} with signal strength ${rssi.toString()}`,
    //     );
    //   });
    // });
  };
  componentWillUnmount() {
    clearInterval(this.interval);
    Geolocation.clearWatch(this.watchId);
    bleManagerEmitter.removeListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    bleManagerEmitter.removeListener('BleManagerStopScan', this.handleStopScan);
  }

  openCamera = async (camera) => {
    const options1 = {quality: 1, base64: false, width: 800};
    const image1 = await camera.takePictureAsync(options1);

    if (image1.uri) {
      this.setState({show: false});
      this.setState({
        showAlert2: true,
        //error_message: 'Fetching location...',
        showAlert: false,
        showTimer: true,
      });
      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      )
        .then((response) => {
          console.log('size of image ===>', response.size);
          this.getCurrentLocationWithAccuracy(response.uri);
          // response.uri is the URI of the new image that can now be displayed, uploaded...
          // response.path is the path of the new image
          // response.name is the name of the new image with the extension
          // response.size is the size of the new image
        })
        .catch((err) => {
          console.log('error in resizing', err);
          // Oops, something went wrong. Check that the filename is correct and
          // inspect err to get more details.
        });

      //this.getCurrentLocationWithAccuracy(image1.uri);
    } else {
      this.setState({
        showAlert1: true,
        error_message: 'Please re authenticate your face',
        showAlert: false,
        showTimer: false,
      });
    }
  };
  getCurrentLocationWithAccuracy = (uri) => {
    // this.interval = setInterval(
    //   () => this.setState((prev) => ({count: prev.count + 1})),
    //   1000,
    // );
    let coordinatesTemp = {latitude: '', longitude: '', accuracy: ''};
    this.main(uri);
    // setTimeout(async () => {
    //   Geolocation.clearWatch(this.watchId);
    //   if (this.state.coordinates.accuracy > 10) {
    //     this.setState({showAlert2: false, showAlert: true});
    //     await this.checkout(uri, this.state.coordinates);
    //     return;
    //   } else {
    //     if (this.state.coordinates.accuracy === '') {
    //       this.setState({showAlert2: false, showAlert: true});
    //       Geolocation.getCurrentPosition(
    //         (pos) => this.checkout(uri, pos.coords),
    //         () => {},
    //         {enableHighAccuracy: true, maximumAge: 0, distanceFilter: 0},
    //       );
    //     }
    //     console.log('Accuracy achieved');
    //   }
    // }, 10000);
    this.watchId = Geolocation.watchPosition(
      async (position) => {
        console.log('[Matched]', position);
        //this.setState((prev) => ({count: prev.count + 1}));
        this.setState({
          coordinates: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            accuracy: position.coords.accuracy,
          },
        });
        coordinatesTemp.latitude = position.coords.latitude;
        coordinatesTemp.longitude = position.coords.longitude;
        coordinatesTemp.accuracy = position.coords.accuracy;
        if (position.coords.accuracy <= 10) {
          Geolocation.clearWatch(this.watchId);
          await this.checkout(uri, coordinatesTemp);
          return;
        }
      },
      (e) => {
        //alert('no current location found');
        console.log(e.message);
      },

      {
        enableHighAccuracy: true,
        interval: 1000,
        fastestInterval: 1000,
        distanceFilter: 0,
      },
    );
  };

  wait = (ms) => new Promise((resolve, reject) => setTimeout(resolve, ms));

  main = async (uri) => {
    await this.wait(10000);
    Geolocation.clearWatch(this.watchId);
    if (this.state.coordinates.accuracy > 10) {
      this.setState({showAlert2: false, showAlert: true});
      await this.checkout(uri, this.state.coordinates);
      return;
    } else {
      if (this.state.coordinates.accuracy === '') {
        this.setState({showAlert2: false, showAlert: true});
        Geolocation.getCurrentPosition(
          (pos) => this.checkout(uri, pos.coords),
          () => {},
          {enableHighAccuracy: true, maximumAge: 0, distanceFilter: 0},
        );
      }
      console.log('Accuracy achieved');
    }
  };

  checkout = async (uri, coordinates) => {
    let status = this.state.travelCheckOut;
    Geocoder.from(coordinates.latitude, coordinates.longitude)
      .then((json) =>
        this.setState(
          {travelCheckInAddress: json.results[0].formatted_address},
          async () => {
            console.log('travel_status', status);

            let uploadData = new FormData();
            uploadData.append('file', {
              uri: uri,
              name: 'photo.png',
              type: 'image/png',
            });
            uploadData.append('StaffNo', this.state.StaffNo);
            uploadData.append('Latitude', Number(coordinates.latitude));
            uploadData.append('Longitude', Number(coordinates.longitude));
            uploadData.append('InstituteId', this.state.institute_id);
            uploadData.append('Accuracy', Number(coordinates.accuracy));
            uploadData.append('IsTravelCheckIn', status);
            uploadData.append(
              'TravelCheckInAddress',
              status ? this.state.travelCheckInAddress : '',
            );
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            const config = {
              method: 'post',
              url: Const + 'api/Staff/CheckOutWithFormFile',
              headers: {
                'Content-Type': 'multipart/form-data',
              },
              data: uploadData,
              timeout: 60000,
              cancelToken: source.token,
            };
            setTimeout(() => {
              source.cancel('Request cancel after timeout');
            }, 30000);
            axios(config)
              .then((response) => response.data)

              // fetch(Const + 'api/Staff/CheckOutWithFormFile', {
              //   method: 'POST',
              //   body: uploadData,
              //   // isBeaconVerified: false,
              //   // AccessLocationId: null,
              // })
              //.then((response) => response.json())
              .then((json) => {
                this.setState({showAlert2: false, showAlert: true});
                clearInterval(this.interval);
                Geolocation.clearWatch(this.watchId);
                console.log('[JSON]==>', json);
                if (json.status) {
                  this.setState({
                    showAlert: false,
                    showAlert1: true,
                    error_message: json.message,
                    count: 0,
                  });
                  if (this.state.isBluetoothOn) {
                    BluetoothStateManager.disable().then((result) => {
                      // do something...
                      console.log('blettoth', result);
                    });
                  }
                  AsyncStorage.setItem('checked_out', 'yes');
                  if (status) {
                    AsyncStorage.setItem('current_travel_checkin', 'stopped');
                  }
                  try {
                    //AsyncStorage.setItem('coordinates', '');
                    //AsyncStorage.setItem('radius', '');
                    //AsyncStorage.setItem('type', '');
                    AsyncStorage.setItem('checkin_time', '');
                    AsyncStorage.setItem('beacon', '');
                    // AsyncStorage.setItem('geo_id', '');
                    // if (ReactNativeForegroundService.is_task_running('taskid')) {
                    //   ReactNativeForegroundService.remove_task('taskid');
                    // }
                    // Stoping Foreground service.
                    KeepAwake.deactivate();
                    //ReactNativeForegroundService.stop();

                    // return setTimeout(() => {
                    //   this.props.navigation.navigate('Home');
                    // }, 5200);
                  } catch (e) {
                    console.log(e);
                  }
                } else if (
                  this.state.beaconDetails &&
                  this.state.beaconDetails.uniqueId === this.state.beaconID &&
                  this.state.rssi <= 65
                ) {
                  let uploadData1 = new FormData();
                  uploadData1.append('file', {
                    uri: uri,
                    name: 'photo.png',
                    type: 'image/png',
                  });
                  uploadData1.append('StaffNo', this.state.StaffNo);
                  uploadData1.append('Latitude', Number(coordinates.latitude));
                  uploadData1.append(
                    'Longitude',
                    Number(coordinates.longitude),
                  );
                  uploadData1.append('InstituteId', this.state.institute_id);
                  uploadData1.append('Accuracy', Number(coordinates.accuracy));
                  uploadData1.append('IsTravelCheckIn', status);
                  uploadData1.append('IsBeaconVerified', true);
                  uploadData1.append(
                    'AccessLocationId',
                    this.state.beaconDetails.id,
                  );
                  axios({
                    method: 'post',
                    url: Const + 'api/Staff/CheckOutWithFormFile',
                    data: uploadData1,
                    headers: {'Content-Type': 'multipart/form-data'},
                  })
                    .then((response) => response.data)
                    .then((json) => {
                      clearInterval(this.interval);
                      Geolocation.clearWatch(this.watchId);
                      console.log('[JSON_BEACON]==>', json);
                      if (!json.status) {
                        this.setState({
                          showAlert1: true,
                          error_message: json.message,
                          showAlert: false,
                          count: 0,
                        });
                        return;
                      }
                      if (json.status) {
                        setTimeout(() => {
                          this.setState({
                            showAlert1: true,
                            error_message: json.message,
                            showAlert: false,
                            count: 0,
                          });
                          if (this.state.isBluetoothOn) {
                            BluetoothStateManager.disable().then((result) => {
                              // do something...
                              console.log('blettoth', result);
                            });
                          }
                          AsyncStorage.setItem('checked_out', 'yes');
                          if (status) {
                            AsyncStorage.setItem(
                              'current_travel_checkin',
                              'stopped',
                            );
                          }
                          try {
                            AsyncStorage.setItem('coordinates', '');
                            AsyncStorage.setItem('radius', '');
                            AsyncStorage.setItem('type', '');
                            AsyncStorage.setItem('checkin_time', '');
                            AsyncStorage.setItem('beacon', '');
                            AsyncStorage.setItem('geo_id', '');
                            return setTimeout(() => {
                              this.props.navigation.navigate('Home');
                            }, 5200);
                          } catch (e) {
                            console.log(e);
                          }
                        }, 1200);
                        return;
                      }
                    })
                    .catch((error) => {
                      return this.setState({
                        showAlert1: true,
                        error_message: `${error.message}`,
                        showAlert: false,
                        count: 0,
                      });
                    });
                  return;
                }
                if (this.state.coordinates.accuracy > 10 && json.isOutSide) {
                  this.setState({
                    showAlert: false,
                    modal: true,
                    count: 0,
                  });
                } else {
                  this.setState({
                    showAlert1: true,
                    error_message: json.message,
                    showAlert: false,
                    count: 0,
                  });
                  setTimeout(
                    () => this.props.navigation.navigate('Home'),
                    1200,
                  );
                }
              })
              .catch((error) => {
                clearInterval(this.interval);
                Geolocation.clearWatch(this.watchId);
                console.log('error', error);
                if (axios.isCancel(error)) {
                  console.log('Request canceled', error.message);
                  this.setState({
                    showAlert: false,
                    showAlert2: false,
                    showAlert1: true,
                    error_message:
                      'Request taking too long to respond. Refresh internet connection and retry.',
                    count: 0,
                    success: false,
                  });
                  return;
                }
                this.setState({
                  showAlert1: true,
                  error_message:
                    'Sorry! Unhandled exception occured while checking-out.',
                  showAlert: false,
                  count: 0,
                  showAlert2: false,
                });
              });
          },
        ),
      )
      .catch((e) => {
        console.log(e);
        this.setState({showAlert2: false, showAlert: false});
        alert('Unable to find location');
      });
  };

  openCamera1 = async (camera) => {
    const options1 = {quality: 1, base64: false};
    const body = new FormData();

    try {
      const image1 = await camera.takePictureAsync(options1);
      ImageResizer.createResizedImage(
        image1.uri,
        300,
        300,
        'PNG',
        0,
        0,
        undefined,
      ).then(async (response) => {
        console.log('size of image ===>', response.size);
        this.setState({showAlert: true});
        if (response.uri) {
          body.append('StaffNo', this.state.StaffNo);
          body.append('InstituteId', this.state.institute_id);
          body.append('File', {
            uri: response.uri,
            name: 'photo.png',
            type: 'image/png',
          });
          body.append('AccessLocationId', Number(this.state.data.locationId));
          body.append('QrCodeOwnerStaffCode', this.state.data.StaffNo);
          console.log(body);
          const response = await axios({
            method: 'POST',
            url: Const + 'api/staff/QrCheckOut',
            data: body,
            headers: {'Content-Type': 'multipart/form-data'},
          });
          this.setState({show1: false, showAlert: false});
          console.log('JSON ==>', response.data);
          const json = response.data;
          if (!json.status) {
            return alert(response.data.message);
          }

          if (json.status) {
            this.setState({
              showAlert1: true,
              error_message: json.message,
              showAlert: false,
              count: 0,
            });
            if (this.state.isBluetoothOn) {
              BluetoothStateManager.disable().then((result) => {
                // do something...
                console.log('blettoth', result);
              });
            }
            AsyncStorage.setItem('checked_out', 'yes');
            try {
              //AsyncStorage.setItem('coordinates', '');
              //AsyncStorage.setItem('radius', '');
              //AsyncStorage.setItem('type', '');
              AsyncStorage.setItem('checkin_time', '');
              AsyncStorage.setItem('beacon', '');
              // AsyncStorage.setItem('geo_id', '');
              // if (ReactNativeForegroundService.is_task_running('taskid')) {
              //   ReactNativeForegroundService.remove_task('taskid');
              // }
              // Stoping Foreground service.
              KeepAwake.deactivate();
              //ReactNativeForegroundService.stop();

              return setTimeout(() => {
                this.props.navigation.navigate('Home');
              }, 5200);
            } catch (e) {
              console.log(e);
            }
          }
          //alert(response.data.response.message)
        }
      });
    } catch (e) {
      console.log(e);
      alert(e.toString());
      this.setState({show1: false, showAlert: false});
    }
  };

  CheckOutWithBarCode = async ({data}) => {
    let parsedDate = JSON.parse(data);
    let now1 = new Date().valueOf();
    let then1 = new Date(parsedDate.timeStamp).valueOf();
    const difference = (now1 - then1) / 1000;
    if (difference > 10) {
      this.setState({show2: false}, () => {
        return alert('Time exceeded for check out');
      });
      return;
    }
    this.setState({
      data: JSON.parse(data),
      show2: false,
      show1: true,
    });
  };

  launchTipsModal = () => {
    this.setState({modal: true, notInFence: false});
  };
  goToHome = () => {
    this.props.navigation.navigate('Home');
  };
  render() {
    var cardStyle1 = {
      borderRadius: 10,
      elevation: 4,
      padding: 20,
    };
    var cardStyle2 = {
      borderRadius: 10,
      elevation: 4,
      padding: 20,
    };
    let {current_travel_check_in} = this.state;
    const mainCardStyle =
      this.state.travel_check_in == 'yes' ? cardStyle1 : cardStyle2;
    if (this.state.loader) {
      return <Loader />;
    }

    return (
      <View style={styles.container}>
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={true}
          title="Loading"
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
        />
        {this.state.showAlert2 && (
          <Location
            accuracy={Number(this.state.coordinates.accuracy).toFixed(2)}
            latitude={Number(this.state.coordinates.latitude)}
            longitude={Number(this.state.coordinates.longitude)}
            isVisible={this.state.showAlert2}
            text={'Please wait... Trying to check-out...'}
            //count={this.state.count}
          />
        )}
        <AwesomeAlert
          show={this.state.showAlert1}
          showProgress={
            this.state.error_message === 'Fetching location...' ? true : false
          }
          //title={'Attention'}
          title={
            this.state.error_message === 'Fetching location...'
              ? this.state.count.toString()
              : 'Attention'
          }
          message={this.state.error_message}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={true}
          showCancelButton={
            this.state.error_message === 'Fetching location...' ? false : true
          }
          cancelText="Okay"
          onCancelPressed={() => {
            this.setState({showAlert1: false}, () => this.goToHome());
          }}
          cancelButtonColor="#f05760"
          cancelButtonTextStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
          titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
        />
        <Container>
          <SubHeader
            title="Check Out"
            showBack={true}
            backScreen="Home"
            showBell={false}
            navigation={this.props.navigation}
          />
          <Content>
            {this.state.show && (
              <CameraComp
                visible={this.state.show}
                onClose={() => this.setState({show: false})}
                onPress={(camera) => this.openCamera(camera)}
                //onBarCodeRead = {this.CheckOutWithBarCode}
                //btnText={'check-out'}
                btnText={
                  !this.state.pressed
                    ? 'check-out'
                    : this.state.btnCount.toString()
                }
                onRecordingStart={(event) => {
                  //console.log('event', event);
                  this.interval2 = setInterval(() => {
                    this.setState((prev) => ({btnCount: prev.btnCount + 1}));
                  }, 1000);
                }}
                onRecordingEnd={() => {
                  clearInterval(this.interval2);
                  this.setState({
                    show: false,
                    btnCount: 0,
                    pressed: false,
                    showAlert: true,
                  });
                }}
              />
            )}
            {/* <View style = {{padding: 10, flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({qrCodeScanner: true, show2: true})
                    }>
                    <Card 
                      style={{
                      borderRadius: 10,
                      elevation: 4,
                      padding: 20,
                      width: wp('85'),
                    }}>
                      <Text style={styles.checkinText}>QR Code Check Out</Text>
                    </Card>
                  </TouchableWithoutFeedback>
              </View> */}

            <View style={{marginTop: 20, marginLeft: 15, marginRight: 15}}>
              <Text style={styles.heading}>
                Please select your Check OUT type
              </Text>
              <View style={styles.center}>
                <TouchableWithoutFeedback
                  onPress={() => {
                    if (current_travel_check_in === 'running') {
                      return alert(
                        'You have checked in via travel check in. Opt for Travel Check Out.',
                      );
                    }
                    this.setState({travelCheckOut: false, show: true});
                  }}>
                  <Card style={styles.mainCardStyle}>
                    <Text style={styles.checkinText}>Check OUT</Text>
                  </Card>
                </TouchableWithoutFeedback>
                {this.state.travel_check_in == 'yes' && (
                  <TouchableWithoutFeedback
                    onPress={() => {
                      if (current_travel_check_in === 'running') {
                        this.setState({travelCheckOut: true, show: true});
                      } else {
                        alert('You have not checked in via travel check in');
                      }
                    }}>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>Travel Check OUT</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
                {this.state.qrCheckout === 'true' && (
                  <TouchableWithoutFeedback
                    onPress={() =>
                      this.setState({qrCodeScanner: true, show2: true})
                    }>
                    <Card style={styles.mainCardStyle}>
                      <Text style={styles.checkinText}>QR Code Check OUT</Text>
                    </Card>
                  </TouchableWithoutFeedback>
                )}
              </View>
            </View>

            {this.state.show1 && (
              <CameraComp
                visible={this.state.show1}
                onClose={() => this.setState({show1: false})}
                onPress={(camera) => this.openCamera1(camera)}
                btnText={'check-out'}
              />
            )}
            {this.state.show2 && (
              <CameraComp
                visible={this.state.show2}
                onClose={() => this.setState({show2: false})}
                onPress={(camera) => {}}
                btnText={'QR Code Scanner'}
                onBarCodeRead={this.CheckOutWithBarCode}
                //type = {RNCamera.Constants.Type.back}
              />
            )}
            {this.state.modal && (
              <ModalForAccuracy
                visible={this.state.modal}
                onClose={() => this.setState({modal: false})}
                onPress={() => this.props.navigation.goBack()}
                //accuracy={parseInt(this.state.coordinates.accuracy).toString()}
                accuracy={Math.round(
                  Number(this.state.coordinates.accuracy),
                ).toString()}
                checkout
              />
            )}
          </Content>
        </Container>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  header: {
    backgroundColor: '#089bf9',
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 17,
    color: '#ffffff',
  },
  cardContainer: {
    marginTop: '10%',
    marginLeft: '6%',
    marginRight: '6%',
    justifyContent: 'center',
  },
  card: {
    borderRadius: 10,
    elevation: 4,
    paddingTop: '3%',
    paddingBottom: '3%',
    height: hp('60'),
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('35'),
    paddingRight: wp('7'),
    marginTop: '4%',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#ffffff',
  },
  btImage: {
    width: 50,
    height: 39,
    borderRadius: 20,
  },
  preview: {
    flex: 1,
  },
  heading: {
    fontFamily: 'Poppins-SemiBold',
    margin: 12,
  },
  buttonContainer1: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f05760',
    borderRadius: 20,
    width: wp('41'),
    marginTop: '4%',
  },
  mainCardStyle: {
    borderRadius: 10,
    elevation: 4,
    padding: 20,
    width: wp('85'),
  },
  checkinText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: '#ffffff',
    paddingRight: '3%',
  },
  userImage: {
    width: 120,
    height: 120,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  note: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
    marginBottom: 10,
  },
});
