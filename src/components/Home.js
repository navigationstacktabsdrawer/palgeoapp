import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Alert,
  Platform,
} from 'react-native';
import CustomHeader from './common/CustomHeader';
import SideBar from './common/Sidebar';
import {
  Container,
  Drawer,
  Card,
  Content,
  Textarea,
  Icon,
  Fab,
  Button,
} from 'native-base';
import {Icon as Icon1} from 'react-native-elements';
import Loader from './common/Loader';
import AsyncStorage from '@react-native-community/async-storage';
import BackgroundTimer from 'react-native-background-timer';
import Geolocation from 'react-native-geolocation-service';
import Const from './common/Constants';
import DocumentPicker from 'react-native-document-picker';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AwesomeAlert from 'react-native-awesome-alerts';
import Modal from 'react-native-modal';
import Geocoder from 'react-native-geocoding';
import ImagePicker from 'react-native-image-crop-picker';
import AskForUpdate from './common/AskForUpdate';
import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import {
  addTask,
  getStatus,
  compareTwoTime,
  loginAPI,
} from '../utils/helperFunctions';
import VersionCheck from 'react-native-version-check';
import RNFetchBlob from 'rn-fetch-blob';
import base64 from 'react-native-base64';
import Location from '../components/common/Location';
import {GOOGLE_MAPS_APIKEY} from '../utils/configs/Constants';
import {Colors} from '../utils/configs/Colors';
import Leaves from '../components/Requests/Leaves';

Geocoder.init(GOOGLE_MAPS_APIKEY);
const geolib = require('geolib');
var moment = require('moment');

const REQUEST_BUTTONS = [
  {
    iconName: 'handshake-slash',
    iconColor: 'white',
    buttonColor: '#34A34F',
    type: 'font-awesome-5',
  },
  {
    iconName: 'route',
    iconColor: 'white',
    buttonColor: '#DD5144',
    type: 'font-awesome-5',
  },
  {
    iconName: 'hand-grab-o',
    iconColor: 'white',
    buttonColor: Colors.maroon,
    type: 'font-awesome',
  },
  {
    iconName: 'sick',
    iconColor: 'white',
    buttonColor: '#34A34F',
    type: '',
  },
  {
    iconName: 'hand',
    iconColor: 'white',
    buttonColor: '#DD5144',
    type: 'entypo',
  },
];

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      checked_out: '',
      running_status: 'resume',
      newVersionRequired: false,
      position: {},
      bearer_token: '',
      StaffNo: '',
      institute_id: '',
      showAlert: false,
      showAlert1: false,
      error_message: '',
      modalVisible: false,
      modal: true,
      isPause: false,
      address: '',
      message: '',
      address: '',
      attachments: [],
      uploaded: false,
      current_travel_checkin: 'stopped',
      app_state: '',
      inactive: true,
      active: true,
      files: [],
      accuracy: '',
      latitude: 12.4,
      longitude: 78.3,
      active: false,
    };
  }

  successLocation = async (position) => {
    this.setState({
      position,
      accuracy: position.coords.accuracy,
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
    });
    //await AsyncStorage.setItem('userLocation', JSON.stringify(position.coords));
    console.log('accuracy_watch', position.coords);
  };

  errorLocation = (error) => console.log(error);

  async componentDidMount() {
    // //this.checkUpdateVersion();
    // this.subscip = DeviceEventEmitter.addListener(
    //   'notificationClickHandle',
    //   function (e) {
    //     console.log('json', e);
    //     //ForegroundService.cancelNotification(144)
    //     //ReactNativeForegroundService.dismiss(144)
    //   },
    // );
    // setTimeout(() => ReactNativeForegroundService.stop(), 10000 )
    // this.interval = setInterval(() => {
    loginAPI();
    // }, 60000*10)

    const locationPermissionsIos = await AsyncStorage.getItem(
      'locationPermissionsIos',
    );
    console.log('locationPermissionsIos ===>', locationPermissionsIos);
    // if(locationPermissionsIos){
    //   addTask()
    // }
    if (
      Platform.OS === 'ios' &&
      (locationPermissionsIos === 'false' || !locationPermissionsIos)
    ) {
      Alert.alert(
        'Attention',
        'Enable location services(always) for proper working of the app',
        [
          {
            text: 'Open Settings',
            onPress: () =>
              Linking.openSettings().then(async () => {
                // const locationState = await GPSState.getStatus()
                // console.log(locationState, '===')
                const res = await Geolocation.requestAuthorization('always');
                console.log('res===', res);
                if (res === 'granted') {
                  await AsyncStorage.setItem('locationPermissionsIos', 'true');
                }
              }),
          },
        ],
        {
          cancelable: false,
        },
      );
      //alert('Enable location services for proper working of the app')
    }

    this.watchID = Geolocation.watchPosition(
      this.successLocation,
      this.errorLocation,
      {
        enableHighAccuracy: true,
        accuracy: 'high',
        distanceFilter: 0,
        interval: 2000,
        fastestInterval: 1000,
      },
    );
    setTimeout(() => Geolocation.clearWatch(this.watchID), 4000);

    const checked_out = await AsyncStorage.getItem('checked_out');
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const StaffNo = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const running_status = await AsyncStorage.getItem('running_status');
    const isInside = await AsyncStorage.getItem('isInside');
    const geoOld = await AsyncStorage.getItem('geo_id');
    const isFaceRequired = await AsyncStorage.getItem('isFaceRequired');
    const currentTravelLocation = JSON.parse(
      await AsyncStorage.getItem('currentTravelLocation'),
    );
    //console.log('application', app_status);
    // const current_travel_checkin = await AsyncStorage.getItem(
    //   'current_travel_checkin',
    // );
    fetch(Const + 'api/Staff/IsCheckedInNew', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        staffCode: StaffNo,
        instituteId: Number(institute_id),
      }),
    })
      .then((response) => response.json())
      .then(async (json) => {
        //console.log('jOSN=-==', json);
        if (json && !json.isCheckedIn) {
          AsyncStorage.setItem('checked_out', 'yes');
          AsyncStorage.setItem('geo_id', '');
          AsyncStorage.setItem('radius', '');
          AsyncStorage.setItem('coordinates', JSON.stringify([]));
          AsyncStorage.setItem('current_travel_checkin', 'stopped');
          return;
        }
        if (json && json.isCheckedIn) {
          await AsyncStorage.setItem('checked_out', 'no');
          if (json.isTravelCheckIn) {
            await AsyncStorage.setItem('current_travel_checkin', 'running');
            if (!currentTravelLocation) {
              await AsyncStorage.setItem(
                'currentTravelLocation',
                JSON.stringify(this.state.position.coords),
              );
            }
            return this.setState({current_travel_checkin: 'running'});
          }
          if (json.locationDetails.type === 'Circle' && !json.isTravelCheckIn) {
            if (geoOld) {
              return;
            }
            await AsyncStorage.setItem(
              'radius',
              json.locationDetails.radius.toString(),
            );
            await AsyncStorage.setItem(
              'geo_id',
              json.locationDetails.id.toString(),
            );
            await AsyncStorage.setItem(
              'coordinates',
              JSON.stringify(json.locationDetails.coordinates),
            );
          }
          return;
        }
      })
      .catch((error) => console.log('error_IS_CHECKED_IN =>', error));
    const current_travel_checkin = await AsyncStorage.getItem(
      'current_travel_checkin',
    );
    const locations = JSON.parse(await AsyncStorage.getItem('locations'));
    this.setState({
      checked_out,
      bearer_token,
      StaffNo,
      institute_id,
      running_status: running_status ? running_status : 'resume',
      current_travel_checkin: current_travel_checkin
        ? current_travel_checkin
        : 'stopped',
    });

    let compareResult = false;

    console.log(
      'checked_out_mounted and_checkin current_travel',
      this.state.checked_out,
      this.state.current_travel_checkin,
    );

    AsyncStorage.getItem('locationPermissions').then((locationPermissions) => {
      console.log('locationPermissions ==>', locationPermissions);
      this.setState({locationPermissions: locationPermissions}, () => {
        if (
          this.state.locationPermissions === 'yes' ||
          this.state.locationPermissions
        ) {
          let compareArray = [];
          if (locations && locations.length > 0) {
            let currentTime = moment.utc().format('HH:mm');
            locations.forEach((location) => {
              let checkoutTime = moment(location.checkOutTime, 'HH:mm').format(
                'HH:mm',
              );
              console.log(currentTime, checkoutTime);
              compareResult = compareTwoTime(currentTime, checkoutTime);
              compareArray.push(compareResult);
              console.log('compare', compareResult);
            });
          }

          //console.log('permissions granted');
          const running = ReactNativeForegroundService.get_all_tasks();
          console.log('foreground_service===>', running);

          const run = ReactNativeForegroundService.is_running();

          console.log('service running ==>', run);

          if (
            compareArray.every((item) => item === true || checked_out === 'yes')
          ) {
            ReactNativeForegroundService.stop();
          }
          if (!running['taskid']) {
            //ReactNativeForegroundService.remove_task('taskid');
            addTask();
          }

          if (!run) {
            if (checked_out === 'no') {
              ReactNativeForegroundService.start({
                id: 144,
                title: 'Background Location Tracking',
                message: 'You are being tracked!',
              });
            }
          }

          // if (
          //   (!run && compareArray.includes(false)) ||
          //   !run && current_travel_checkin === 'running'
          // ) {
          //   ReactNativeForegroundService.start({
          //     id: 144,
          //     title: 'Background Location Tracking',
          //     message: 'You are being tracked!',
          //   });
          // }
        }
      });
    });

    this.willFocusSubscription = this.props.navigation.addListener(
      'focus',
      async () => {
        //this.checkUpdateVersion();

        const checked_out = await AsyncStorage.getItem('checked_out');
        const bearer_token = await AsyncStorage.getItem('bearer_token');
        const StaffNo = await AsyncStorage.getItem('user_id');
        const institute_id = await AsyncStorage.getItem('institute_id');
        const running_status = await AsyncStorage.getItem('running_status');
        const current_travel_checkin = await AsyncStorage.getItem(
          'current_travel_checkin',
        );
        const locations = JSON.parse(await AsyncStorage.getItem('locations'));
        //console.log('application_focus', app_status);

        this.setState({
          checked_out,
          bearer_token,
          StaffNo,
          institute_id,
          running_status: running_status ? running_status : 'resume',
          current_travel_checkin,
        });
        const running = ReactNativeForegroundService.get_all_tasks();
        const run = ReactNativeForegroundService.is_running();
        let compareResult = false;
        let compareArray = [];
        if (locations && locations.length > 0) {
          let currentTime = moment.utc().format('HH:mm');
          locations.forEach((location) => {
            let checkoutTime = moment(location.checkOutTime, 'HH:mm').format(
              'HH:mm',
            );
            console.log(currentTime, checkoutTime);
            compareResult = compareTwoTime(currentTime, checkoutTime);
            compareArray.push(compareResult);
            console.log('compare', compareResult);
          });
        }
        if (
          compareArray.every((item) => item === true) ||
          checked_out === 'yes'
        ) {
          ReactNativeForegroundService.stop();
        }
        //console.log('task is running? ' + JSON.stringify(running, null, 4));
        if (!run) {
          ReactNativeForegroundService.remove_task('taskid');
          addTask();
        }
        if (running['taskid'] && !run && checked_out === 'no') {
          ReactNativeForegroundService.start({
            id: 144,
            title: 'Background Location Tracking',
            message: 'You are being tracked!',
          });
        }

        const checkinStatus = await getStatus(
          bearer_token,
          StaffNo,
          institute_id,
        );
        //console.log('checkinStatus ===>', checkinStatus)
        if (checkinStatus) {
          AsyncStorage.setItem('checked_out', 'no');
        } else {
          AsyncStorage.setItem('checked_out', 'yes');
        }
        // if (running['taskid'] && this.state.checked_out === 'no' && !run) {
        //   ReactNativeForegroundService.start({
        //     id: 144,
        //     title: 'Background Location Tracking',
        //     message: 'You are being tracked!',
        //   });
        // }
        //console.log('checked_out', this.state.checked_out);
      },
    );

    //   if (Platform.OS == 'android') {
    //     const granted = await PermissionsAndroid.request(
    //       PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
    //       {
    //         title: 'Attention',
    //         message: 'Palgeo needs access to your location in background',
    //       },
    //     );
    //     if (granted) {
    //       console.log('Android Location permission granted');
    //     }
    //   } else if (Platform.OS == 'ios') {
    //     Geolocation.requestAuthorization('always');
    //   }
  }

  checkUpdateVersion = () => {
    //this.setState({showAlert: true});
    VersionCheck.needUpdate()
      .then(async (res) => {
        console.log('update needed ==>', res);
        if (res.isNeeded) {
          this.setState({newVersionRequired: true});
        } else {
          this.setState({newVersionRequired: false});
        }
      })
      .catch((e) => alert(e.toString()));
    // fetch(
    //   Const +
    //     'api/MobileApp/IsMobileUpdateRequired?version=' +
    //     parseInt(VersionNumber.buildVersion),
    //   {
    //     method: 'GET',
    //     headers: {
    //       Accept: 'text/plain',
    //       'Content-Type': 'application/json-patch+json',
    //     },
    //   },
    // )
    //   .then((response) => response.json())
    //   .then((json) => {
    //     console.log('version_json_check', json);
    //     if (json) {
    //       this.setState({newVersionRequired: true});
    //     } else {
    //       this.setState({newVersionRequired: false});
    //     }
    //   })
    //   .catch((error) => {
    //     //this.setState({showAlert: false});
    //     console.error(error);
    //   });
  };

  async componentWillUnmount() {
    this.props.navigation.removeListener(this.willFocusSubscription);
    Geolocation.clearWatch(this.watchID);
    BackgroundTimer.stopBackgroundTimer();
    clearInterval(this.interval);
    //this.subscip.remove();
  }

  openDrawer = () => {
    this.drawer._root.open();
  };
  openModal = async (status) => {
    console.log(status);
    const coordinates = JSON.parse(
      await AsyncStorage.getItem('userCurrentLocation'),
    );
    if (coordinates) {
      Geocoder.from(coordinates.latitude, coordinates.longitude)
        .then((json) => {
          this.setState({
            modalVisible: true,
            isPause: status,
            address: json.results[0].formatted_address,
            uploaded: false,
            message: '',
          });
        })
        .catch((error) => console.log(error));
    } else {
      Geolocation.getCurrentPosition(
        (position) => {
          var coordinates = {
            latitude: parseFloat(position.coords.latitude),
            longitude: parseFloat(position.coords.longitude),
          };
          Geocoder.from(coordinates.latitude, coordinates.longitude)
            .then((json) => {
              this.setState({
                modalVisible: true,
                isPause: status,
                address: json.results[0].formatted_address,
                uploaded: false,
                message: '',
              });
            })
            .catch((error) => console.log(error));
        },
        (error) => {},
        {enableHighAccuracy: true, timeout: 2000, maximumAge: 1000},
      );
    }
  };
  pauseOrResume = async (status) => {
    const coordinates = JSON.parse(
      await AsyncStorage.getItem('userCurrentLocation'),
    );
    if (coordinates) {
      this.setState({showAlert: true, modalVisible: false});
      fetch(Const + 'api/GeoFencing/travelcheckin/pause/update', {
        method: 'POST',
        withCredentials: true,
        credentials: 'include',
        headers: {
          Authorization: 'Bearer ' + this.state.bearer_token,
          Accept: 'application/json, text/plain',
          'Content-Type': 'application/json-patch+json',
        },
        body: JSON.stringify({
          instituteId: Number(this.state.institute_id),
          StaffCode: this.state.StaffNo,
          coordinates,
          isCheckOut: false,
          isCheckIn: false,
          address: this.state.address,
          message: this.state.message,
          isPause: status,
          attachments: !status ? this.state.attachments : [],
        }),
      })
        .then((response) => response.json())
        .then((json) => {
          console.log('json_travel', json);
          if (status) {
            AsyncStorage.setItem('running_status', 'pause');
            this.setState({running_status: 'pause'});
          } else if (!status) {
            AsyncStorage.setItem('running_status', 'resume');
            this.setState({running_status: 'resume'});
          }
          if (json.status) {
            this.setState({
              showAlert1: true,
              error_message: json.message,
              showAlert: false,
              files: [],
              attachments: [],
            });
          } else {
            this.setState({
              showAlert1: true,
              error_message: json.message,
              showAlert: false,
              files: [],
              attachments: [],
            });
          }
        })
        .catch((error) => {
          this.setState({
            showAlert1: true,
            error_message: 'Unknown error occured',
            showAlert: false,
            files: [],
            attachments: [],
          });
        });
    } else {
      Geolocation.getCurrentPosition(
        (position) => {
          const currentLongitude = parseFloat(position.coords.longitude);
          const currentLatitude = parseFloat(position.coords.latitude);
          var coordinates = {
            latitude: currentLatitude,
            longitude: currentLongitude,
          };
          this.setState({showAlert: true, modalVisible: false});
          fetch(Const + 'api/GeoFencing/travelcheckin/pause/update', {
            method: 'POST',
            withCredentials: true,
            credentials: 'include',
            headers: {
              Authorization: 'Bearer ' + this.state.bearer_token,
              Accept: 'application/json, text/plain',
              'Content-Type': 'application/json-patch+json',
            },
            body: JSON.stringify({
              instituteId: Number(this.state.institute_id),
              StaffCode: this.state.StaffNo,
              coordinates,
              isCheckOut: false,
              isCheckIn: false,
              address: this.state.address,
              message: this.state.message,
              isPause: status,
              attachments: !status ? this.state.attachments : [],
            }),
          })
            .then((response) => response.json())
            .then((json) => {
              console.log('json_travel', json);
              if (status) {
                AsyncStorage.setItem('running_status', 'pause');
                this.setState({running_status: 'pause'});
              } else if (!status) {
                AsyncStorage.setItem('running_status', 'resume');
                this.setState({running_status: 'resume'});
              }
              if (json.status) {
                this.setState({
                  showAlert1: true,
                  error_message: json.message,
                  showAlert: false,
                });
              } else {
                this.setState({
                  showAlert1: true,
                  error_message: json.message,
                  showAlert: false,
                });
              }
            })
            .catch((error) => {
              this.setState({
                showAlert1: true,
                error_message: 'Unknown error occured',
                showAlert: false,
              });
            });
        },
        (error) => {
          console.log(error);
        },
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 0},
      );
    }
  };
  uploadAttachments = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        includeBase64: true,
        type: [DocumentPicker.types.allFiles],
      });
      var tempArr = [];
      var files = [];
      console.log('files', results);
      results.forEach((res) => {
        RNFetchBlob.fs
          .readFile(res.uri, 'base64')
          .then((data) => {
            var Attachment = {
              fileName: res.name,
              fileType: res.type,
              attachment: data,
            };
            // console.log("Arr", Attachment)
            this.setState({
              attachments: [...this.state.attachments, Attachment],
              uploaded: true,
              files: [...this.state.files, res.name],
            });
            // tempArr.push(Attachment)
            // files.push(res.name)
          })
          .catch((err) => {});
        //console.log('files', files, tempArr)
      });
      //files = tempArr.map((tt) => tt.fileName)
    } catch (e) {
      console.log(e);
    }
    // let files = []
    // ImagePicker.openPicker({
    //   multiple: true,
    //   includeBase64: true,
    //   compressImageQuality: 0.5,
    // }).then((images) => {
    //   var tempArr = [];
    //   images.map((item, index) => {
    //     var attachmentObj = {
    //       fileName: item.path.replace(/^.*[\\\/]/, ''),
    //       fileType: item.mime,
    //       attachment: item.data,
    //     };
    //     tempArr.push(attachmentObj);
    //   });
    //  files = tempArr.map((tt) => tt.fileName)
    //   this.setState({attachments: tempArr, uploaded: true, files});
    // });
  };

  render() {
    if (this.state.loader) {
      return <Loader />;
    }
    if (this.state.newVersionRequired) {
      return <AskForUpdate />;
    }
    return (
      <View style={styles.container}>
        <Drawer
          ref={(ref) => {
            this.drawer = ref;
          }}
          content={<SideBar navigation={this.props.navigation} />}>
          <Container>
            <CustomHeader
              navigation={this.props.navigation}
              openDrawer={this.openDrawer}
            />

            <Content>
              <Modal isVisible={this.state.modalVisible}>
                <View style={styles.modalContainer}>
                  <View
                    style={{
                      position: 'absolute',
                      right: 0,
                      top: -10,
                      backgroundColor: 'red',
                      borderRadius: 20,
                    }}>
                    <Icon
                      name="times-circle"
                      type="FontAwesome5"
                      style={{color: '#fff'}}
                      onPress={() => {
                        this.setState({modalVisible: false, address: ''});
                      }}
                    />
                  </View>
                  <View style={styles.center}>
                    <Text style={styles.modalHeader}>
                      Please fill the details before you submit
                    </Text>
                  </View>
                  <View style={{marginTop: '3%'}}>
                    <View style={styles.labelContainer}>
                      <Text style={styles.label}>Current Address</Text>
                    </View>
                    <View style={styles.item1}>
                      <Textarea
                        rowSpan={3}
                        bordered
                        style={styles.textarea}
                        value={this.state.address}
                        onChangeText={(address) => {
                          this.setState({address: address});
                        }}
                        disabled
                      />
                    </View>
                  </View>
                  <View style={{marginTop: '4%'}}>
                    <View style={styles.labelContainer}>
                      <Text style={styles.label}>Message</Text>
                    </View>
                    <View style={styles.item1}>
                      <Textarea
                        rowSpan={3}
                        bordered
                        style={styles.textarea}
                        value={this.state.message}
                        onChangeText={(message) => {
                          this.setState({message: message});
                        }}
                      />
                    </View>
                  </View>
                  {!this.state.isPause && (
                    <View style={{marginTop: '4%'}}>
                      <View style={styles.input}>
                        <TouchableWithoutFeedback
                          onPress={this.uploadAttachments}>
                          <View>
                            {this.state.uploaded && (
                              // <Text style={styles.label}>
                              //   Uploaded Successfully
                              // </Text>
                              <Text style={styles.label}>
                                {this.state.files.join(',')}
                              </Text>
                            )}

                            {!this.state.uploaded && (
                              <Text style={styles.label}>
                                Upload Attachments
                              </Text>
                            )}
                          </View>
                        </TouchableWithoutFeedback>
                      </View>
                    </View>
                  )}
                  <TouchableWithoutFeedback
                    onPress={() => this.pauseOrResume(this.state.isPause)}>
                    <View style={styles.buttonContainer3}>
                      <Text style={styles.footerText}>Submit</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </Modal>
              <AwesomeAlert
                show={this.state.showAlert}
                showProgress={true}
                title="Loading"
                closeOnTouchOutside={false}
                closeOnHardwareBackPress={false}
              />
              <AwesomeAlert
                show={this.state.showAlert1}
                showProgress={false}
                title="Attention"
                message={this.state.error_message}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                cancelText="Okay"
                onCancelPressed={() => {
                  this.setState({showAlert1: false});
                }}
                cancelButtonColor="#f05760"
                cancelButtonTextStyle={{
                  fontFamily: 'Poppins-Regular',
                  fontSize: 13,
                }}
                messageStyle={{fontFamily: 'Poppins-Regular', fontSize: 13}}
                titleStyle={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}
              />
              <View style={{alignItems: 'center', margin: '4%'}}>
                <Text style={styles.text}>Palgeo Administrator Dashboard</Text>
                {this.state.current_travel_checkin == 'running' &&
                  this.state.checked_out == 'no' && (
                    <Card style={styles.walletContainer}>
                      <View style={styles.row}>
                        <View style={{width: wp('65')}}>
                          <View style={styles.headingContainer}>
                            <Text style={styles.heading}>
                              Active Travel Checkin
                            </Text>
                          </View>
                        </View>
                        <View>
                          {this.state.running_status == 'resume' &&
                            this.state.current_travel_checkin == 'running' && (
                              <TouchableWithoutFeedback
                                onPress={() => this.openModal(true)}>
                                <View style={styles.buttonContainer}>
                                  <Text style={styles.buttonText}>Pause</Text>
                                </View>
                              </TouchableWithoutFeedback>
                            )}
                          {this.state.running_status == 'pause' &&
                            this.state.current_travel_checkin == 'running' && (
                              <TouchableWithoutFeedback
                                onPress={() => this.openModal(false)}>
                                <View style={styles.buttonContainer1}>
                                  <Text style={styles.buttonText}>Resume</Text>
                                </View>
                              </TouchableWithoutFeedback>
                            )}
                        </View>
                      </View>
                    </Card>
                  )}
                {/* <Leaves /> */}
              </View>
            </Content>
          </Container>
        </Drawer>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  heading: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  walletContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    elevation: 5,
    width: wp('93'),
    paddingTop: '3%',
    paddingLeft: '4%',
    paddingRight: '4%',
    paddingBottom: '3%',
    marginTop: '5%',
  },
  row: {
    flexDirection: 'row',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'red',
    borderRadius: 10,
    width: wp('20'),
  },
  buttonContainer1: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'green',
    borderRadius: 10,
    width: wp('20'),
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#fff',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  headingContainer: {
    marginTop: 8,
  },
  modalContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: '5%',
  },
  modalHeader: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 13,
  },
  labelContainer: {
    margin: '1.5%',
  },
  label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#000000',
  },
  item1: {
    borderRadius: 8,
    backgroundColor: '#ffffff',
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
  textarea: {
    borderLeftWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderRadius: 8,
  },
  buttonContainer3: {
    width: wp('80'),
    backgroundColor: '#f05760',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
    borderRadius: 30,
    marginTop: hp('3'),
  },
  footerText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#ffffff',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    backgroundColor: '#ffffff',
    paddingLeft: '5%',
    borderRadius: 10,
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
    height: hp('7'),
    justifyContent: 'center',
  },
  item: {
    borderRadius: 10,
    backgroundColor: '#ffffff',
    height: hp('7'),
    borderColor: '#f1f1f1',
    borderWidth: 1.5,
  },
});
