import { Thumbnail } from "native-base";
import React, { Component } from "react";
import { Text, TouchableOpacity, StyleSheet, View } from "react-native";
import Timeline from "react-native-timeline-flatlist";
export class ApprovalStages extends Component {
  constructor(props) {
    super(props);
    this.renderEvent = this.renderEvent.bind(this);
  }
  renderEvent(rowData, sectionID, rowID) {
    console.log("rowData.imageUrl =", rowData.imageUrl);
    let title = (
      <Text
        style={{
          fontFamily: "Poppins-Regular",
          fontSize: 15,
          color: "#F15761",
        }}
      >
        {rowData.title}
      </Text>
    );
    var desc = null;
    if (rowData.description && rowData.imageUrl)
      desc = (
        <View>
          <Thumbnail source={{ uri: rowData.imageUrl }} />
          <Text style={{ fontFamily: "Poppins-Regular", fontSize: 15 }}>
            {rowData.description}
          </Text>
        </View>
      );

    return (
      <View style={{ flex: 1 }}>
        {title}
        {desc}
      </View>
    );
  }
  render() {
    let { onPress, title, color, width, headerColor, Arr } = this.props;
    return (
      <View
        style={{
          width: width,
          alignSelf: "center",
          elevation: 3,
          backgroundColor: "white",
          shadowColor: "silver",
          shadowOffset: { width: 0, height: 0.5 },
          shadowOpacity: 0.6,
          paddingBottom: 10,
          borderRadius: 10,
        }}
      >
        <View
          style={[
            styles.headerContainer,
            {
              backgroundColor: headerColor,
              alignSelf: "flex-start",
              alignItems: "flex-start",
              flexDirection: "column",
            },
          ]}
        >
          <Text style={[styles.text, { textAlign: "left" }]}>{title}</Text>
        </View>
        <Timeline
          data={Arr}
          style={{ padding: 10 }}
          circleColor={headerColor}
          innerCircle={"dot"}
          //showTime={true}
          renderEvent={this.renderEvent}
        />
        {/*{Arr.map((item, index) => {
          return (
            <View key={index} style={[styles.headerContainer]}>
              <View style={[styles.headerTitleContainer]}>
                <Text style={[styles.text, { color: headerColor }]}>
                  {item.stage}
                </Text>
              </View>
              <View style={[styles.headerTitleContainer, { flex: 0.5 }]}>
                <Thumbnail
                  square
                  style={{ borderRadius: 10 }}
                  source={item.photo}
                />
              </View>
              <View style={[styles.headerTitleContainer]}>
                <Text style={[styles.text, { color: headerColor }]}>
                  {item.name}
                </Text>
                <Text style={[styles.text, { color: headerColor }]}>
                  {item.position}
                </Text>
              </View>
            </View>
          );
        })}*/}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: "Poppins-Regular",
    fontSize: 15,
    color: "white",
  },
  headerContainer: {
    width: "100%",
    minHeight: 40,
    flexDirection: "row",
    borderRadius: 10,
    justifyContent: "space-evenly",
    alignItems: "center",
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  headerTitleContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
