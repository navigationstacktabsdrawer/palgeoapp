import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {Header, Body} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default function CustomHeader(props) {
  const notifications = () => {
    props.navigation.navigate('CircularList');
  };
  return (
    <Header style={styles.header} androidStatusBarColor="#f05760">
      <Body style={styles.row}>
        <TouchableWithoutFeedback onPress={props.openDrawer}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../assets/bars.png')}
              resizeMode="contain"
              style={styles.barImage}
            />
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.headerContainer}>
          <Text style={styles.headerText}>PalGeo</Text>
          {/* <Text style={styles.locationText}>PalGeo</Text> */}
        </View>
        <TouchableWithoutFeedback onPress={notifications}>
          <View style={styles.imageContainer1}>
            <Image
              source={require('../../assets/bell.png')}
              resizeMode="contain"
              style={styles.barImage}
            />
          </View>
        </TouchableWithoutFeedback>
      </Body>
    </Header>
  );
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#f05760',
    height: Platform.OS == 'android' ? hp('9') : undefined,
  },
  row: {
    flexDirection: 'row',
    paddingTop: Platform.OS == 'android' ? '5%' : '0%',
    paddingBottom: '5%',
  },
  barImage: {
    width: 30,
    height: 20,
  },
  headerContainer: {
    marginLeft: '3%',
    marginTop: '2%',
  },
  headerText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#ffffff',
  },
  imageContainer: {
    //marginLeft: '1%',
    marginTop: '2.5%',
    //backgroundColor: 'green',
    paddingHorizontal: 10,
  },
  imageContainer1: {
    position: 'absolute',
    right: 0,
    top: Platform.OS == 'android' ? hp('4') : null,
  },
  locationText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 11,
    color: '#ffffff',
  },
});
