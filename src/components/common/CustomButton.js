import React, { Component } from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";
export class CustomButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let { onPress, title, color, width } = this.props;
    return (
      <TouchableOpacity
        style={{
          backgroundColor: color,
          width: width,
          height: 40,
          justifyContent: "center",
          alignItems: "center",
          alignSelf: "center",
          borderRadius: 10,
          padding: 10,
        }}
        onPress={() => {
          onPress();
        }}
      >
        <Text style={styles.text}>{title}</Text>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: "Poppins-Regular",
    fontSize: 15,
    color: "white",
  },
});
