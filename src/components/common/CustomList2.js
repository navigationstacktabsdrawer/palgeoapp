import moment from 'moment';
import {Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
export class CustomList2 extends Component {
  constructor(props) {
    super(props);
    this.state = {Active: -1};
  }
  render() {
    let {
      onPress,
      onPress2,
      title1,
      title2,
      title3,
      color,
      width,
      headerColor,
      Arr,
    } = this.props;
    return (
      <View
        style={{
          width: width,
          alignSelf: 'center',
          backgroundColor: color,
          borderRadius: 10,
        }}>
        <View style={[styles.headerContainer, {backgroundColor: headerColor}]}>
          <View
            style={[
              styles.headerTitleContainer,
              {alignItems: 'flex-start', paddingLeft: 20},
            ]}>
            <Text style={styles.text}>{title1}</Text>
          </View>
          <View style={styles.headerTitleContainer}>
            <Text style={styles.text}>{title2}</Text>
          </View>
          <View style={[styles.headerTitleContainer, {alignItems: 'flex-end'}]}>
            <Text style={styles.text}>{title3}</Text>
          </View>
        </View>
        <ScrollView
          style={{
            width: '100%',
            maxHeight: 200,
            minHeight: 100,
          }}>
          {Arr.length ? (
            Arr.map((item, index) => {
              //  console.log(item);
              return (
                <View
                  key={index}
                  style={[
                    styles.headerContainer,
                    {
                      //backgroundColor:
                      //  item.holiday == null ? "transparent" : "white",
                      marginBottom: 10,
                    },
                  ]}>
                  <View
                    style={[
                      styles.headerTitleContainer,
                      {alignItems: 'flex-start', paddingLeft: 20},
                    ]}>
                    <Text style={[styles.text, {color: 'black'}]}>
                      {moment(item).format('DD.MM.YYYY')}
                    </Text>
                  </View>
                  {/*{item.holiday == null ? (*/}
                  <View
                    style={[
                      styles.headerTitleContainer,
                      {
                        flexDirection: 'row',
                        justifyContent: 'center',
                      },
                    ]}>
                    <Text
                      style={[styles.text, {color: 'black', marginRight: 10}]}>
                      {/*{item.session1}*/}
                      Morning
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        if (this.state.Morning == index) {
                          this.setState({Morning: -1});
                        } else {
                          this.setState({Morning: index}, function () {
                            onPress(
                              this.state.Morning == index ? true : false,
                              item,
                            );
                          });
                        }
                      }}>
                      {this.state.Morning == index ? (
                        <Thumbnail
                          square
                          style={{width: 20, height: 20}}
                          source={require('../../assets/checkBox.png')}
                        />
                      ) : (
                        <View
                          style={{
                            width: 20,
                            height: 20,
                            borderWidth: 1,
                            borderRadius: 5,
                            backgroundColor: 'white',
                            borderColor: headerColor,
                          }}
                        />
                      )}
                    </TouchableOpacity>
                  </View>
                  {/*) : (
                    <View style={[styles.headerTitleContainer, { flex: 2 }]}>
                      <Text style={[styles.text, { color: "black" }]}>
                        {item.holiday}
                      </Text>
                    </View>
                  )}*/}
                  {/*{item.holiday == null ? (*/}
                  <View
                    style={[
                      styles.headerTitleContainer,
                      {
                        flexDirection: 'row',
                        justifyContent: 'center',
                      },
                    ]}>
                    <Text
                      style={[styles.text, {color: 'black', marginRight: 10}]}>
                      {/*{item.session2}*/}
                      Noon
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        if (this.state.Noon == index) {
                          this.setState({Noon: -1});
                        } else {
                          this.setState({Noon: index}, function () {
                            onPress2(
                              this.state.Noon == index ? true : false,
                              item,
                            );
                          });
                        }
                      }}>
                      {this.state.Noon == index ? (
                        <Thumbnail
                          square
                          style={{width: 20, height: 20}}
                          source={require('../../assets/checkBox.png')}
                        />
                      ) : (
                        <View
                          style={{
                            width: 20,
                            height: 20,
                            borderWidth: 1,
                            borderRadius: 5,
                            backgroundColor: 'white',
                            borderColor: headerColor,
                          }}
                        />
                      )}
                    </TouchableOpacity>
                  </View>
                  {/*) : null}*/}
                </View>
              );
            })
          ) : (
            <View style={{marginTop: 35}}>
              <ActivityIndicator />
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
    color: 'white',
  },
  headerContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    borderRadius: 15,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  headerTitleContainer: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
