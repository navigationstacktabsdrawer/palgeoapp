// import {Row} from 'native-base';
// import React from 'react';
// import {
//   StyleSheet,
//   Text,
//   View,
//   TouchableOpacity,
//   Modal,
//   Dimensions,
//   Image,
// } from 'react-native';
// import {RNCamera} from 'react-native-camera';

// const CameraComp = (props) => {
//   const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//       flexDirection: 'column',
//       zIndex: 1,
//       //backgroundColor: 'green',
//     },
//     preview: {
//       //flex: !props.reduce ? 1 : null,
//       flex: 1,
//       //height: props.reduce ? Dimensions.get('screen').height * 0.7 : null,
//       //justifyContent: 'flex-end',
//       //alignItems: 'flex-end',
//       //borderTopWidth: 60,
//       //borderColor: '#f05760',
//       //marginTop: props.reduce ? Dimensions.get('screen').height * 0.25 : null,
//       //backgroundColor: 'yellow',
//       //zIndex: 2,
//     },
//     capture: {
//       flex: 0,
//       width: Dimensions.get('screen').width,
//       backgroundColor: '#f05760',
//       //borderRadius: 5,
//       padding: 15,
//       paddingHorizontal: 20,
//       alignSelf: 'center',
//       justifyContent: 'flex-end',
//       alignItems: 'center',
//       //margin: 20,
//     },
//   });
//   return (
//     <Modal
//       visible={props.visible}
//       onRequestClose={props.onClose}
//       onDismiss={props.onClose}
//       //statusBarTranslucent={true}
//       style={styles.container}>
//       {props.reduce && props.pressed && (
//         <View
//           style={{
//             //marginTop: 30,
//             //flex: 1,
//             height: Dimensions.get('screen').height * 0.4,
//             marginBottom: 30,
//             //flexDirection: 'row',
//             alignItems: 'center',
//             justifyContent: 'center',
//             //paddingBottom: 10,
//             // backgroundColor: 'red',
//           }}>
//           <Image
//             source={require('../../assets/update.jpg')}
//             style={{
//               width: 100,
//               height: 100,

//               margin: 0,
//             }}
//             resizeMode="contain"
//           />
//           <Text
//             style={{
//               fontSize: 20,
//               textAlign: 'center',
//               fontFamily: 'Poppins-SemiBold',
//             }}>
//             {props.question} and {props.text}
//           </Text>
//         </View>
//       )}
//       <RNCamera
//         style={styles.preview}
//         type={RNCamera.Constants.Type.front}
//         flashMode={RNCamera.Constants.FlashMode.auto}
//         //defaultVideoQuality={'4:3'}
//         onRecordingStart={props.onRecordingStart}
//         onRecordingEnd={props.onRecordingEnd}
//         captureAudio={false}
//         playSoundOnRecord={true}
//         //pictureSize="60x60"
//         androidCameraPermissionOptions={{
//           title: 'Permission to use camera',
//           message: 'We need your permission to use your camera',
//           buttonPositive: 'Ok',
//           buttonNegative: 'Cancel',
//         }}>
//         {({camera, status}) => {
//           if (status !== 'READY') return <Text>Waiting</Text>;
//           return (
//             <>
//               <View
//                 style={{
//                   flex: 1,
//                   //flexDirection: 'row',
//                   justifyContent: 'flex-end',
//                   alignSelf: 'center',
//                 }}>
//                 <Row>
//                   <View
//                     style={{
//                       width: 30,
//                       //height: 1200,
//                       backgroundColor: '#f05760',
//                       marginRight: Dimensions.get('window').width - 30,
//                     }}></View>
//                   <View
//                     style={{
//                       width: 30,
//                       //height: '100%',
//                       backgroundColor: '#f05760',
//                     }}></View>
//                 </Row>
//                 <TouchableOpacity
//                   onPress={() => props.onPress(camera)}
//                   style={styles.capture}
//                   disabled={props.pressed}>
//                   <Text
//                     style={{
//                       fontSize: 17,
//                       color: 'white',
//                       textTransform: 'uppercase',
//                       letterSpacing: 2,
//                       fontFamily: 'Poppins-Regular',
//                       fontWeight: 'bold',
//                     }}>
//                     {' '}
//                     {props.btnText}{' '}
//                   </Text>
//                 </TouchableOpacity>
//               </View>
//             </>
//           );
//         }}
//       </RNCamera>
//     </Modal>
//   );
// };
// export default CameraComp;


import {Row} from 'native-base';
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  Image,
} from 'react-native';
import {RNCamera} from 'react-native-camera';

const CameraComp = (props) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      zIndex: 1,
      //backgroundColor: 'green',
    },
    preview: {
      //flex: !props.reduce ? 1 : null,
      flex: 1,
      //height: props.reduce ? Dimensions.get('screen').height * 0.7 : null,
      //justifyContent: 'flex-end',
      //alignItems: 'flex-end',
      //borderTopWidth: 60,
      //borderColor: '#f05760',
      //marginTop: props.reduce ? Dimensions.get('screen').height * 0.25 : null,
      //backgroundColor: 'yellow',
      //zIndex: 2,
    },
    capture: {
      flex: 0,
      width: Dimensions.get('screen').width,
      backgroundColor: '#f05760',
      //borderRadius: 5,
      padding: 15,
      paddingHorizontal: 20,
      alignSelf: 'center',
      justifyContent: 'flex-end',
      alignItems: 'center',
      //margin: 20,
    },
  });
  return (
    <Modal
      visible={props.visible}
      onRequestClose={props.onClose}
      onDismiss={props.onClose}
      //statusBarTranslucent={true}
      style={styles.container}>
      {props.reduce && props.pressed && (
        <View
          style={{
            //marginTop: 30,
            //flex: 1,
            height: Dimensions.get('screen').height * 0.4,
            marginBottom: 30,
            //flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            //paddingBottom: 10,
            // backgroundColor: 'red',
          }}>
          <Image
            source={require('../../assets/update.jpg')}
            style={{
              width: 100,
              height: 100,

              margin: 0,
            }}
            resizeMode="contain"
          />
          <Text
            style={{
              fontSize: 20,
              textAlign: 'center',
              fontFamily: 'Poppins-SemiBold',
            }}>
            {props.question} and {props.text}
          </Text>
        </View>
      )}
      <RNCamera
        style={styles.preview}
        type={props.onBarCodeRead ? RNCamera.Constants.Type.back:RNCamera.Constants.Type.front}
        flashMode={RNCamera.Constants.FlashMode.auto}
        onBarCodeRead = {props.onBarCodeRead}
        onRecordingStart={props.onRecordingStart}
        onRecordingEnd={props.onRecordingEnd}
        captureAudio={false}
        playSoundOnRecord={true}
        onPictureTaken = {props.onPictureTaken}
        //pictureSize="60x60"
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>
        {({camera, status}) => {
          if (status !== 'READY') return <Text>Waiting</Text>;
          return (
            <>
              <View
                style={{
                  flex: 1,
                  //flexDirection: 'row',
                  justifyContent: 'flex-end',
                  alignSelf: 'center',
                }}>
                <Row>
                  <View
                    style={{
                      width: 30,
                      //height: 1200,
                      backgroundColor: '#f05760',
                      marginRight: Dimensions.get('window').width - 30,
                    }}></View>
                  <View
                    style={{
                      width: 30,
                      //height: '100%',
                      backgroundColor: '#f05760',
                    }}></View>
                </Row>
                <TouchableOpacity
                  onPress={() => props.onPress(camera)}
                  style={styles.capture}
                  disabled={props.pressed}>
                  <Text
                    style={{
                      fontSize: 17,
                      color: 'white',
                      textTransform: 'uppercase',
                      letterSpacing: 2,
                      fontFamily: 'Poppins-Regular',
                      fontWeight: 'bold',
                    }}>
                    {' '}
                    {props.btnText}{' '}
                  </Text>
                </TouchableOpacity>
              </View>
            </>
          );
        }}
      </RNCamera>
    </Modal>
  );
};
export default CameraComp;
