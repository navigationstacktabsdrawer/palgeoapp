import React from 'react';
import  { View , Text } from 'react-native';
import Loader from './Loader';
export default class Nodata extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loader : true
        }
    }
    componentDidMount(){
        setTimeout(()=>{this.setState({loader:false})},2800)
    }
    render(){
        if(this.state.loader){
            return(
                <Loader /> 
            )
        }
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontFamily:'Poppins-Regular',fontSize:15,color:'red'}}>{this.props.title}</Text>
            </View>
        )
    }
   
}