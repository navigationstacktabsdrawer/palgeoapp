import React, {Component} from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {Item, Picker} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const screenWidth = Dimensions.get('window').width;

export default class CustomPicker extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let {options, label, selectedValue, onValueChange, value} = this.props;
    return (
      <Item regular style={styles.item}>
        <Picker
          note
          mode="dropdown"
          style={{width: screenWidth - 20, alignSelf: 'center'}}
          selectedValue={selectedValue}
          onValueChange={(value) => onValueChange(value)}>
          {options.map((item, ind) => {
            return (
              <Picker.Item label={item.name} value={item.id} key={item.id} />
            );
          })}
        </Picker>
      </Item>
    );
  }
}
const styles = StyleSheet.create({
  item: {
    borderRadius: 10,
    marginBottom: 15,
    alignSelf: 'center',
    width: '95%',
    backgroundColor: 'white',
    shadowColor: 'silver',
    elevation: 3,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.3,
    height: hp('5'),
  },
});
