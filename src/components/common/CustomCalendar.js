import {Item, Picker, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import CustomPicker from './CustomPicker';
import moment from 'moment';
import CalendarStrip from 'react-native-calendar-strip';
let datesWhitelist = [
  {
    start: moment(),
    end: moment().add(2000000, 'days'), // total 4 days enabled
  },
];
let datesBlacklist = [moment().add(1, 'days')]; // 1 day disabled
export class CustomCalendar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {onPress, color, themeColor} = this.props;

    return (
      <View style={{width: '100%', alignItems: 'center'}}>
        <CalendarStrip
          scrollable
          scrollerPaging
          onDateSelected={(value) => {
            onPress(value);
          }}
          calendarAnimation={{type: 'sequence', duration: 30}}
          daySelectionAnimation={{
            type: 'background',
            duration: 200,
            highlightColor: themeColor,
          }}
          style={{
            height: 100,
            width: '95%',
            alignSelf: 'center',
            paddingTop: 10,
            paddingBottom: 10,
            borderRadius: 5,
            elevation: 3,
            shadowColor: color,
            shadowOpacity: 0.4,
            shadowOffset: {width: 0, height: 0},
          }}
          calendarHeaderStyle={{color: themeColor}}
          calendarColor={'white'}
          dateNumberStyle={{color: themeColor}}
          dateNameStyle={{color: themeColor}}
          highlightDateNumberStyle={{color: 'white'}}
          highlightDateNameStyle={{color: 'white'}}
          disabledDateNameStyle={{color: 'black'}}
          disabledDateNumberStyle={{color: 'black'}}
          datesWhitelist={datesWhitelist}
          //datesBlacklist={datesBlacklist}
          iconContainer={{flex: 0.1}}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  arrow: {
    backgroundColor: 'white',
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 10,
    padding: 10,
  },
  thumbnail: {
    borderRadius: 10,
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  monthView: {
    padding: 5,
    borderRadius: 10,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
