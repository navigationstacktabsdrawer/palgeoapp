import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Image,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Icon,
  Left,
  Body,
  Right,
  Toast,
} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';

import Const from './Constants';
export default class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      profile_pic: '',
      showOtherCheckInMenu: false,
      otherStaff: [],
      menuLoader: false,
      menuLoader1: false,
      menuLoader2: false,
      menuLoader3: false,
      user_id: null,
      institute_id: null,
      bearer_token: null,
      travel_check_in: null,
      menus: [],
      qrCheckin: 'false',
      qrCheckout: 'false',
    };
  }
  async componentDidMount() {
    const bearer_token = await AsyncStorage.getItem('bearer_token');
    const user_id = await AsyncStorage.getItem('user_id');
    const institute_id = await AsyncStorage.getItem('institute_id');
    const travel_check_in = await AsyncStorage.getItem('travel_check_in');
    const qrCheckin = await AsyncStorage.getItem('qrCheckin');
    const qrCheckout = await AsyncStorage.getItem('qrCheckout');
    const menus = await AsyncStorage.getItem('menus');
    const isFaceRequired = await AsyncStorage.getItem('isFaceRequired');
    const isCCTVFaceRegister = await AsyncStorage.getItem('isCCTVFaceRegister');

    console.log('isFace===>', isFaceRequired);
    this.setState({
      bearer_token,
      user_id,
      institute_id,
      travel_check_in,
      menus: JSON.parse(menus),
      qrCheckin,
      qrCheckout,
      isFaceRequired: isFaceRequired === 'true' ? true : false,
      isCCTVFaceRegister: isCCTVFaceRegister === 'true' ? true : false,
    });
    AsyncStorage.getItem('profile_pic').then((profile_pic) => {
      this.setState({profile_pic: profile_pic});
    });
    this._unsubscribe = this.props.navigation.addListener('focus', async () => {
      AsyncStorage.getItem('profile_pic').then((profile_pic) => {
        console.log('profic_pic', profile_pic);
        this.setState({profile_pic: profile_pic});
      });
      const bearer_token = await AsyncStorage.getItem('bearer_token');
      const user_id = await AsyncStorage.getItem('user_id');
      const institute_id = await AsyncStorage.getItem('institute_id');
      const travel_check_in = await AsyncStorage.getItem('travel_check_in');
      this.setState({
        bearer_token,
        user_id,
        institute_id,
        travel_check_in,
      });
    });
    this.CheckOtherCheckIn();
  }

  componentWillUnmount() {
    this.props.navigation.removeListener(this._unsubscribe);
  }

  goToFaceRegister = () => {
    this.props.navigation.navigate('FaceRegister', {
      showOtherCheckInMenu: this.state.showOtherCheckInMenu,
      otherStaff: this.state.otherStaff,
      show: !this.state.isCCTVFaceRegister,
      show1: this.state.isCCTVFaceRegister,
    });
  };
  goToFaceRegisterOther = () => {
    this.props.navigation.navigate('FaceRegister1', {
      showOtherCheckInMenu: this.state.showOtherCheckInMenu,
      otherStaff: this.state.otherStaff,
    });
  };
  goToCheckIn = () => {
    this.setState({menuLoader1: true});
    const {bearer_token, institute_id, user_id, travel_check_in, qrCheckin} =
      this.state;
    fetch(Const + 'api/Staff/IsCheckedInNew', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        staffCode: user_id,
        instituteId: Number(institute_id),
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          showAlert: false,
          loader: false,
          menuLoader1: false,
        });
        if (json && json.isCheckedIn) {
          Toast.show({
            text: 'You have already checked in.Please view your tasks',
            duration: 3000,
            type: 'warning',
            textStyle: {
              fontFamily: 'Poppins-Regular',
              color: '#ffffff',
              textAlign: 'center',
              fontSize: 14,
            },
          });
          this.props.navigation.navigate('StaffTasks', {disable: true});
        } else {
          //console.log('ttravel', travel_check_in);
          if (travel_check_in === 'no' && qrCheckin === 'false') {
            //this.props.navigation.navigate('Checkin', {travel: false});
            this.props.navigation.navigate('Checkin', {show: true});
          } else {
            this.props.navigation.navigate('Checkin', {
              travel: travel_check_in,
              qrCheckin,
              show: false,
            });
          }
        }
      })
      .catch((error) => {
        this.setState({showAlert: false, loader: false});
        console.error(error);
      });
  };
  goToCheckout = () => {
    const {travel_check_in, qrCheckout} = this.state;
    AsyncStorage.getItem('institute_id').then((institute_id) => {
      AsyncStorage.getItem('user_id').then((user_id) => {
        AsyncStorage.getItem('bearer_token').then((bearer_token) => {
          //console.log(bearer_token);
          this.setState({menuLoader: true});
          fetch(Const + 'api/Staff/StaffTasks', {
            method: 'POST',
            withCredentials: true,
            credentials: 'include',
            headers: {
              Authorization: bearer_token,
              Accept: 'text/plain',
              'Content-Type': 'application/json-patch+json',
            },
            body: JSON.stringify({
              staffCode: user_id,
              instituteId: institute_id,
            }),
          })
            .then((response) => response.json())
            .then((json) => {
              console.log('tasks_json', json);
              if (json.length == 0) {
                fetch(Const + 'api/Staff/IsCheckedInNew', {
                  method: 'POST',
                  withCredentials: true,
                  credentials: 'include',
                  headers: {
                    Authorization: bearer_token,
                    Accept: 'text/plain',
                    'Content-Type': 'application/json-patch+json',
                  },
                  body: JSON.stringify({
                    staffCode: user_id,
                    instituteId: Number(institute_id),
                  }),
                })
                  .then((response) => response.json())
                  .then((json) => {
                    console.log(json);
                    this.setState({
                      showAlert: false,
                      loader: false,
                      menuLoader: false,
                    });
                    if (json && json.isCheckedIn) {
                      if (travel_check_in === 'no' && qrCheckout === 'false') {
                        this.props.navigation.navigate('Checkout', {
                          show: true,
                        });
                      } else {
                        this.props.navigation.navigate('Checkout', {
                          show: false,
                          travel_check_in,
                          qrCheckin: qrCheckout,
                        });
                      }
                    } else {
                      alert('Please check in first');
                    }
                  })
                  .catch((error) => {
                    this.setState({showAlert: false, loader: false});
                    console.error(error);
                  });
              } else {
                this.setState({
                  showAlert: false,
                  loader: false,
                  menuLoader: false,
                });
                if (travel_check_in === 'no' && qrCheckout === 'false') {
                  this.props.navigation.navigate('StaffTasks', {
                    show: true,
                    disable: false,
                  });
                } else {
                  this.props.navigation.navigate('StaffTasks', {
                    show: false,
                    travel_check_in,
                    qrCheckin: qrCheckout,
                    disable: false,
                  });
                }
              }
            })
            .catch((error) => {
              console.error(error);
            });
        });
      });
    });
  };
  goToLiveCheckout = () => {
    AsyncStorage.getItem('institute_id').then((institute_id) => {
      AsyncStorage.getItem('user_id').then((user_id) => {
        AsyncStorage.getItem('bearer_token').then((bearer_token) => {
          this.setState({menuLoader3: true});
          fetch(Const + 'api/Staff/StaffTasks', {
            method: 'POST',
            withCredentials: true,
            credentials: 'include',
            headers: {
              Authorization: bearer_token,
              Accept: 'text/plain',
              'Content-Type': 'application/json-patch+json',
            },
            body: JSON.stringify({
              staffCode: user_id,
              instituteId: institute_id,
            }),
          })
            .then((response) => response.json())
            .then((json) => {
              if (json.length == 0) {
                fetch(Const + 'api/Staff/IsCheckedInNew', {
                  method: 'POST',
                  withCredentials: true,
                  credentials: 'include',
                  headers: {
                    Authorization: bearer_token,
                    Accept: 'text/plain',
                    'Content-Type': 'application/json-patch+json',
                  },
                  body: JSON.stringify({
                    staffCode: user_id,
                    instituteId: institute_id,
                  }),
                })
                  .then((response) => response.json())
                  .then((json) => {
                    this.setState({
                      showAlert: false,
                      loader: false,
                      menuLoader3: false,
                    });
                    if (json && json.isCheckedIn) {
                      this.props.navigation.navigate('LiveCheckOut', {
                        show: true,
                        travel_check_in: this.state.travel_check_in,
                      });
                    } else {
                      alert('Please check in first');
                    }
                  })
                  .catch((error) => {
                    this.setState({showAlert: false, loader: false});
                    console.error(error);
                  });
              } else {
                this.props.navigation.navigate('StaffTasks', {disable: false});
              }
            })
            .catch((error) => {
              console.error(error);
            });
        });
      });
    });
  };
  CheckOtherCheckIn = () => {
    AsyncStorage.getItem('org_id').then((org_id) => {
      AsyncStorage.getItem('user_id').then((user_id) => {
        AsyncStorage.getItem('bearer_token').then((bearer_token) => {
          fetch(
            Const + `api/Staff/staff/descendantStaff/${user_id}/${org_id}`,
            {
              method: 'GET',
              withCredentials: true,
              credentials: 'include',
              headers: {
                Authorization: bearer_token,
                Accept: 'text/plain',
                'Content-Type': 'application/json-patch+json',
              },
            },
          )
            .then((response) => response.json())
            .then((json) => {
              //console.log('json', json);
              if (json.length == 0) {
                this.setState({showOtherCheckInMenu: false, otherStaff: []});
              } else {
                this.setState({showOtherCheckInMenu: true, otherStaff: json});
              }
            })
            .catch((error) => {
              console.error(error);
            });
        });
      });
    });
  };
  goToPrivacy = () => {
    this.props.navigation.navigate('Privacy', {backScreen: 'Home'});
  };
  goToReports = () => {
    this.props.navigation.navigate('Reports');
  };
  goToManagerReports = () => {
    this.props.navigation.navigate('ManagerReports');
  };
  goToSelectInstitute = () => {
    this.props.navigation.navigate('SelectInstitute');
  };
  goToCircular = () => {
    this.props.navigation.navigate('Circular');
  };
  goToCircularList = () => {
    this.props.navigation.navigate('CircularList');
  };
  goToMyProfile = () => {
    this.props.navigation.navigate('MyProfile');
  };
  goToLocateMenu = () => {
    this.props.navigation.navigate('LocateMenuStep1');
  };
  goToOtherCheckIn = () => {
    this.props.navigation.navigate('OtherCheckin');
  };
  goToLiveCheckIn = () => {
    this.setState({menuLoader2: true});
    const {bearer_token, institute_id, user_id, travel_check_in} = this.state;
    fetch(Const + 'api/Staff/IsCheckedInNew', {
      method: 'POST',
      withCredentials: true,
      credentials: 'include',
      headers: {
        Authorization: bearer_token,
        Accept: 'text/plain',
        'Content-Type': 'application/json-patch+json',
      },
      body: JSON.stringify({
        staffCode: user_id,
        instituteId: Number(institute_id),
      }),
    })
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          showAlert: false,
          loader: false,
          menuLoader2: false,
        });
        if (json && json.isCheckedIn) {
          Toast.show({
            text: 'You have already checked in.Please view your tasks',
            duration: 3000,
            type: 'warning',
            textStyle: {
              fontFamily: 'Poppins-Regular',
              color: '#ffffff',
              textAlign: 'center',
              fontSize: 14,
            },
          });
          this.props.navigation.navigate('StaffTasks', {disable: true});
        } else {
          //console.log('ttravel', travel_check_in);
          if (travel_check_in === 'no') {
            this.props.navigation.navigate('LiveCheckin', {travel: false});
          } else {
            this.props.navigation.navigate('LiveCheckin', {travel: true});
          }
        }
      })
      .catch((error) => {
        this.setState({showAlert: false, loader: false});
        console.error(error);
      });
  };
  goToOtherCheckOut = () => {
    this.props.navigation.navigate('OtherCheckout');
  };
  goToLeaves = () => {
    this.props.navigation.navigate('Leaves');
  };
  handleLogout = () => {
    //AsyncStorage.setItem('user_id', '');
    AsyncStorage.setItem('bearer_token', '');
    //AsyncStorage.clear()
    this.props.navigation.replace('MainNavigator');
  };
  render() {
    const {isFaceRequired} = this.state;
    return (
      <Container>
        <Header style={styles.header} androidStatusBarColor="#f05760">
          <Left style={styles.leftContainer}>
            {this.state.profile_pic ? (
              <Image
                source={{uri: this.state.profile_pic}}
                style={{width: 65, height: 65, borderRadius: 10}}
              />
            ) : (
              <Image
                source={require('../../assets/sideicon.png')}
                style={{width: 65, height: 65}}
              />
            )}
          </Left>
          <Body>
            <View style={styles.welcomeContainer}>
              <Text style={styles.welcomeText}>Welcome to PalGeo!</Text>
            </View>
          </Body>
          {Platform.OS == 'ios' ? <Right></Right> : null}
        </Header>
        <Content>
          <List>
            {this.state.menus.includes('User Profile') && (
              <TouchableWithoutFeedback onPress={this.goToMyProfile}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_proile1.png')}
                      style={{width: 25, height: 25}}
                    />
                    <Text style={styles.menu}>User Profile</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Campus Fencing') && (
              <TouchableWithoutFeedback onPress={this.goToSelectInstitute}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_campus_fencing.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Campus Fencing</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Face Register') && (
              <TouchableWithoutFeedback onPress={this.goToFaceRegister}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_face_register.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Face Register</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.showOtherCheckInMenu && (
              <TouchableWithoutFeedback onPress={this.goToFaceRegisterOther}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_face_register.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Others Face Register</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Check in') && (
              <TouchableWithoutFeedback onPress={this.goToCheckIn}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/checkin.png')}
                      style={{width: 27, height: 27}}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <Text style={styles.menu}>Check In</Text>
                      {this.state.menuLoader1 && (
                        <View style={{marginLeft: 10}}>
                          <ActivityIndicator color="red" size="small" />
                        </View>
                      )}
                    </View>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Live check in') && (
              <TouchableWithoutFeedback onPress={this.goToLiveCheckIn}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/checkin.png')}
                      style={{width: 27, height: 27}}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <Text style={styles.menu}>Live Check In</Text>
                      {this.state.menuLoader2 && (
                        <View style={{marginLeft: 10}}>
                          <ActivityIndicator color="red" size="small" />
                        </View>
                      )}
                    </View>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Check out') && (
              <TouchableWithoutFeedback onPress={this.goToCheckout}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/checkout.png')}
                      style={{width: 27, height: 27}}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <Text style={styles.menu}>Check Out</Text>
                      {this.state.menuLoader && (
                        <View style={{marginLeft: 10}}>
                          <ActivityIndicator color="red" size="small" />
                        </View>
                      )}
                    </View>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Live check out') && (
              <TouchableWithoutFeedback onPress={this.goToLiveCheckout}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/checkout.png')}
                      style={{width: 27, height: 27}}
                    />
                    <View style={{flexDirection: 'row'}}>
                      <Text style={styles.menu}>Live Check Out</Text>
                      {this.state.menuLoader3 && (
                        <View style={{marginLeft: 10}}>
                          <ActivityIndicator color="red" size="small" />
                        </View>
                      )}
                    </View>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.showOtherCheckInMenu && (
              <TouchableWithoutFeedback onPress={this.goToOtherCheckIn}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/checkin.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Other Check In</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.showOtherCheckInMenu && (
              <TouchableWithoutFeedback onPress={this.goToOtherCheckOut}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/checkout.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Other Check Out</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Reports') && (
              <TouchableWithoutFeedback onPress={this.goToReports}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_report.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Reports</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}

            {this.state.menus.includes('Manager Report') && (
              <TouchableWithoutFeedback onPress={this.goToManagerReports}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_report.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Manager Report</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}

            {this.state.menus.includes('e-Circular') && (
              <TouchableWithoutFeedback onPress={this.goToCircular}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_ecricule.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>e-Circular</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Locate Staff') && (
              <TouchableWithoutFeedback onPress={this.goToLocateMenu}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_ecricule.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Locate Staff</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            <TouchableWithoutFeedback onPress={this.goToLeaves}>
              <ListItem>
                <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                    source={require('../../assets/ic_notification.png')}
                    style={{width: 27, height: 27}}
                  />
                  <Text style={styles.menu}>Leaves</Text>
                </Body>
              </ListItem>
            </TouchableWithoutFeedback>
            {this.state.menus.includes('QrCodeGenerator') && (
              <TouchableWithoutFeedback
                onPress={() =>
                  this.props.navigation.navigate('QRCodeGenerator')
                }>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      //source={require('../../assets/ic_report.png')}
                      source={{
                        uri: 'https://static.thenounproject.com/png/2956132-200.png',
                      }}
                      style={{
                        width: 27,
                        height: 27,
                        backgroundColor: 'lightyellow',
                        borderRadius: 27 / 2,
                        tintColor: 'orange',
                      }}
                    />
                    <Text style={styles.menu}>QR Code Generator</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Notifications') && (
              <TouchableWithoutFeedback onPress={this.goToCircularList}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_notification.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Notifications</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Privacy Policy') && (
              <TouchableWithoutFeedback onPress={this.goToPrivacy}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_privacy.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Privacy Policy</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
            {this.state.menus.includes('Logout') && (
              <TouchableWithoutFeedback onPress={this.handleLogout}>
                <ListItem>
                  <Body style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image
                      source={require('../../assets/ic_logout.png')}
                      style={{width: 27, height: 27}}
                    />
                    <Text style={styles.menu}>Logout</Text>
                  </Body>
                </ListItem>
              </TouchableWithoutFeedback>
            )}
          </List>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  sideHeader: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#b9b9b9',
  },
  menu: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    paddingLeft: '4%',
  },
  header: {
    backgroundColor: '#f05760',
    height: Platform.OS == 'android' ? hp('13') : undefined,
  },
  leftContainer: {
    marginLeft: wp('2'),
    marginBottom: Platform.OS == 'ios' ? '4%' : '0%',
  },
  buttonContainer: {
    width: wp('22'),
    height: hp('6'),
    backgroundColor: '#e7f6fb',
    margin: '2%',
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  welcomeText: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#ffffff',
  },
  welcomeContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: wp('3'),
  },
});
