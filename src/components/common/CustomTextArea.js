import {Textarea, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {CustomButton} from './CustomButton';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
export class CustomTextArea extends Component {
  constructor(props) {
    super(props);
    this.state = {Attachment: {}};
  }
  SelectImage = async () => {
    const {SelectedImage} = this.props;
    try {
      const res = await DocumentPicker.pick({
        includeBase64: true,
        type: [DocumentPicker.types.allFiles],
      });
      let typeIdentifier = res.type.split('/');
      console.log(typeIdentifier[1]);
      //alert(typeIdentifier[1]);
      RNFetchBlob.fs
        .readFile(res.uri, 'base64')
        .then((data) => {
          var Attachment = {
            FileName:
              'Attachment_' + new Date().getTime() + '.' + typeIdentifier[1],
            FileType: res.type,
            Attachment: data,
          };

          this.setState({Attachment: Attachment}, () =>
            SelectedImage(Attachment),
          );
        })
        .catch((err) => {});
    } catch (err) {
      console.log('Unknown Error: ' + JSON.stringify(err));
    }
  };
  render() {
    let {
      text,
      onChangeText,
      value,
      placeholder,
      width,
      backgroundColor,
      placeholderTextColor,
      title,
      onPress,
      onPress2,
    } = this.props;
    return (
      <View style={styles.item1}>
        <Text style={[styles.label, {margin: 10}]}>{title}</Text>
        <Textarea
          blurOnSubmit={true}
          rowSpan={5}
          bordered
          placeholderTextColor={placeholderTextColor}
          placeholder={placeholder}
          style={[
            styles.textarea,
            {
              width: width,
              backgroundColor: backgroundColor,
              alignSelf: 'center',
            },
          ]}
          value={value}
          onChangeText={(message) => {
            onChangeText(message);
          }}
        />
        <View style={{marginTop: 20}}>
          <Text style={[styles.label, {margin: 10}]}>Attachment</Text>
          <TouchableOpacity
            onPress={this.SelectImage}
            style={{
              backgroundColor: backgroundColor,
              borderRadius: 10,
              height: 40,
              width: '95%',
              alignSelf: 'center',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Thumbnail
              square
              style={{
                width: 15,
                height: 15,
                resizeMode: 'contain',
                marginLeft: 10,
              }}
              source={require('../../assets/attachment.png')}
            />
            <Text
              style={[
                styles.label,
                {color: placeholderTextColor, padding: 10, fontWeight: '400'},
              ]}>
              Attach document proof
            </Text>
          </TouchableOpacity>
          <View
            style={{
              backgroundColor: backgroundColor,
              borderRadius: 10,
              margin: 10,
              minHeight: 40,
              width: '55%',
              paddingHorizontal: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '85%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  styles.label,
                  {
                    color: placeholderTextColor,
                    paddingHorizontal: 10,
                    fontSize: 14,
                  },
                ]}>
                {text}
              </Text>
            </View>
            <TouchableOpacity
              onPress2={() => {
                onPress2();
              }}
              style={{width: '15%', alignItems: 'center'}}>
              <Thumbnail
                square
                style={{
                  width: 12,
                  height: 12,
                  resizeMode: 'contain',
                }}
                source={require('../../assets/cross.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={{marginVertical: 20}}>
            <CustomButton
              title="Apply"
              width="40%"
              color={placeholderTextColor}
              onPress={() => {
                onPress();
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  item1: {
    width: '95%',
    alignSelf: 'center',
    borderRadius: 10,
    backgroundColor: 'white',
    elevation: 3,
    shadowColor: 'silver',
    shadowOffset: {width: 0, height: 0.5},
    shadowOpacity: 0.6,
  },
  textarea: {
    alignSelf: 'center',
    padding: 10,
    borderLeftWidth: 0,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderRadius: 8,
    width: '100%',
  },
  label: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#000000',
  },
});
