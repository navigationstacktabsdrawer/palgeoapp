import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {Header, Body} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
export default SubHeader = (props) => {
  const goBack = () => {
    props.navigation.navigate(props.backScreen);
  };
  const notifications = () => {
    props.navigation.navigate('Notifications');
  };
  return (
    <Header style={styles.header} androidStatusBarColor="#f05760">
      <Body style={styles.row}>
        {!props.showBack ? null : (
          <TouchableWithoutFeedback onPress={goBack}>
            <View style={styles.imageContainer}>
              <Image
                source={require('../../assets/back-icon.png')}
                resizeMode="contain"
                style={styles.barImage}
              />
            </View>
          </TouchableWithoutFeedback>
        )}

        <View style={styles.headerContainer}>
          <Text style={styles.text}>{props.title}</Text>
        </View>
        {!props.showBell ? null : (
          <TouchableWithoutFeedback onPress={notifications}>
            <View style={styles.imageContainer2}>
              <Image
                source={require('../../assets/bell.png')}
                resizeMode="contain"
                style={styles.barImage1}
              />
            </View>
          </TouchableWithoutFeedback>
        )}
      </Body>
    </Header>
  );
};
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#f05760',
    height: Platform.OS == 'android' ? hp('8') : undefined,
  },
  row: {
    flexDirection: 'row',
    paddingTop: Platform.OS == 'android' ? '5%' : '0%',
    paddingBottom: Platform.OS == 'android' ? '5%' : '4.5%',
  },
  barImage: {
    width: 25,
    height: 25,
  },
  headerContainer: {
    marginLeft: '3%',
  },
  text: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 15,
    color: '#ffffff',
  },
  imageContainer: {
    marginLeft: '1%',
  },
  imageContainer1: {
    marginTop: '2.5%',
    paddingLeft: wp('12'),
  },
  imageContainer2: {
    position: 'absolute',
    right: wp('2'),
    top: hp('3'),
  },
  barImage1: {
    width: 20,
    height: 20,
  },
});
