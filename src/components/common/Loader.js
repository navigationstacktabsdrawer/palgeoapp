import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  StatusBar,
} from 'react-native';
export default Loader = () => {
  return (
    <View style={[styles.container, styles.horizontal]}>
      <StatusBar backgroundColor="#f05760" />
      <ActivityIndicator size="large" color="#f86c6b" />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
