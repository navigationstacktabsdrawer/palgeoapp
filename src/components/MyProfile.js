import React, { Component } from 'react';
import { StyleSheet , View , Image , Text  } from 'react-native';
import SubHeader from './common/SubHeader';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ProfileDetails from './Profile/ProfileDetails';
import AssignedLocations from './Profile/AssignedLocations';
import CurrentTasks from './Profile/CurrentTasks';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
const Tab = createMaterialTopTabNavigator();
const tabActiveColor = '#ffffff';
const tabInActiveColor = '#ffffff';
function MyTabs() {
    return (
          <Tab.Navigator
              initialRouteName="Profile"
              screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                  let tabIcon = require('../assets/ic_profile.png');
                  let screenName = "Profile";
                  if (route.name === 'Profile') {
                    tabIcon = require('../assets/ic_profile.png');
                    screenName = "Profile";
                  } else if (route.name === 'MyLocations') {
                    tabIcon = require('../assets/ic_location.png')
                    screenName = "MyLocations";
                  } else if (route.name === 'MyTasks') {
                    tabIcon =  require('../assets/ic_task.png');
                    screenName = "MyTasks";
                  } 
                   return (
                    <View style={{flexDirection:'row'}}>
                      <Image
                          source={tabIcon}
                          resizeMode='contain'
                          style={{
                            height: 22,
                            width: 22,
                            tintColor: focused ? tabActiveColor : tabInActiveColor,
                          }}
                      />
                    </View>
                  );
                },
              })}
              tabBarOptions={{
                activeTintColor: '#ffffff',
                labelStyle: { fontSize: 12,fontFamily:'Poppins-Regular' },
                style: { backgroundColor: '#f05760' },
                showIcon:true,
                showLabel: false,
                safeAreaInset: { bottom: 'never' },
                indicatorStyle : {backgroundColor:'#ffffff',height:3}
              }}
          >
          <Tab.Screen
              name='Profile'
              component={ProfileDetails}
          />
          <Tab.Screen
              name='MyLocations'
              component={AssignedLocations}
          />
          <Tab.Screen
              name='MyTasks'
              component={CurrentTasks}
          />
         </Tab.Navigator>
      );
  }
export default class MyProfile extends Component {
  constructor(props){
     super(props);
  }
  render() {
    return (
          <View style={{flex:1,backgroundColor : '#ffffff'}}>
            <SubHeader title="My Profile"   showBack={true}    backScreen="Home"  navigation={this.props.navigation}/>
            <MyTabs />
        </View>
        );
     }
 }
const styles = StyleSheet.create({
    text : {
        fontFamily : 'Poppins-Regular',
        fontSize : 14,
        margin : '1%'
    },
    date : {
      fontFamily : 'Poppins-Regular',
        fontSize : 12.5,
        margin : '1%',
        color: '#909090'
    },
    upperBackground : {
        position : 'absolute',
        width:wp('100'),
        height : 100,
        backgroundColor : '#f05760',
        top : 37
    },
})